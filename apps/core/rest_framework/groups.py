def create_groups():
    groups = [
        dict(name='Client', permissions_code_name=[
            'add_client',
            'change_client',
            'delete_client',
            'view_client',
        ]),
        dict(name='File manipulation', permissions_code_name=[
            'add_file',
            'change_file',
            'delete_file',
            'view_file',
        ]),
        dict(name='Client Contact', permissions_code_name=[
            'add_clientcontact',
            'change_clientcontact',
            'delete_clientcontact',
            'view_clientcontact',
        ]),

        dict(name='Assignment', permissions_code_name=[
            'add_assignment',
            'change_assignment',
            'delete_assignment',
            'view_assignment',
        ]),
        dict(name='BankAccount', permissions_code_name=[
            'add_bankaccount',
            'change_bankaccount',
            'delete_bankaccount',
            'view_bankaccount',
        ]),
        dict(name='Branch', permissions_code_name=[
            'add_branch',
            'change_branch',
            'delete_branch',
            'view_branch',
        ]),
        dict(name='CashBox', permissions_code_name=[
            'add_cashbox',
            'change_cashbox',
            'delete_cashbox',
            'view_cashbox',
        ]),
        dict(name='Comment', permissions_code_name=[
            'add_comment',
            'change_comment',
            'delete_comment',
            'view_comment',
        ]),
        dict(name='Contract', permissions_code_name=[
            'add_contract',
            'change_contract',
            'delete_contract',
            'view_contract',
        ]),
        dict(name='Contract Config', permissions_code_name=[
            'add_contractconfig',
            'change_contractconfig',
            'delete_contractconfig',
            'view_contractconfig',
        ]),
        dict(name='Currency', permissions_code_name=[
            'add_currency',
            'change_currency',
            'delete_currency',
            'view_currency',
        ]),
        dict(name='Expense', permissions_code_name=[
            'add_expense',
            'change_expense',
            'delete_expense',
            'view_expense',
        ]),
        dict(name='Fee', permissions_code_name=[
            'add_fee',
            'change_fee',
            'delete_fee',
            'view_fee',
        ]),
        dict(name='Invoice', permissions_code_name=[
            'add_invoice',
            'change_invoice',
            'delete_invoice',
            'view_invoice',
        ]),
        dict(name='Assignment Invoice', permissions_code_name=[
            'add_assignmentinvoice',
            'change_assignmentinvoice',
            'delete_assignmentinvoice',
            'view_assignmentinvoice',
        ]),
        dict(name='Invoice Settings', permissions_code_name=[
            'add_invoicesettings',
            'change_invoicesettings',
            'delete_invoicesettings',
            'view_invoicesettings',
        ]),
        dict(name='Logs', permissions_code_name=[
            'add_logs',
            'change_logs',
            'delete_logs',
            'view_logs',
        ]),
        dict(name='Outsource', permissions_code_name=[
            'add_outsource',
            'change_outsource',
            'delete_outsource',
            'view_outsource',
        ]),
        dict(name='Outsource Contact', permissions_code_name=[
            'add_outsourcecontact',
            'change_outsourcecontact',
            'delete_outsourcecontact',
            'view_outsourcecontact',
        ]),
        dict(name='Position', permissions_code_name=[
            'add_position',
            'change_position',
            'delete_position',
            'view_position',
        ]),
        dict(name='PreInvoice', permissions_code_name=[
            'add_preinvoice',
            'change_preinvoice',
            'delete_preinvoice',
            'view_preinvoice',
        ]),
        dict(name='ContractRate', permissions_code_name=[
            'add_contractrate',
            'change_contractrate',
            'delete_contractrate',
            'view_contractrate',
        ]),
        dict(name='Role', permissions_code_name=[
            'add_role',
            'change_role',
            'delete_role',
            'view_role',
        ]),
        dict(name='Tags', permissions_code_name=[
            'add_tags',
            'change_tags',
            'delete_tags',
            'view_tags',
        ]),
        dict(name='Team', permissions_code_name=[
            'add_team',
            'change_team',
            'delete_team',
            'view_team',
        ]),
        dict(name='Transaction', permissions_code_name=[
            'add_transaction',
            'change_transaction',
            'delete_transaction',
            'view_transaction',
        ]),
        dict(name='SalaryTransaction', permissions_code_name=[
            'add_salarytransaction',
            'change_salarytransaction',
            'delete_salarytransaction',
            'view_salarytransaction',
        ]),
        dict(name='User', permissions_code_name=[
            'add_user',
            'change_user',
            'delete_user',
            'view_user',
        ]),

    ]

    from django.contrib.auth.models import Permission, Group
    for group in groups:
        permissions = Permission.objects.filter(
            codename__in=group['permissions_code_name'])
        group, created = Group.objects.get_or_create(name=group['name'])
        group.permissions.set(permissions)

# 'add_answer',
# 'change_answer',
# 'delete_answer',
# 'view_answer',
