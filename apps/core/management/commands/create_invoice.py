from django.core.management import BaseCommand

from main.models import InvoiceSettings


class Command(BaseCommand):
    help = 'Get Company'

    def handle(self, *args, **options):
        self.create_invoice()

    def create_invoice(self, **_):
        settings = InvoiceSettings.objects.filter(deactivated=False)
