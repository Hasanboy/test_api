from django.db import models
from django.db.models import QuerySet, Count


class BaseQuerySet(models.QuerySet):

    def delete(self):
        self.update(is_delete=True)


class DeleteManager(models.Manager):
    def get_queryset(self) -> QuerySet:
        return BaseQuerySet(self.model).filter(is_delete=False)


class ManagerMixin(object):
    def get_queryset(self) -> QuerySet:
        queryset = super(ManagerMixin, self).get_queryset()
        queryset = queryset.filter(is_delete=False)
        return queryset


class StatisticManager(ManagerMixin):

    def count_by_general_status(self, filters=None):
        queryset = self.order_by()
        if filters:
            queryset = queryset.filter(**filters)
        queryset.query.group_by = []
        queryset = queryset.values('general_status')
        queryset = queryset.annotate(total_count=Count('*'))
        queryset = queryset.values('general_status', 'total_count')
        return queryset
