from rest_framework import viewsets

from main.models import ContractConfig
from main.serializers.contract.contract_config_models import ContractConfigModelSerializer


class ContractConfigViewSet(viewsets.ModelViewSet):
    queryset = ContractConfig.objects.all()
    ordering = ['-created_date']
    serializer_class = ContractConfigModelSerializer
    filter_fields = ['contract']
