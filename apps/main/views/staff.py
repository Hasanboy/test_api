from rest_framework import mixins
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from django.contrib.auth.models import Group, Permission

from main.filters.staff import StaffFilterSet
from main.models import User
from main.serializers.role import GroupModelSerializer, PermissionSerializer
from main.serializers.user import UserCreateModelSerializer


class GroupViewSet(ModelViewSet):
    model = Group
    queryset = Group.objects.exclude(
        name__in=['Applicant', 'Employer', 'Employer Staff'])
    serializer_class = GroupModelSerializer
    search_fields = ['name']


class PermissionViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                        GenericViewSet):
    serializer_class = PermissionSerializer
    queryset = Permission.objects.all()
    search_fields = ['name']


class StaffViewSet(ModelViewSet):
    """
            retrieve:
            Return object of Staff

            list:
            Return a list of Staffs objects

            create:
            Create a new  instance of Staffs
            ###Request body

                {

                    'password': 'hive_password_123',                        # password
                    'username': 'hive'                                      # username
                    'groups': [1,2]
                    'salary': 23232323
                }

            update:
            Update fields of Staffs object
        """
    serializer_class = UserCreateModelSerializer
    queryset = User.objects.filter(is_staff=False, is_superuser=False,
                                   user_type=User.ADMIN)
    ordering = ['-date_joined']
    filter_class = StaffFilterSet
    search_fields = ['username']
