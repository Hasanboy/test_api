from rest_framework import viewsets

from main.models import Tags
from main.serializers.tags import TagsModelSerializer


class TagsViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Brand.

        list:
        Return a list of all brands.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Brand instance.
        ###fields
        + name - **String**
    """
    model = Tags
    queryset = Tags.objects.all()
    filter_fields = ['model']
    serializer_class = TagsModelSerializer
