from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from main.models import InvoiceSettings
from main.serializers.invoice.invoice_settings import InvoiceSettingsModelSerializer


class InvoiceSettingsViewSet(viewsets.ModelViewSet):
    """
        retrieve:
        Return object of InvoiceSettings

        list:
        Return a list of InvoiceSettings objects

        create:
        Create a new  instance of InvoiceSettings

        update:
        Update fields of InvoiceSettings object
    """
    queryset = InvoiceSettings.objects.all()
    serializer_class = InvoiceSettingsModelSerializer
    ordering_fields = []
    ordering = ['-created_date']

    def get_permissions(self):
        if self.action == 'list':
            return [AllowAny()]
        return super().get_permissions()
