from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from main.models import Role
from main.serializers.role import RoleModelSerializer


class RoleViewSet(viewsets.ModelViewSet):
    """
        retrieve:
        Return object of Role

        list:
        Return a list of Role objects

        create:
        Create a new  instance of Role

        update:
        Update fields of Role object
    """
    model = Role
    queryset = Role.objects.all()
    serializer_class = RoleModelSerializer
    ordering_fields = []
    search_fields = ['name']
    ordering = ['-created_date']

    def get_permissions(self):
        if self.action == 'list':
            return [AllowAny()]
        return super().get_permissions()
