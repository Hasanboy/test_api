from rest_framework import viewsets, status
from rest_framework.response import Response

from main.filters.client import ClientFilterSet
from main.models import Client, Assignment, Contract, User, ClientContact
from main.serializers.client import ClientModelSerializer


class ClientViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Client.

        list:
        Return a list of all Client.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Brand instance.
        ###fields
        + name - **String**
    """
    model = Client
    queryset = Client.objects.all()

    serializer_class = ClientModelSerializer
    search_fields = ['name']
    ordering = ['-created_date']
    filter_class = ClientFilterSet

    def destroy(self, request, *args, **kwargs):
        client: Client = self.get_object()
        assignments = Assignment.objects.filter(client=client.id)
        contracts = Contract.objects.filter(client=client.id)

        if assignments or contracts:
            return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
        else:
            User.objects.filter(client=client.id).delete()
            client_contacts = ClientContact.objects.filter(client=client.id)
            if client_contacts:
                client_contacts.delete()
            client.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
