from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import AllowAny

from main.models import Invoice, Fee
from main.serializers.statistics import InvoiceFilterSerializer, \
    InvoiceStatisticsStatusSerializer, \
    InvoiceStatisticsPeriodSerializer, FeeFilterSerializer, \
    FeeStatisticsPeriodSerializer


class OrderStatistics(GenericViewSet):
    def get_permissions(self):
        permissions = super(OrderStatistics, self).get_permissions()
        if self.action in ['invoice_status', 'invoice_period',
                           'invoice_total_period', 'fee_period']:
            permissions = [AllowAny()]
        return permissions

    @action(['GET'], detail=False)
    def invoice_status(self, request, **__):
        """
            ### Return statistics as:
                {
                   'from_date': '2016-01-01',
                   'to_date': '2018-01-01',
                   'all': True,
                }
        """

        date_filter_serializer = InvoiceFilterSerializer(data=request.GET)
        date_filter_serializer.is_valid(raise_exception=True)
        queryset = Invoice.objects.stat_invoice_status(
            **date_filter_serializer.data, user=request.user)
        serializer = InvoiceStatisticsStatusSerializer(queryset, many=True)

        if serializer.data:
            total_paid_amount = serializer.data[0]['total_paid_amount']
            total_amount = serializer.data[0]['total_amount']
        else:
            total_paid_amount = 0
            total_amount = 0

        return Response(dict(
            total_paid_amount=total_paid_amount,
            total_amount=total_amount
        ))

    @action(['GET'], detail=False)
    def invoice_period(self, request, **__):
        """
            ### Return statistics as:
                {
                   'from_date': '2016-01-01',
                   'to_date': '2018-01-01',
                   'all': True,
                }
        """

        date_filter_serializer = InvoiceFilterSerializer(data=request.GET)
        date_filter_serializer.is_valid(raise_exception=True)
        queryset = Invoice.objects.stat_paid_invoice_month(
            **date_filter_serializer.data, user=request.user)
        serializer = InvoiceStatisticsPeriodSerializer(queryset, many=True)

        return Response(serializer.data)

    @action(['GET'], detail=False)
    def fee_period(self, request, **__):
        """
            ### Return statistics as:
                {
                   'from_date': '2016-01-01',
                   'to_date': '2018-01-01',
                   'all': True,

                }
        """

        date_filter_serializer = FeeFilterSerializer(data=request.GET)
        date_filter_serializer.is_valid(raise_exception=True)
        queryset = Fee.objects.stat_fee_by_date(**date_filter_serializer.data,
                                                user=request.user)
        serializer = FeeStatisticsPeriodSerializer(queryset, many=True)

        return Response(serializer.data)
