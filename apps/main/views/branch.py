from rest_framework import viewsets, status
from rest_framework.response import Response

from main.models import Branch
from main.serializers.branch import BranchModelSerializer


class BranchViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Brand.

        list:
        Return a list of all brands.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Brand instance.
        ###fields
        + name - **String**
    """
    model = Branch
    queryset = Branch.objects.all()

    serializer_class = BranchModelSerializer

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)