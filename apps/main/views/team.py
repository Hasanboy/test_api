from rest_framework import viewsets

from main.models import Team
from main.serializers.team import TeamModelSerializer


class TeamViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Expense.

        list:
        Return a list of all Expense.
        ###Filter fields

        create:
        Create a new Expense instance.
        ###Request body
            {
                'name': 'Team One',
                'team_leader': 1,
                'branch': 1,
                'team_members': [1,2,3],
            }
    update:
           Update fields of Order object
       """
    queryset = Team.objects.all()

    serializer_class = TeamModelSerializer
