from rest_framework import permissions, status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from core.rest_framework.permissions import IsAuthenticated
from main.models import User, Currency
from main.serializers.position import PositionModelSerializer, PhotoSerializers
from main.serializers.role import RoleModelSerializer
from main.serializers.select_serializer import SelectCurrencyUserSerializer
from main.serializers.user import CheckTokenSerializer


class UserViewSet(GenericViewSet):
    serializer_class = AuthTokenSerializer
    queryset = User.objects.all()

    @action(['POST'], detail=False, permission_classes=[permissions.AllowAny])
    def login(self, request: Request):
        self.serializer_class = AuthTokenSerializer
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})

    @action(['DELETE'], detail=False, permission_classes=[IsAuthenticated])
    def logout(self, request: Request):
        Token.objects.get(user=request.user).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(['GET'], detail=False, url_path='check_token/(?P<token>.{40})',
            permission_classes=[permissions.AllowAny])
    def check_token(self, request, token=None):
        serializer = CheckTokenSerializer(data=dict(token=token))
        serializer.is_valid(raise_exception=True)
        token = serializer.validated_data['token']
        token = Token.objects.filter(key=token).first()
        if token:
            user_type = None
            serializer = {
            }.get(user_type, None)
            if serializer:
                data = serializer(token.user).data
            else:
                data = dict(
                    username=token.user.username, id=token.user.id,
                )
            if token.user.photo:
                photo = PhotoSerializers(data=dict(
                    id=token.user.photo.id,
                    name=token.user.photo.name,
                    content_type=token.user.photo.content_type,
                    file=token.user.photo.file,

                ), context=dict(request=request))

                photo.is_valid(raise_exception=True)
                photo_data = photo.data
            else:
                photo_data = None
            position = PositionModelSerializer(token.user.position)
            role = RoleModelSerializer(token.user.role)
            currency_data = Currency.objects.filter(is_primary=True).first()
            if currency_data:
                currency = SelectCurrencyUserSerializer(data=dict(
                    id=currency_data.id,
                    name=currency_data.name,
                    sign=currency_data.sign,
                    is_primary=currency_data.is_primary,
                    rate=currency_data.rate,

                ))

                currency.is_valid(raise_exception=True)
                currency_obj = currency.data
            else:
                currency_obj = None
            data = dict(
                user=data,
                photo=photo_data,
                is_superuser=token.user.is_superuser,
                position=position.data,
                role=role.data,
                currency=currency_obj
            )
            return Response(data)
        return Response(status=status.HTTP_404_NOT_FOUND)
