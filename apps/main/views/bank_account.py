from rest_framework import viewsets, status
from rest_framework.response import Response

from main.models import BankAccount
from main.serializers.bank_account import BankAccountModelSerializer


class BankAccountViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given BankAccount.

        list:
        Return a list of all BankAccount.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new BankAccount instance.
        ###fields
        + name - **String**
    """
    model = BankAccount
    queryset = BankAccount.objects.all()
    ordering = ['-created_date']
    serializer_class = BankAccountModelSerializer
    filter_fields = ['branch']

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)