from rest_framework import viewsets

from main.models import PreInvoice
from main.serializers.invoice.pre_invoice import InvoicePreviewModelSerializer


class PreInvoiceViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given PreInvoice.

        list:
        Return a list of all PreInvoice.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new PreInvoice instance.
        {
            'client': 1,
            'assignments': [1, 2],
            'from_date': '2020-02-25',
            'to_date': '2020-02-27',
            'type': 'all'

        }
    update:
           Update fields of Invoice object
       """
    model = PreInvoice
    queryset = PreInvoice.objects.all()

    serializer_class = InvoicePreviewModelSerializer
