from rest_framework import viewsets, status
from rest_framework.response import Response

from main.serializers.content_type import ContentTypeSerializer
from django.contrib.contenttypes.models import ContentType


class ContentTypeViewSet(viewsets.ModelViewSet):
    queryset = ContentType.objects.filter(
        model__in=['client', 'assignment', 'outsource', 'user'])

    serializer_class = ContentTypeSerializer

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def create(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
