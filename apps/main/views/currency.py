from rest_framework import viewsets, status
from rest_framework.response import Response

from main.filters.currency import CurrencyFilterSet
from main.models import Currency, Assignment, Contract, BankAccount
from main.serializers.currency import CurrencyModelSerializer


class CurrencyViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Currency.

        list:
        Return a list of all Currency.
        ###Filter fields

        create:
        Create a new Currency instance.
        ###fields
        + name - **String**
    """
    model = Currency
    queryset = Currency.objects.all()
    serializer_class = CurrencyModelSerializer
    filter_class = CurrencyFilterSet

    def destroy(self, request, *args, **kwargs):
        currency: Currency = self.get_object()
        assignments = Assignment.objects.filter(currency=currency.id)
        contracts = Contract.objects.filter(currency=currency.id)
        bank_accounts = BankAccount.objects.filter(currency=currency.id)
        if assignments or contracts or bank_accounts:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=dict(
                message='You cannot delete because currency already is related '
            ))
        else:
            currency.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
