from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from main.filters.transaction import TransactionFilterSet
from main.models import Transaction
from main.serializers.transaction import TransactionModelSerializer
from main.serializers.transaction_pending import TransactionPendingSerializer



class TransactionViewSet(viewsets.ModelViewSet):
    """
            ## Readme
            retrieve:
            Return the given Transaction.

            list:
            Return a list of all Transaction.
            ###Filter fields
            + ids - ***1-2-3-4***

            create:
            Create a new Transaction instance.
            {
                'cashbox': 1,
                'amount': 1000,
                'month': 1000,
                'branch': 1000,
                'to_cashbox': 2,
                'from_branch': 2,
                'to_branch': 2,
                'description': 'description',
                'type': 'out',
                'amount': 1000,
                'category': 'rents_utilities/stationary_grocery/remuneration/business_development/tax/internal_expenses/internal/payment',
                'period_from': '2019-01-01',
                'period_to': '2019-01-01',
                'salaries': [
                        {
                            'staff': 1,
                            'salary': 5000
                        },
                        {
                            'staff': 2,
                            'salary': 5000
                        }
                        ]

        update:
               Update fields of Transaction object
           """
    queryset = Transaction.objects.all()
    serializer_class = TransactionModelSerializer
    filter_class = TransactionFilterSet

    # def destroy(self, request, *args, **kwargs):
    #     return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    # def get_serializer_class(self):
    #     instance = self.get_object()
    #     if self.request.data['type'] == 'in' and self.action in ['create']:
    #         return TransactionInSerializer
    #     return super().get_serializer_class()

    @action(['POST'], detail=True)
    def confirm_or_relocate(self, request, pk=None):
        serializer = TransactionPendingSerializer(instance=self.get_object(),
                                                  data=request.data,
                                                  context=dict(
                                                      request=request))
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK, data=serializer.data)
