from rest_framework import viewsets

from main.models import ClientContact
from main.serializers.client_contact import ClientContactModelSerializer


class ClientContactViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given ClientContact.

        list:
        Return a list of all ClientContact.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Contract instance.
        ###fields
        + name - **String**
    """
    queryset = ClientContact.objects.all()
    ordering = ['-created_date']
    serializer_class = ClientContactModelSerializer
    filter_fields = ['client']
