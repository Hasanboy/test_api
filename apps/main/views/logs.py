from rest_framework import viewsets, status
from rest_framework.response import Response

from main.models import Logs
from main.serializers.logs import LogsModelSerializer


class LogsViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Logs.

        list:
        Return a list of all Logs.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Logs instance.
        ###fields
        + name - **String**
    """
    model = Logs
    queryset = Logs.objects.all()
    filter_fields = ['fee']

    serializer_class = LogsModelSerializer

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
