from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from main.models import Contract
from main.serializers.contract.contract import ContractModelSerializer
from main.serializers.contract.next_contract import NextContractSerializer


class ContractViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Contract.

        list:
        Return a list of all Contract.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Contract instance.
        {
            'client': 1,
            'branch': 1,
            'currency': 1,
            'bank_account': 1,
            'payment_duration': 1,
            'is_multiple': true,
            'payment_destination': 1,
            'configs': [
                {
                    'external_assignment_name': 'test one',
                    'internal_assignment_name': 'test internal_assignment_name one ',
                    'team_leader': 1,
                    'billing_type': 'hourly_billing',
                    'hourly_has_fee_ceiling': True,
                    'hourly_fee_ceiling': 123123,
                    'rates': [
                            {
                                'position': 1,
                                'amount_per_hour': 4000
                            },
                            {
                                'position': 2,
                                'amount_per_hour': 4000
                            }

                            ]
                },
                {
                    'external_assignment_name': 'test two',
                    'internal_assignment_name': 'test internal_assignment_name two ',
                    'team_leader': 1,
                    'fixed_fee_expenses_included_in_fee': True,
                    'fixed_fee_amount': 123123,
                    'fixed_fee_pre_amount': 123123,
                    'billing_type': 'fixed_fee',
                }

            ]
        }
    update:
     ###Request body
           update a  Contract instance.
            {
                'id': 1,
                'client': 1,
                'branch': 1,
                'currency': 1,
                'bank_account': 1,
                'payment_duration': 1,
                'is_multiple': true,
                'payment_destination': 1,
                'configs': [
                    {
                        'id': 1,
                        'external_assignment_name': 'test one',
                        'internal_assignment_name': 'test internal_assignment_name one ',
                        'team_leader': 1,
                        'billing_type': 'hourly_billing',
                        'hourly_has_fee_ceiling': True,
                        'hourly_fee_ceiling': 123123,
                        'rates': [
                                {
                                    'position': 1,
                                    'amount_per_hour': 4000
                                },
                                {
                                    'position': 2,
                                    'amount_per_hour': 4000
                                }

                                ]
                    },
                    {
                        'external_assignment_name': 'test two',
                        'internal_assignment_name': 'test internal_assignment_name two ',
                        'team_leader': 1,
                        'fixed_fee_expenses_included_in_fee': True,
                        'fixed_fee_amount': 123123,
                        'fixed_fee_pre_amount': 123123,
                        'billing_type': 'fixed_fee',
                    }

                ]
            }
       """
    queryset = Contract.objects.all()
    ordering = ['-created_date']
    serializer_class = ContractModelSerializer
    filter_fields = ['client']

    @action(['POST'], detail=False)
    def next_contract(self, request):
        serializer = NextContractSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'results': serializer.data})
