from rest_framework import viewsets, status
from rest_framework.response import Response

from main.models import Outsource
from main.serializers.outsource import OutsourceModelSerializer


class OutsourceViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Outsource.

        list:
        Return a list of all Outsource.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Outsource instance.
        ###fields
        + name - **String**
    """
    model = Outsource
    queryset = Outsource.objects.all()

    serializer_class = OutsourceModelSerializer
    ordering = ['-created_date']

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
