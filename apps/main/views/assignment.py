from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from main.filters.assignment import AssignmentFilterSet
from main.models import Assignment, Expense, Fee
from main.serializers.assignment.assignment import AssignmentModelSerializer
from main.serializers.assignment.assignment_completed import \
    AssignmentCompletedSerializer
from main.serializers.assignment.assignment_submit import \
    AssignmentSubmitSerializer, AssignmentApprovedSerializer, \
    AssignmentPureSerializer


class AssignmentViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Assignment.

        list:
        Return a list of all Assignment.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Assignment instance.
        {
            'name': 'assignment',
            'created_date': '3112-12-31',
            'dead_line': '3112-12-31',
            'client': 1,
            'contract_config': 1,
            'branch': 1,
            'originated_by': 1,
            'team_leader': 1,
            'tags': [1, 2],
            'work_group': [1],
            'is_billable': True,
            'billing_type': 'hourly_billing',
            'hourly_has_fee_ceiling': True,
            'hourly_fee_ceiling': 123123,
            'fixed_fee_pre_amount': 123123,
            'fixed_fee_amount': 123123,
            'fixed_fee_expenses_included_in_fee': True,
            'payment_destination': 1,
            'currency': 1,
            'bank_account': 1,
            'payment_duration': 20,
            'invoice_delivered_by': 'billing_department',
            'rates': [
                {
                    'position': 1,
                    'amount_per_hour': 4000
                },
                {
                    'position': 2,
                    'amount_per_hour': 4000
                }

            ]
        }
    update:
           Update fields of Assignment object
       """
    model = Assignment
    queryset = Assignment.objects.all()
    ordering = ['-created_date']
    search_fields = ['name', 'client__name']
    serializer_class = AssignmentModelSerializer
    filter_class = AssignmentFilterSet

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.action in ['list', 'submitted', 'approved', 'retrieve']:
            queryset = queryset.get_invoices_type()
        return queryset

    def destroy(self, request, *args, **kwargs):
        assignment: Assignment = self.get_object()
        expenses = Expense.objects.filter(assignment=assignment.id)
        fees = Fee.objects.filter(assignment=assignment.id)
        if expenses or fees:
            return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
        else:
            assignment.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        response = super(AssignmentViewSet, self).list(request, *args,
                                                       **kwargs)

        if not self.request.user.has_perm('admin.all_true'):
            # if self.request.GET.get('all') != 'true':
            queryset_one = queryset.filter(owner=self.request.user)
            queryset_two = queryset.filter(team_leader=self.request.user)
            queryset_three = queryset.filter(originated_by=self.request.user)
            queryset_four = queryset.filter(
                work_group__id=self.request.user.id)
            last_queryset = queryset_one.union(queryset_one, queryset_two,
                                               queryset_three, queryset_four)
            last_queryset = last_queryset.order_by('-created_date')

            page = self.paginate_queryset(last_queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)
            serializer = self.get_serializer(last_queryset,
                                             context=dict(request=request),
                                             many=True)
            return Response(serializer.data)
        return response

    @action(['GET'], detail=False, serializer_class=AssignmentSubmitSerializer)
    def submitted(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        query_list = []
        for quer in queryset:
            fees = Fee.objects.filter(assignment=quer.id,
                                      status_approvement=Fee.SUBMITTED)
            expenses = Expense.objects.filter(assignment=quer.id,
                                              status_approvement=Expense.SUBMITTED)
            if not fees and not expenses:
                query_list.append(quer.id)
        queryset = queryset.exclude(id__in=query_list)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset,
                                         context=dict(request=request),
                                         many=True)
        return Response(serializer.data)

    @action(['GET'], detail=False,
            serializer_class=AssignmentApprovedSerializer)
    def approved(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        query_list = []
        for quer in queryset:
            fees = Fee.objects.filter(assignment=quer.id,
                                      status_approvement=Fee.APPROVED)
            expenses = Expense.objects.filter(assignment=quer.id,
                                              status_approvement=Expense.APPROVED)
            if not fees and not expenses:
                query_list.append(quer.id)
        queryset = queryset.exclude(id__in=query_list)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset,
                                         context=dict(request=request),
                                         many=True)
        return Response(serializer.data)

    @action(['GET'], detail=False,
            serializer_class=AssignmentPureSerializer)
    def assignment_pure(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset,
                                         context=dict(request=request),
                                         many=True)
        return Response(serializer.data)

    @action(['POST'], detail=True)
    def complete(self, request, pk=None):
        serializer = AssignmentCompletedSerializer(instance=self.get_object(),
                                                   data=request.data,
                                                   context=dict(
                                                       request=request))
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK, data=serializer.data)
