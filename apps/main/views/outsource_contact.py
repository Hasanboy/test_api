from rest_framework import viewsets

from main.models import OutsourceContact
from main.serializers.client_contact import OutsourceContactModelSerializer


class OutsourceContactViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given OutsourceContact.

        list:
        Return a list of all OutsourceContact.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new OutsourceContact instance.
        ###fields
        + name - **String**
    """
    queryset = OutsourceContact.objects.all()
    ordering = ['-created_date']
    serializer_class = OutsourceContactModelSerializer
    filter_fields = ['outsource']
