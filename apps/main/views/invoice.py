from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from main.models import Invoice, Fee, Expense, ContractConfig
from main.serializers.invoice.invoice import InvoiceModelSerializer
from main.serializers.invoice.invoice_delivered import \
    InvoiceDeliveredSerializer
from main.serializers.invoice.invoice_payments import InvoicePaymentSerializer
from main.serializers.invoice.invoice_send import InvoiceSendSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Invoice.

        list:
        Return a list of all Invoice.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Invoice instance.
        {
            'client': 1,
            'contract': 1,
            'total_amount': 10000,
            'type': "pre_payment/simple",
            'description': 'Hello',
            'assignments': [
                {
                    'assignment': 1,
                    'amount': 1000,
                },
                {
                    'assignment': 2,
                    'fees': [3],
                    'expenses': [1, 2]
                }

            ],
            'due_date': '2020-02-25',

        }
    update:
           Update fields of Invoice object
       """
    model = Invoice
    queryset = Invoice.objects.all()

    serializer_class = InvoiceModelSerializer

    filter_fields = ['type', 'contract', 'status_payment', 'client']

    def destroy(self, request, *args, **kwargs):
        invoice: Invoice = self.get_object()
        if invoice.status_payment == Invoice.DRAFT:
            fees = Fee.objects.filter(invoice=invoice.id)
            if fees:
                fees.update(invoice=None, status_invoice=Fee.OPEN)
            expenses = Expense.objects.filter(invoice=invoice.id)
            if expenses:
                expenses.update(invoice=None, status_invoice=Expense.OPEN)
            if invoice.contract_config:
                contract_config = ContractConfig.objects.filter(
                    id=invoice.contract_config.id).first()
                if contract_config:
                    contract_config.is_invoiced = False
                    contract_config.save()
            invoice.delete()

            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @action(['POST'], detail=True)
    def payment(self, request, pk=None):

        serializer = InvoicePaymentSerializer(instance=self.get_object(),
                                              data=request.data,
                                              context=dict(request=request))
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    @action(['POST'], detail=True)
    def send_invoice(self, request, pk=None):
        serializer = InvoiceSendSerializer(instance=self.get_object(),
                                           data=request.data,
                                           context=dict(request=request))
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    def list(self, request, *args, **kwargs):
        queryset = Invoice.objects.check_due_date()
        self.serializer_class(queryset, context=dict(request=request),
                              many=True)
        return super().list(request, *args, **kwargs)

    @action(['POST'], detail=True)
    def delivered(self, request, pk=None):
        serializer = InvoiceDeliveredSerializer(instance=self.get_object(),
                                                data=request.data,
                                                context=dict(request=request))
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK, data=serializer.data)
