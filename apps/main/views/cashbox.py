from rest_framework import viewsets, status
from rest_framework.response import Response

from main.models import CashBox
from main.serializers.cashbox import CashBoxModelSerializer


class CashBoxViewSet(viewsets.ModelViewSet):
    """
            ## Readme
            retrieve:
            Return the given CashBox.

            list:
            Return a list of all CashBox.
            ###Filter fields
            + ids - ***1-2-3-4***

            create:
            Create a new CashBox instance.
            {
                'name': 'assignment',
                'branch': 1,
                'currency': 1,
                'bank_account': 1,
                'type': "bank/cash/bank_card",

        update:
               Update fields of CashBox object
           """
    queryset = CashBox.objects.all()

    serializer_class = CashBoxModelSerializer
    filter_fields = ['branch', 'bank_account', 'currency', 'type', 'cashier']

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.get_balance()
        return queryset

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
