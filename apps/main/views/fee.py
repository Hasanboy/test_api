from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from main.filters.fee import FeeFilterSet
from main.models import Fee
from main.serializers.fees.fee_approve_status import \
    FeeTimeSheetChangeApproveStatus
from main.serializers.fees.fee import FeeTimeSheetModelSerializer
from main.serializers.fees.fee_reject import FeeRejectSerializer
from main.serializers.fees.fee_time_sheet_status import \
    FeeTimeSheetChangeStatus


class FeeViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Fee.

        list:
        Return a list of all Fee.
        ###Filter fields

        create:
        Create a new Fee instance.
        ###Request body
            {
                'assignment': 1,
                'description': 'Hello',
                'date': "2010-01-01",
                'amount': 231231,
                'total_duration': 231231,

            }
    reject:
     ###Request body
           reject a  Fee instance.
            [

            {
                'fee': 8,
                'comment': 'Hello'
            },
            {
                'fee': 9,
                'comment': 'Hello'
            }

        ]
       """
    model = Fee
    queryset = Fee.objects.all()
    filter_class = FeeFilterSet
    search_fields = ['description', 'assignment__name']
    serializer_class = FeeTimeSheetModelSerializer
    ordering = ['-date']

    def get_queryset(self):
        queryset = super().get_queryset()
        # if self.action == 'list' and self.request.GET.get('all') != 'true':
        if self.action == 'list' and not self.request.user.has_perm(
                'admin.all_true'):
            qs = queryset.filter(user=self.request.user)
            return qs
        return queryset

    @action(['POST'], detail=True)
    def change_status_time(self, request, pk=None):
        self.serializer_class = FeeTimeSheetChangeStatus
        serializer = self.get_serializer(data=request.data,
                                         instance=self.get_object())
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)

    def destroy(self, request, *args, **kwargs):
        fee: Fee = self.get_object()
        if fee.status_invoice == Fee.CLOSED or fee.status_approvement == Fee.APPROVED:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=dict(
                message='You cannot delete because fee already is closed or approved '
            ))
        else:
            fee.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

    @action(['POST'], detail=False)
    def change_status_approve(self, request, pk=None):
        self.serializer_class = FeeTimeSheetChangeApproveStatus
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)

    @action(['POST'], detail=False)
    def reject(self, request, pk=None):
        self.serializer_class = FeeRejectSerializer
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)

    def list(self, request, *args, **kwargs):
        queryset = Fee.objects.check_expire(self.request.user)
        self.serializer_class(queryset, context=dict(request=request),
                              many=True)
        return super(FeeViewSet, self).list(request, *args, **kwargs)
