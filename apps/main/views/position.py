from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from main.models import Position
from main.serializers.position import PositionModelSerializer


class PositionViewSet(viewsets.ModelViewSet):
    """
        retrieve:
        Return object of Position

        list:
        Return a list of Position objects

        create:
        Create a new  instance of Position

        update:
        Update fields of Position object
    """
    model = Position
    queryset = Position.objects.all()
    serializer_class = PositionModelSerializer
    ordering_fields = []
    search_fields = ['name']
    ordering = ['-created_date']

    def get_permissions(self):
        if self.action == 'list':
            return [AllowAny()]
        return super().get_permissions()
