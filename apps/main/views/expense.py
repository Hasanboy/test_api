from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from main.filters.expense import ExpenseFilterSet
from main.models import Expense
from main.serializers.expenses.expense import ExpenseModelSerializer
from main.serializers.expenses.expense_reject import ExpenseRejectSerializer
from main.serializers.expenses.expenses_change_pay import \
    ExpenseChangeApproveStatus


class ExpenseViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Expense.

        list:
        Return a list of all Expense.
        ###Filter fields

        create:
        Create a new Expense instance.
        ###Request body
            {
                'assignment': 1,
                'cashier': 1,
                'expenses': [
                    {
                        'amount': 1000,
                        'description': "Hello",
                        'date': "2010-01-01",
                    },
                    {
                        'amount': 1000,
                        'description': "Hello",
                        'date': "2010-01-01",
                    },
                    {
                        'amount': 1000,
                        'description': "Hello",
                        'date': "2010-01-01",
                    }
                ],
            }
    reject:
     ###Request body
           reject a  Expense instance.
            [

            {
                'expense': 8,
                'comment': 'Hello'
            },
            {
                'expense': 9,
                'comment': 'Hello'
            }

        ]
       """
    model = Expense
    queryset = Expense.objects.all()
    serializer_class = ExpenseModelSerializer
    filter_class = ExpenseFilterSet
    ordering = ['-created_date']

    def get_queryset(self):
        queryset = super().get_queryset()
        # if self.action == 'list' and self.request.GET.get('all') != 'true':
        if self.action == 'list' and not self.request.user.has_perm(
                'admin.all_true'):
            qs = queryset.filter(user=self.request.user)
            return qs
        return queryset

    def destroy(self, request, *args, **kwargs):
        expense: Expense = self.get_object()
        if expense.status_invoice == Expense.CLOSED or expense.status_approvement == Expense.APPROVED:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=dict(
                message='You cannot delete because expense already is closed or approved '
            ))
        else:
            expense.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

    @action(['POST'], detail=False)
    def change_status_approve(self, request, pk=None):
        self.serializer_class = ExpenseChangeApproveStatus
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)

    @action(['POST'], detail=False)
    def reject(self, request, pk=None):
        self.serializer_class = ExpenseRejectSerializer
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)
