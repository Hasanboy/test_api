from rest_framework import viewsets, status
from rest_framework.response import Response

from main.models import Comment
from main.serializers.comment import CommentModelSerializer


class CommentViewSet(viewsets.ModelViewSet):
    """
        ## Readme
        retrieve:
        Return the given Comment.

        list:
        Return a list of all Comment.
        ###Filter fields
        + ids - ***1-2-3-4***

        create:
        Create a new Comment instance.
        ###fields
        + name - **String**
    """
    queryset = Comment.objects.all()

    serializer_class = CommentModelSerializer

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
