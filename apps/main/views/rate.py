from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from main.models import AssignmentRate
from main.serializers.rate import RateModelSerializer


class RateViewSet(viewsets.ModelViewSet):
    """
        retrieve:
        Return object of AssignmentRate

        list:
        Return a list of AssignmentRate objects

        create:
        Create a new  instance of AssignmentRate

        update:
        Update fields of AssignmentRate object
    """
    model = AssignmentRate
    queryset = AssignmentRate.objects.all()
    serializer_class = RateModelSerializer

    def get_permissions(self):
        if self.action == 'list':
            return [AllowAny()]
        return super().get_permissions()
