# Generated by Django 2.2.11 on 2020-06-03 01:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0078_auto_20200602_1255'),
    ]

    operations = [
        migrations.AddField(
            model_name='contractconfig',
            name='is_paid',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='invoice',
            name='contract_config',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='invoices', to='main.ContractConfig'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='expense',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='transactions', to='main.Expense'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='status',
            field=models.CharField(choices=[('pending', 'pending'), ('confirm', 'confirm'), ('cancel', 'cancel')], max_length=45),
        ),
    ]
