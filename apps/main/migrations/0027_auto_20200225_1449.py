# Generated by Django 2.1.2 on 2020-02-25 14:49

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0026_auto_20200225_1150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignment',
            name='bank_account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='assignments', to='main.BankAccount'),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='branch',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assignments', to='main.Branch'),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assignments', to='main.Client'),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='contract',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='assignments', to='main.Contract'),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='currency',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='assignments', to='main.Currency'),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='originated_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='origin_assignments', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='payment_destination',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='assignments_payment', to='main.Branch'),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='server_delivered_to',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='deliver_assignments', to='main.Client'),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='team_leader',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='team_assignment', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='bankaccount',
            name='branch',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bank_accounts', to='main.Branch'),
        ),
        migrations.AlterField(
            model_name='contract',
            name='bank_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contracts', to='main.BankAccount'),
        ),
        migrations.AlterField(
            model_name='expense',
            name='assignment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='expenses', to='main.Assignment'),
        ),
        migrations.AlterField(
            model_name='expense',
            name='cashier',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cashier_expenses', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='expense',
            name='outsource',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='expense', to='main.Outsource'),
        ),
        migrations.AlterField(
            model_name='expense',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='expenses', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='fee',
            name='assignment',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fees', to='main.Assignment'),
        ),
        migrations.AlterField(
            model_name='fee',
            name='time_sheet',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fees', to='main.TimeSheet'),
        ),
        migrations.AlterField(
            model_name='fee',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fees', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='timesheet',
            name='assignment',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='time_trackers', to='main.Assignment'),
        ),
        migrations.AlterField(
            model_name='timesheet',
            name='fee',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='time_trackers', to='main.Fee'),
        ),
    ]
