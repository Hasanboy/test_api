# Generated by Django 2.2.11 on 2020-05-13 16:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0064_assignment_contract_config'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='assignment',
            name='payment_date',
        ),
        migrations.RemoveField(
            model_name='assignment',
            name='success_fee',
        ),
        migrations.RemoveField(
            model_name='assignmentrate',
            name='contract',
        ),
        migrations.AddField(
            model_name='assignment',
            name='fixed_fee_pre_amount',
            field=models.DecimalField(decimal_places=2, max_digits=11, null=True),
        ),
        migrations.AddField(
            model_name='assignmentrate',
            name='contract_config',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='contract_rates', to='main.ContractConfig'),
        ),
        migrations.AlterField(
            model_name='contractconfig',
            name='contract',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contract_configs', to='main.Contract'),
        ),
        migrations.AlterField(
            model_name='contractconfig',
            name='external_assignment_name',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='contractconfig',
            name='internal_assignment_name',
            field=models.CharField(max_length=255),
        ),
    ]
