# Generated by Django 2.1.2 on 2020-02-12 16:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20200212_1146'),
    ]

    operations = [
        migrations.CreateModel(
            name='OutsourceContact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=500)),
                ('email', models.CharField(max_length=500)),
                ('phone', models.CharField(max_length=500)),
                ('position', models.TextField()),
                ('outsource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='outsource_contacts', to='main.Outsource')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
