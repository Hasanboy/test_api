# Generated by Django 2.1.2 on 2020-02-24 14:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0023_auto_20200224_1310'),
    ]

    operations = [
        migrations.RenameField(
            model_name='timesheet',
            old_name='assigment',
            new_name='assignment',
        ),
    ]
