from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest


class OutSourceTest(BaseTest):
    list_url = 'main:outsource-list'
    detail_url = 'main:outsource-detail'

    def test_create(self):
        data = dict(
            email='daler@mail.ru',
            name='Hasanboy',
            password='123',
            address='Street 20',
            contacts=[
                dict(
                    name='Clinet1 name',
                    email='email@mail.ru',
                    phone='Clinet1 phone',
                    position='Clinet1 position',
                ),
                dict(
                    name='Clinet2 name',
                    email='email@mail.ru',
                    phone='Clinet2 phone',
                    position='Clinet2 position',
                )
            ],
            tags=[],

        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return response.data['id']

    def test_create_min(self):
        data = dict(
            email='daler@mail.ru',
            name='Hasanboy',
            password='123',
            address='Street 20',
            tags=[],

        )
        response = self.client.post(reverse(self.list_url), data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return response.data['id']

    def test_update(self):
        pk = self.test_create()
        data = dict(
            pk=pk,
            email='daler@mail.ru',
            name='Daler',
            address='Street 20',
            contacts=[
                dict(
                    id=1,
                    name='Clinet1 name',
                    email='Clinet1 email',
                    phone='Clinet1 phone',
                    position='Clinet1 position',
                ),
                dict(
                    id=2,
                    name='Clinet2 name',
                    email='Clinet2 email',
                    phone='Clinet2 phone',
                    position='Clinet2 position',
                ),
                dict(
                    name='Clinet3 name',
                    email='Clinet3 email',
                    phone='Clinet3 phone',
                    position='Clinet3 position',

                )
            ],
            tags=[],

        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_list(self):
        self.test_create()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_detail(self):
        pk = self.test_create()
        response = self.client.get(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_delete(self):
        pk = self.test_create()
        response = self.client.delete(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED, response.data)
