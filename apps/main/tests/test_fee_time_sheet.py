from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest
from main.models import Fee


class FeeTest(BaseTest):
    list_url = 'main:fee-list'
    detail_url = 'main:fee-detail'
    reject_url = 'main:fee-reject'
    change_status_url = 'main:fee-change-status-time'
    change_approve_url = 'main:fee-approve'

    change_status_approve_url = 'main:fee-change-status-approve'

    def setUp(self):
        super(FeeTest, self).setUp()
        self.set_user('test1@example.com')

    def test_create_whit_time(self):
        data = dict(
            assignment=1,
            description='Hasanboy 21',
            date='2019-01-01'
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)

        return response.data['id']

    def test_create_whit_spent_time(self):
        data = dict(
            assignment=1,
            description='Hasanboy 21',
            date='2019-01-01',
            total_duration=10033,
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)

    def test_change_status(self):
        pk = self.test_create_whit_time()
        response = self.client.post(
            reverse(self.change_status_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_update(self):
        pk = self.test_create_whit_time()
        data = dict(
            assignment=2,
            description='Hasanboy',
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

        response = self.client.post(
            reverse(self.change_status_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

        data = dict(
            assignment=2,
            description='Hasanboy 22',
            date='2019-01-01'
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_create_fail(self):
        self.test_create_whit_time()
        data = dict(
            assignment=1,
            description='Hasanboy',
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_create_fail_new_user(self):
        self.set_user('test2@example.com')
        data = dict(
            assignment=1,
            description='Hasanboy',
            date='2019-01-01'
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)
        data = dict(
            assignment=4,
            description='Hasanboy',
            date='2019-01-01'
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)

    def test_filter_list_by_submited(self):
        data = dict(
            submitted=1

        )
        response = self.client.get(reverse(self.list_url), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    # def test_change_status_approve(self):
    #     data = dict(
    #         status_approvement=Fee.SUBMITTED,
    #         fees=[5]
    #
    #     )
    #     response = self.client.post(reverse(self.change_status_approve_url), data)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_change_status_approve_fail(self):
        data = dict(
            status_approvement=Fee.REJECTED,
            fees=[5]
        )
        response = self.client.post(reverse(self.change_status_approve_url),
                                    data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_reject(self):
        data = [

            {
                'fee': 5,
                'comment': 'Hello'
            },
            {
                'fee': 6,
                'comment': 'Hello'
            }

        ]
        response = self.client.post(reverse(self.reject_url), data)
        fees = Fee.objects.filter(id__in=[5, 6],
                                  status_approvement=Fee.REJECTED).count()
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(fees, 2)

    def test_reject_fail(self):
        data = [

            {
                'fee': 1,
                'comment': 'Hello'
            },
            {
                'fee': 2,
                'comment': 'Hello'
            }

        ]
        response = self.client.post(reverse(self.reject_url), data)
        fees = Fee.objects.filter(id__in=[5, 6],
                                  status_approvement=Fee.REJECTED).count()
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)
        self.assertNotEqual(fees, 2)

    def test_list(self):
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_detail(self):
        pk = self.test_create_whit_time()
        response = self.client.get(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_delete(self):
        pk = self.test_create_whit_time()
        response = self.client.delete(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT,
                         response.data)

    def test_delete_fail(self):
        pk = 9
        response = self.client.delete(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_create_without_assignment(self):
        data = dict(
            description='Hasanboy 21',
            date='2019-01-01',
            total_duration=10033,
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        return response.data['id']

    def test_update_to_assignment_fixed(self):
        pk = self.test_create_without_assignment()
        data = dict(
            assignment=1,
            description='Hasanboy 21',
            date='2019-01-01',
            total_duration=10033,
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_update_to_assignment_hourly(self):
        pk = self.test_create_without_assignment()
        data = dict(
            assignment=2,
            description='Hasanboy 21',
            date='2019-01-01',
            total_duration=10033,
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_update_to_assignment_fixed_fail(self):
        pk = self.test_create_without_assignment()
        self.set_user('test4@example.com')

        data = dict(
            assignment=1,
            description='Hasanboy 21',
            date='2019-01-01',
            total_duration=10033,
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_update_to_without_assignment(self):
        pk = self.test_create_whit_time()
        data = dict(
            description='Hasanboy 21',
            date='2019-01-01',
            total_duration=10033,
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_update_to_fail(self):
        pk = 10
        data = dict(
            assignment=1,
            description='Hasanboy 21',
            date='2019-01-01',
            total_duration=10033,
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

        data = dict(
            status_approvement=Fee.SUBMITTED,
            fees=[10]

        )
        response = self.client.post(reverse(self.change_status_approve_url),
                                    data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)
