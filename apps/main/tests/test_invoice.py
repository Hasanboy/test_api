from rest_framework import status
from rest_framework.reverse import reverse

from core.rest_framework.tests import BaseTest


class InvoiceTest(BaseTest):
    list_url = 'main:invoice-list'
    detail_url = 'main:invoice-detail'
    delivered_url = 'main:invoice-delivered'
    assignment_detail_url = 'main:assignment-detail'

    def setUp(self):
        super(InvoiceTest, self).setUp()
        self.set_user('test1@example.com')

    def test_create_hourly(self):
        data = {
            'client': 1,
            'description': 'Hello',
            'type': 'simple',
            'assignments': [
                {
                    'assignment': 1,
                    'amount': 1000

                },
                {
                    'assignment': 2,
                    'fees': [3, 4],
                    'expenses': [1, 2]
                },

            ],
            'due_date': '2020-02-25',
            'issue_date': '2020-02-25',

        }
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        print(response.data['total_amount'], response.data['balance'])
        # for res in response.data['assignments']:
        #     print(res)
        return response.data['id']

    def test_update_hourly(self):
        pk = self.test_create_hourly()
        data = {
            'client': 1,
            'description': 'Hello',
            'assignments': [
                {
                    'assignment': 1,
                    'amount': 10000

                },
                {
                    'assignment': 2,
                    'fees': [],
                    'expenses': [2]
                },

            ],
            'type': 'simple',
            'due_date': '2020-02-25',
            'issue_date': '2020-02-25',

        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_update_hourly_fail(self):
        pk = self.test_create_hourly()
        data = {
            'client': 1,
            'description': 'Hello',
            'assignments': [
                {
                    'assignment': 1,
                    'fees': [1],
                    'expenses': [6]

                },
                {
                    'assignment': 2,
                    'amount': 25000
                }

            ],
            'type': 'pre_payment',
            'due_date': '2020-02-25',
            'issue_date': '2020-02-25',

        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_list(self):
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_detail(self):
        # pk = self.test_create_hourly()
        pk = 10
        response = self.client.get(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_delete_stat_assignments(self):
        pk = 1
        response = self.client.get(
            reverse(self.assignment_detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

        pk_invoice = self.test_create_hourly()
        pk = 1
        response = self.client.get(
            reverse(self.assignment_detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

        invoice_response = self.client.delete(
            reverse(self.detail_url, kwargs=dict(pk=pk_invoice)))
        self.assertEqual(invoice_response.status_code,
                         status.HTTP_204_NO_CONTENT, invoice_response.data)

        pk = 1
        response = self.client.get(
            reverse(self.assignment_detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    # def test_create_for_balance(self):
    #     data = {
    #         'client': 1,
    #         'description': 'Hello',
    #         'type': 'pre_payment',
    #         'total_amount': 1000,
    #         'due_date': '2020-02-25',
    #         'issue_date': '2020-02-25',
    #
    #     }
    #     response = self.client.post(reverse(self.list_url), data, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
    #     return response.data['id']

    # def test_update_for_balance(self):
    #     pk = self.test_create_for_balance()
    #     data = {
    #         'client': 1,
    #         'description': 'Hello',
    #         'type': 'pre_payment',
    #         'total_amount': 10000,
    #         'due_date': '2020-02-25',
    #         'issue_date': '2020-02-25',
    #
    #     }
    #     response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
    #
    #     self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
    #     return response.data['id']

    def test_create_for_balance_fail(self):
        data = {
            'client': 1,
            'description': 'Hello',
            'type': 'pre_payment',
            'due_date': '2020-02-25',
            'issue_date': '2020-02-25',

        }
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    # def test_create_for_balance_with_contract(self):
    #     data = {
    #         'contract': 1,
    #         'client': 1,
    #         'description': 'Hello',
    #         'type': 'pre_payment',
    #         'total_amount': 1000,
    #         'due_date': '2020-02-25',
    #         'issue_date': '2020-02-25',
    #
    #     }
    #     response = self.client.post(reverse(self.list_url), data, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
    #     return response.data['id']

    def test_create_for_contract_config(self):
        data = {
            'client': 1,
            'contract_config': 1,
            'description': 'Hello',
            'type': 'pre_payment',
            'due_date': '2020-02-25',
            'issue_date': '2020-02-25',

        }
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        print(response.data['contract'])

        return response.data['id']

    def test_update_for_contract_config(self):
        pk = self.test_create_for_contract_config()
        data = {
            'client': 1,
            'contract_config': 1,
            'description': 'Hello',
            'type': 'pre_payment',
            'due_date': '2020-02-25',
            'issue_date': '2020-02-25',

        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_delivered(self):
        pk = self.test_create_for_contract_config()
        response = self.client.post(
            reverse(self.delivered_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
