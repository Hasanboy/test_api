from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest


class LogsTest(BaseTest):
    list_url = 'main:logs-list'
    detail_url = 'main:logs-detail'

    def test_create(self):
        data = dict(
            fee=1,
            description='Hasanboy',
        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return response.data['id']

    def test_update(self):
        pk = self.test_create()
        data = dict(
            pk=pk,
            description='Hasanboy',
        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_list(self):
        data = dict(
            fee=1
        )
        self.test_create()
        response = self.client.get(reverse(self.list_url), data)

        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data['count'], 2)

    def test_detail(self):
        pk = self.test_create()
        response = self.client.get(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
