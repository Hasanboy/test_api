from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest


class ContractTest(BaseTest):
    list_url = 'main:rate-list'
    detail_url = 'main:rate-detail'

    def test_create(self):
        data = dict(
            assignment=1,
            position=3,
            amount_per_hour=222,
        )
        response = self.client.post(reverse(self.list_url), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

        data = dict(
            assignment=2,
            position=3,
            amount_per_hour=222,
        )
        response = self.client.post(reverse(self.list_url), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

        return response.data['id']

    def test_create_fail(self):
        self.test_create()
        data = dict(
            assignment=1,
            position=3,
            amount_per_hour=222,
        )
        response = self.client.post(reverse(self.list_url), data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_update(self):
        pk = self.test_create()

        data = dict(
            assignment=2,
            position=3,
            amount_per_hour=2222,
        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)

        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_list(self):
        self.test_create()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_detail(self):
        pk = self.test_create()
        response = self.client.get(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_delete(self):
        pk = self.test_create()
        response = self.client.delete(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)
