from rest_framework import status
from rest_framework.reverse import reverse

from core.rest_framework.tests import BaseTest


class InvoiceStatisticTest(BaseTest):
    invoice_statistics_url = 'main:invoice_statistics-invoice-status'
    invoice_period_url = 'main:invoice_statistics-invoice-period'
    fee_period_period_url = 'main:invoice_statistics-fee-period'

    # fixtures = [
    #     # TODO: write fixtures here
    #     'users.yaml',
    #     'files.yaml',
    #     'branchs.yaml',
    #     'tags.yaml',
    #     'positions.yaml',
    #     'currencies.yaml',
    #     'clients.yaml',
    #     'roles.yaml',
    #
    # ]




    def setUp(self):
        super(InvoiceStatisticTest, self).setUp()
        self.set_user('test1@example.com')

    def test_statistic_status(self):
        data = {
            'from_date': '2016-01-01',
            'to_date': '2018-01-01',
            'all': True,

        }
        response = self.client.get((reverse(self.invoice_statistics_url)),
                                   data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        print(response.data)

    def test_invoice_period(self):
        response = self.client.get((reverse(self.invoice_period_url)))

        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_fee_period(self):
        response = self.client.get((reverse(self.fee_period_period_url)))

        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

        data = {
            'from_date': '2016-01-01',
            'to_date': '2020-02-27',

        }
        response = self.client.get((reverse(self.fee_period_period_url)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
