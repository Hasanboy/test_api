from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest


class CashboxTest(BaseTest):
    list_url = 'main:cashbox-list'
    detail_url = 'main:cashbox-detail'

    def test_create_bank(self):
        data = dict(
            name='Cashbox 1',
            type='bank',
            bank_account=1,
            branch=1
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['bank_account']['id'],
                         data['bank_account'])
        self.assertEqual(response.data['branch']['id'], data['branch'])
        return response.data['id']

    def test_update_bank(self):
        pk = self.test_create_bank()

        data = dict(
            name='Cashbox 1 New',
            type='bank',
            bank_account=1,
            branch=1
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['bank_account']['id'],
                         data['bank_account'])
        self.assertEqual(response.data['branch']['id'], data['branch'])

    def test_create_bank_fail(self):
        self.test_create_bank()

        data = dict(
            name='Cashbox 1',
            type='bank',
            bank_account=1,
            branch=1
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

        data = dict(
            name='Cashbox 1',
            type='bank',
            bank_account=3,
            branch=1
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_create_cash(self):
        data = dict(
            name='Cashbox 2',
            type='cash',
            bank_account=None,
            branch=1,
            currency=1,

        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['branch']['id'], data['branch'])
        self.assertEqual(response.data['currency']['id'], data['currency'])
        return response.data['id']

    def test_update_cash(self):
        pk = self.test_create_cash()

        data = dict(
            name='Cashbox 2 New',
            type='cash',
            branch=2,
            currency=1,

        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['branch']['id'], data['branch'])
        self.assertEqual(response.data['currency']['id'], data['currency'])

    def test_update_cash_fail(self):
        pk = self.test_create_cash()

        data = dict(
            name='Cashbox 2 New',
            type='cash',
            branch=2,
        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_update_bank_to_bank_card(self):
        pk = self.test_create_bank()
        data = dict(
            name='Cashbox 1 New',
            type='bank_card',
            branch=1,
            currency=1,

        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['branch']['id'], data['branch'])
        return response.data['id']

    def test_update_bank_card_to_bank(self):
        pk = self.test_update_bank_to_bank_card()
        data = dict(
            name='Cashbox 2 New',
            type='bank',
            bank_account=1,
            branch=1,
            currency=2,

        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['type'], data['type'])
        self.assertNotEqual(response.data['currency']['id'], data['currency'])
        self.assertEqual(response.data['branch']['id'], data['branch'])

    def test_list(self):
        self.test_create_bank()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_detail(self):
        pk = self.test_create_bank()
        response = self.client.get(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_delete(self):
        pk = self.test_create_bank()
        response = self.client.delete(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED, response.data)
