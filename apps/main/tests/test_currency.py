from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest


class CurrencyTest(BaseTest):
    list_url = 'main:currency-list'
    detail_url = 'main:currency-detail'

    def test_create(self):
        data = dict(
            name='Dollar',
            sign='$',
            rate=4000.2,
        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return response.data['id']

    def test_update(self):
        pk = self.test_create()

        data = dict(
            name='Dollar',
            sign='$',
            rate=4000,
        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_update_pc(self):
        pk = 1
        data = dict(
            name='Dollar',
            sign='$',
            rate=4000,
            is_primary=True,
        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_create_fail(self):
        self.test_update()
        data = dict(
            name='EURO',
            sign='&',
            is_primary=True,
        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_list(self):
        self.test_create()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_detail(self):
        pk = self.test_create()
        response = self.client.get(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_delete(self):
        pk = self.test_create()
        response = self.client.delete(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

    def test_delete_fail(self):
        pk = 1
        response = self.client.delete(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
