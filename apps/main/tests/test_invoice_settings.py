from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest
from main.models import InvoiceSettings


class InvoiceSettingsTest(BaseTest):
    list_url = 'main:invoice_recurring-list'
    detail_url = 'main:invoice_recurring-detail'

    def test_create(self):
        data = dict(
            client=1,
            issue_date='2019-01-01',
            amount=20000,
            frequency_type=InvoiceSettings.MONTH,
            frequency_interval=2,
        )
        response = self.client.post(reverse(self.list_url), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return response.data['id']

    # def test_update(self):
    #     pk = self.test_create()
    #     data = dict(
    #         pk=pk,
    #         client=1,
    #         issue_date='2019-01-01',
    #         frequency_issue=20,
    #         frequency_count=20,
    #         amount=20000,
    #     )
    #     response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_list(self):
        self.test_create()
        response = self.client.get(reverse(self.list_url))

        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data['count'], 1)

    def test_detail(self):
        pk = self.test_create()
        response = self.client.get(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
