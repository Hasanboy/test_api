from rest_framework import status
from rest_framework.reverse import reverse

from core.rest_framework.tests import BaseTest


class InvoicePaymentTest(BaseTest):
    payment_url = 'main:invoice-payment'

    def setUp(self):
        super(InvoicePaymentTest, self).setUp()
        self.set_user('test1@example.com')

    def test_payment(self):
        data = {
            'payment_date': '2019-01-01',
            'amount': 25000,
            'notes': 'Hello',
        }
        response = self.client.post(
            reverse(self.payment_url, kwargs=dict(pk=1)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_payment_contract_config(self):
        data = {
            'payment_date': '2019-01-01',
            'amount': 600,
            'notes': 'Hello',
        }
        response = self.client.post(
            reverse(self.payment_url, kwargs=dict(pk=13)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    # def test_payment_balance_general(self):
    #     balance = ClientBalance.objects.filter(client=1, type=ClientBalance.GENERAL).first()
    #     invoice = \
    #         Invoice.objects.filter(client=1,
    #                                status_payment__in=[Invoice.UNPAID, Invoice.PARTIALLY]).first()
    #     invoice_last = \
    #         Invoice.objects.filter(id=1)[0]
    #
    #     data = {
    #         'payment_date': '2019-01-01',
    #         'amount': 300,
    #         'notes': 'Hello',
    #     }
    #     response = self.client.post(reverse(self.payment_url, kwargs=dict(pk=9)), data)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
    #
    #     invoice.refresh_from_db()
    #     balance.refresh_from_db()
    #
    #     invoice_last.refresh_from_db()
