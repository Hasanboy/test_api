from decimal import Decimal

from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest
from main.models import Assignment, ContractConfig


class AssignmentTest(BaseTest):
    list_url = 'main:assignment-list'
    list_new_url = 'main:assignment-submitted'
    detail_url = 'main:assignment-detail'
    completed_url = 'main:assignment-complete'
    assignment_pure_url = 'main:assignment-assignment-pure'

    def test_create_simple(self):
        data = dict(
            name='assignment',
            created_date='3112-12-31',
            dead_line='3112-12-31',
            client=1,
            branch=1,
            originated_by=1,
            team_leader=1,
            tags=[1, 2],
            work_group=[1],
        )
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['created_date'], data['created_date'])
        self.assertEqual(response.data['dead_line'], data['dead_line'])
        self.assertEqual(response.data['client']['id'], data['client'])
        self.assertEqual(response.data['branch']['id'], data['branch'])
        self.assertEqual(response.data['originated_by']['id'],
                         data['originated_by'])
        self.assertEqual(response.data['team_leader']['id'],
                         data['team_leader'])
        self.assertEqual(response.data['is_billable'], False)
        return response.data['id']

    def test_update_simple(self):
        pk = self.test_create_simple()
        data = dict(
            id=pk,
            name='assignment',
            created_date='3112-12-31',
            dead_line='3112-12-31',
            client=1,
            branch=1,
            originated_by=1,
            team_leader=1,
            tags=[1, 2],
            work_group=[1, 2],

        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['created_date'], data['created_date'])
        self.assertEqual(response.data['dead_line'], data['dead_line'])
        self.assertEqual(response.data['client']['id'], data['client'])
        self.assertEqual(response.data['branch']['id'], data['branch'])
        self.assertEqual(response.data['originated_by']['id'],
                         data['originated_by'])
        self.assertEqual(response.data['team_leader']['id'],
                         data['team_leader'])
        self.assertEqual(response.data['is_billable'], False)

    def test_update_simple_fail(self):
        pk = self.test_create_simple()
        data = dict(
            id=pk,
            name='assignment',
            created_date='3112-12-31',
            contract_config=2,
            dead_line='3112-12-31',
            client=1,
            branch=1,
            team_leader=1,
            tags=[1, 2],
            work_group=[1, 2],

        )
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_detail(self):
        pk = 1
        response = self.client.get(
            reverse(self.detail_url, kwargs=dict(pk=pk)))

        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_list(self):
        self.set_user('test4@example.com')
        data = {
            'all': 'true',
            'contract': 1
        }
        response = self.client.get(reverse(self.list_url), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        response = self.client.get(reverse(self.list_new_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_assignment_pure(self):
        self.set_user('test4@example.com')
        data = {
            'all': 'true',
        }
        response = self.client.get(reverse(self.assignment_pure_url), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_delete(self):
        pk = self.test_create_simple()
        response = self.client.delete(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT,
                         response.data)

    def test_create_with_hourly_billing_with_contract(self):
        data = {
            'contract_config': 3,
            'name': 'assignment',
            'created_date': '3112-12-31',
            'dead_line': '3112-12-31',
            'client': 2,
            'branch': 2,
            'originated_by': 1,
            'team_leader': 1,
            'tags': [1, 2],
            'work_group': [1],
            'is_billable': True,
            'billing_type': Assignment.HOURLY_BILLING,
            'hourly_has_fee_ceiling': True,
            'hourly_fee_ceiling': 123123,
            'payment_destination': 1,
            'currency': 2,
            'bank_account': 2,
            'payment_duration': 30,
        }
        contract_config = ContractConfig.objects.filter(id=3).first()
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        self.assertEqual(response.data['name'],
                         contract_config.internal_assignment_name)
        self.assertEqual(response.data['client']['id'],
                         contract_config.contract.client.id)
        self.assertEqual(response.data['branch']['id'],
                         contract_config.contract.branch.id)
        self.assertEqual(response.data['currency']['id'],
                         contract_config.contract.currency.id)
        self.assertEqual(response.data['bank_account']['id'],
                         contract_config.contract.bank_account.id)
        self.assertEqual(response.data['payment_duration'],
                         contract_config.contract.payment_duration)
        self.assertEqual(response.data['billing_type'],
                         contract_config.billing_type)
        self.assertEqual(response.data['hourly_has_fee_ceiling'],
                         contract_config.hourly_has_fee_ceiling)
        self.assertEqual(response.data['hourly_fee_ceiling'],
                         contract_config.hourly_fee_ceiling)
        self.assertEqual(Decimal(response.data['fixed_fee_amount']),
                         contract_config.fixed_fee_amount)
        self.assertEqual(response.data['fixed_fee_expenses_included_in_fee'],
                         contract_config.fixed_fee_expenses_included_in_fee)

        return response.data['id']

    def test_update_contract_to_another_contract(self):
        pk = self.test_create_with_hourly_billing_with_contract()
        data = {
            'contract_config': 4,
            'name': 'assignment',
            'created_date': '3112-12-31',
            'dead_line': '3112-12-31',
            'client': 2,
            'branch': 2,
            'originated_by': 1,
            'team_leader': 1,
            'tags': [1, 2],
            'work_group': [1],
            'is_billable': True,
            'billing_type': Assignment.HOURLY_BILLING,
            'hourly_has_fee_ceiling': True,
            'hourly_fee_ceiling': 123123,
            'payment_destination': 1,
            'currency': 2,
            'bank_account': 2,
            'payment_duration': 30,

        }
        contract_config = ContractConfig.objects.filter(id=1).first()
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

        self.assertEqual(response.data['name'],
                         contract_config.internal_assignment_name)
        self.assertEqual(response.data['client']['id'],
                         contract_config.contract.client.id)
        self.assertEqual(response.data['branch']['id'],
                         contract_config.contract.branch.id)
        self.assertEqual(response.data['currency']['id'],
                         contract_config.contract.currency.id)
        self.assertEqual(response.data['bank_account']['id'],
                         contract_config.contract.bank_account.id)
        self.assertEqual(response.data['payment_duration'],
                         contract_config.contract.payment_duration)
        self.assertEqual(response.data['billing_type'],
                         contract_config.billing_type)
        self.assertEqual(response.data['team_leader']['id'],
                         contract_config.team_leader.id)
        self.assertEqual(response.data['hourly_has_fee_ceiling'],
                         contract_config.hourly_has_fee_ceiling)
        self.assertEqual(response.data['hourly_fee_ceiling'],
                         contract_config.hourly_fee_ceiling)
        self.assertEqual(Decimal(response.data['fixed_fee_amount']),
                         contract_config.fixed_fee_amount)
        self.assertEqual(response.data['fixed_fee_expenses_included_in_fee'],
                         contract_config.fixed_fee_expenses_included_in_fee)

        data = {
            'contract_config': 3,
            'name': 'assignment',
            'created_date': '3112-12-31',
            'dead_line': '3112-12-31',
            'client': 2,
            'branch': 2,
            'originated_by': 1,
            'team_leader': 1,
            'tags': [1, 2],
            'work_group': [1],
            'is_billable': True,
            'billing_type': Assignment.HOURLY_BILLING,
            'hourly_has_fee_ceiling': True,
            'hourly_fee_ceiling': 123123,
            'payment_destination': 1,
            'currency': 2,
            'bank_account': 2,
            'payment_duration': 30,

        }
        contract_config = ContractConfig.objects.filter(id=2).first()
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

        self.assertEqual(response.data['name'],
                         contract_config.internal_assignment_name)
        self.assertEqual(response.data['client']['id'],
                         contract_config.contract.client.id)
        self.assertEqual(response.data['branch']['id'],
                         contract_config.contract.branch.id)
        self.assertEqual(response.data['currency']['id'],
                         contract_config.contract.currency.id)
        self.assertEqual(response.data['bank_account']['id'],
                         contract_config.contract.bank_account.id)
        self.assertEqual(response.data['payment_duration'],
                         contract_config.contract.payment_duration)
        self.assertEqual(response.data['billing_type'],
                         contract_config.billing_type)
        self.assertEqual(response.data['team_leader']['id'],
                         contract_config.team_leader.id)
        self.assertEqual(response.data['hourly_has_fee_ceiling'],
                         contract_config.hourly_has_fee_ceiling)
        self.assertEqual(response.data['hourly_fee_ceiling'],
                         contract_config.hourly_fee_ceiling)
        self.assertEqual(Decimal(response.data['fixed_fee_amount']),
                         contract_config.fixed_fee_amount)
        self.assertEqual(response.data['fixed_fee_expenses_included_in_fee'],
                         contract_config.fixed_fee_expenses_included_in_fee)

    def test_update_contract_to_another_contract_fail(self):
        pk = self.test_create_with_hourly_billing_with_contract()
        data = {
            'name': 'assignment',
            'created_date': '3112-12-31',
            'dead_line': '3112-12-31',
            'client': 2,
            'branch': 2,
            'originated_by': 1,
            'team_leader': 1,
            'tags': [1, 2],
            'work_group': [1],
            'is_billable': True,
            'billing_type': Assignment.HOURLY_BILLING,
            'hourly_has_fee_ceiling': True,
            'hourly_fee_ceiling': 123123,
            'payment_destination': 1,
            'currency': 2,
            'bank_account': 2,
            'payment_duration': 30,

        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_completed(self):
        pk = self.test_create_with_hourly_billing_with_contract()

        response = self.client.post(
            reverse(self.completed_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_completed_fail(self):
        self.set_user('test2@example.com')
        pk = self.test_create_with_hourly_billing_with_contract()

        response = self.client.post(
            reverse(self.completed_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)
