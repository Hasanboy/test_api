from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest


class TeamTest(BaseTest):
    list_url = 'main:teams-list'
    detail_url = 'main:teams-detail'

    def test_create(self):
        data = dict(
            name='team one',
            team_leader=1,
            branch=1,
            team_members=[1, 2, 3],
        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return response.data['id']

    def test_create_fail(self):
        data = dict(
            name='team one update',
            team_leader=1,
            team_members=[1, 2, 3],
        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_update(self):
        pk = self.test_create()
        data = dict(
            pk=pk,
            name='team update',
            team_leader=1,
            branch=1,
            team_members=[1, 2, 3],

        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_list(self):
        self.test_create()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_detail(self):
        pk = self.test_create()
        response = self.client.get(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_delete(self):
        pk = self.test_create()
        response = self.client.delete(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)
