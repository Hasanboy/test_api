from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest
from main.models import Transaction


class TestTransaction(BaseTest):
    list_url = 'main:transactions-list'
    detail_url = 'main:transactions-detail'
    status_url = 'main:transactions-confirm-or-relocate'

    # TODO: OUT TYPE
    def test_create_others(self):
        data = {
            'cashbox': 1,
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'out',
            'amount': 1000,
            'category': 'rents_utilities',
        }
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['payment_date'], data['payment_date'])
        self.assertEqual(response.data['description'], data['description'])
        self.assertEqual(response.data['cashbox']['id'], data['cashbox'])
        return response.data['id']

    def test_update_others(self):
        pk = self.test_create_others()

        data = {
            'cashbox': 1,
            'payment_date': '2019-01-02',
            'description': 'description new',
            'type': 'out',
            'amount': 1000,
            'category': 'rents_utilities',
        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['payment_date'], data['payment_date'])
        self.assertEqual(response.data['description'], data['description'])
        self.assertEqual(response.data['cashbox']['id'], data['cashbox'])

    def test_create_internal_transfers(self):
        data = {
            'cashbox': 1,
            'to_cashbox': 2,
            'from_branch': 2,
            'to_branch': 2,
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'out',
            'amount': 1000,
            'category': 'internal_expenses',
        }
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['payment_date'], data['payment_date'])
        self.assertEqual(response.data['description'], data['description'])
        self.assertEqual(response.data['cashbox']['id'], data['cashbox'])
        self.assertEqual(response.data['to_cashbox']['id'], data['to_cashbox'])
        self.assertEqual(response.data['from_branch']['id'],
                         data['from_branch'])
        self.assertEqual(response.data['to_branch']['id'], data['to_branch'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        return response.data['id']

    def test_update_internal_transfers(self):
        pk = self.test_create_internal_transfers()
        data = {
            'cashbox': 1,
            'to_cashbox': 2,
            'from_branch': 2,
            'to_branch': 2,
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'out',
            'amount': 1000,
            'category': 'internal_expenses',
        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['payment_date'], data['payment_date'])
        self.assertEqual(response.data['description'], data['description'])
        self.assertEqual(response.data['cashbox']['id'], data['cashbox'])
        self.assertEqual(response.data['to_cashbox']['id'], data['to_cashbox'])
        self.assertEqual(response.data['from_branch']['id'],
                         data['from_branch'])
        self.assertEqual(response.data['to_branch']['id'], data['to_branch'])
        self.assertEqual(response.data['status'], Transaction.PENDING)

    def test_update_internal_transfers_fail(self):
        pk = self.test_create_internal_transfers()
        data = {
            'cashbox': 1,
            'to_cashbox': 2,
            'from_branch': 2,
            'to_branch': 2,
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'in',
            'amount': 1000,
            'category': 'internal_expenses',
        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

        data = {
            'cashbox': 1,
            'to_cashbox': 2,
            'from_branch': 2,
            'to_branch': 2,
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'in',
            'amount': 1000,
            'category': 'remuneration',
        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_create_internal_transfers_fail(self):
        data = {
            'cashbox': 1,
            'amount': 1,
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'out',
            'category': 'internal_expenses',
        }
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

        data = {
            'cashbox': 1,
            'to_cashbox': 2,
            'from_branch': 2,
            'to_branch': 2,
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'out',
            'category': 'internal_expenses',
        }
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_create_remuneration(self):
        data = {
            'cashbox': 1,
            'branch': 1,
            'month': '2019-01-01',
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'out',
            'category': 'remuneration',
            'salaries': [
                {
                    'staff': 1,
                    'salary': 5000
                },
                {
                    'staff': 2,
                    'salary': 5000
                }
            ]
        }
        response = self.client.post(reverse(self.list_url), data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['payment_date'], data['payment_date'])
        self.assertEqual(response.data['description'], data['description'])
        self.assertEqual(response.data['cashbox']['id'], data['cashbox'])
        self.assertEqual(response.data['status'], Transaction.CONFIRM)

        return response.data['id']

    def test_update_remuneration(self):
        pk = self.test_create_remuneration()
        data = {
            'cashbox': 1,
            'branch': 1,
            'month': '2019-01-01',
            'payment_date': '2019-01-01',
            'description': 'description',
            'type': 'out',
            'category': 'remuneration',
            'salaries': [
                {
                    'id': 1,
                    'staff': 1,
                    'salary': 5000
                },
                {
                    'staff': 2,
                    'salary': 5000
                }
            ]
        }
        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)
        self.assertEqual(response.data['type'], data['type'])
        self.assertEqual(response.data['payment_date'], data['payment_date'])
        self.assertEqual(response.data['description'], data['description'])
        self.assertEqual(response.data['cashbox']['id'], data['cashbox'])
        self.assertEqual(response.data['status'], Transaction.CONFIRM)

    def test_list(self):
        self.test_create_others()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_detail(self):
        pk = self.test_create_others()
        response = self.client.get(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_delete(self):
        pk = self.test_create_others()
        response = self.client.delete(
            reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT,
                         response.data)

    def test_relocate(self):
        pk = self.test_create_internal_transfers()
        data = {
            "status": 'relocate',
            "cashbox": 1
        }
        response = self.client.post(
            reverse(self.status_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

        data = {
            "status": 'confirm',
        }
        response = self.client.post(
            reverse(self.status_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         response.data)

    def test_relocate_fail(self):
        pk = self.test_create_others()
        data = {
            "status": 'relocate',
            "cashbox": 1
        }
        response = self.client.post(
            reverse(self.status_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)

    def test_create_payment_invoice(self):
        data = dict(
            cashbox=1,
            invoice=1,
            amount=5000,
            payment_date='2020-02-20',
            description='2222222',
            type=Transaction.IN,
            category=Transaction.PAYMENT,
        )

        response = self.client.post(
            reverse(self.list_url), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         response.data)
        return response.data['id']

    def test_update_payment_invoice(self):
        pk = self.test_create_payment_invoice()
        data = dict(
            cashbox=1,
            invoice=1,
            amount=5000,
            payment_date='2020-02-20',
            description='2222222',
            type=Transaction.IN,
            category=Transaction.PAYMENT,
        )

        response = self.client.put(
            reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                         response.data)
