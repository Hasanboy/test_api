from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest
from main.models import ContractConfig


class ContractTest(BaseTest):
    list_url = 'main:contract-list'
    detail_url = 'main:contract-detail'
    next_contract_url = 'main:contract-next-contract'

    def test_create(self):
        data = dict(
            client=1,
            branch=1,
            currency=1,
            bank_account=1,
            payment_duration=20,
            is_multiple=False,
            configs=[
                dict(
                    external_assignment_name='test 2',
                    internal_assignment_name='test internal',
                    team_leader=1,
                    billing_type=ContractConfig.FIXED_FEE,
                    fixed_fee_amount=100,
                    fixed_fee_pre_amount=100,
                    fixed_fee_expenses_included_in_fee=True

                ),
                dict(
                    external_assignment_name='test 3',
                    internal_assignment_name='test internal 2',
                    team_leader=1,
                    billing_type=ContractConfig.HOURLY_BILLING,
                    hourly_fee_ceiling=100,
                    hourly_has_fee_ceiling=True,
                    rates=[
                        {
                            'position': 1,
                            'amount_per_hour': 4000
                        },
                        {
                            'position': 2,
                            'amount_per_hour': 4000
                        },
                        {
                            'position': 3,
                            'amount_per_hour': 4000
                        }

                    ]
                )
            ]

        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return response.data['id']

    def test_update(self):
        pk = self.test_create()
        data = dict(
            client=2,
            branch=2,
            currency=1,
            bank_account=1,
            payment_duration=20,
            is_multiple=True,
            configs=[
                dict(
                    id=5,
                    external_assignment_name='test 2',
                    internal_assignment_name='test internal',
                    team_leader=1,
                    billing_type=ContractConfig.FIXED_FEE,
                    fixed_fee_amount=100,
                    fixed_fee_pre_amount=1750,
                    fixed_fee_expenses_included_in_fee=True

                ),
                dict(
                    id=6,
                    external_assignment_name='test 2',
                    internal_assignment_name='test internal 2',
                    team_leader=1,
                    billing_type=ContractConfig.HOURLY_BILLING,
                    hourly_fee_ceiling=100,
                    hourly_has_fee_ceiling=True,
                    rates=[
                        {
                            'position': 1,
                            'amount_per_hour': 4000
                        },
                        {
                            'position': 2,
                            'amount_per_hour': 4000
                        },
                        {
                            'position': 3,
                            'amount_per_hour': 4000
                        }

                    ]
                ),
                dict(
                    external_assignment_name='test 3',
                    internal_assignment_name='test internal 3',
                    team_leader=1,
                    billing_type=ContractConfig.HOURLY_BILLING,
                    hourly_fee_ceiling=100,
                    hourly_has_fee_ceiling=True,
                    rates=[
                        {
                            'position': 1,
                            'amount_per_hour': 4000
                        },
                        {
                            'position': 2,
                            'amount_per_hour': 4000
                        },
                        {
                            'position': 3,
                            'amount_per_hour': 4000
                        }

                    ]
                ),
                dict(
                    external_assignment_name='test 2',
                    internal_assignment_name='test internal',
                    team_leader=1,
                    billing_type=ContractConfig.FIXED_FEE,
                    fixed_fee_amount=100,
                    fixed_fee_pre_amount=120,
                    fixed_fee_expenses_included_in_fee=True

                ),
            ]
        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_update_related_assignment(self):
        pk = 1
        data = dict(
            client=2,
            branch=2,
            currency=1,
            bank_account=1,
            payment_duration=20,
            is_multiple=True,
            configs=[
                dict(
                    id=1,
                    external_assignment_name='test new internal',
                    internal_assignment_name='test new internal',
                    team_leader=1,
                    billing_type=ContractConfig.FIXED_FEE,
                    fixed_fee_amount=100,
                    fixed_fee_pre_amount=100,
                    fixed_fee_expenses_included_in_fee=True
                ),
                dict(
                    id=2,
                    external_assignment_name='test 3',
                    internal_assignment_name='test internal 3',
                    team_leader=1,
                    billing_type=ContractConfig.HOURLY_BILLING,
                    hourly_fee_ceiling=100,
                    hourly_has_fee_ceiling=True,
                    rates=[
                        {
                            'position': 1,
                            'amount_per_hour': 4000
                        },
                        {
                            'position': 2,
                            'amount_per_hour': 4000
                        },
                        {
                            'position': 3,
                            'amount_per_hour': 4000
                        }

                    ]
                )
            ]
        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_create_fail(self):
        data = dict(
            client=1,
            branch=1,
            bank_account=1,
            payment_duration=20,

        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_list(self):
        self.test_update()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        # for res in response.data['results']:
        #     for ass in res['configs']:
        #         print(ass['assignment'])

    def test_detail(self):
        pk = self.test_create()
        response = self.client.get(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_delete(self):
        pk = self.test_create()
        response = self.client.delete(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

    def test_next_contract(self):
        data = {
            'branch': 2
        }
        response = self.client.post(reverse(self.next_contract_url), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
