from rest_framework import status
from rest_framework.reverse import reverse

from core.rest_framework.tests import BaseTest


class PreInvoiceTest(BaseTest):
    pre_invoice_url = 'main:pre_invoice-list'

    def setUp(self):
        super().setUp()
        self.set_user('test1@example.com')

    def test_pre_invoice_all(self):
        data = {
            'client': 1,
            'assignments': [1, 2],
            'from_date': '2020-02-25',
            'to_date': '2020-02-27',
            'type': 'all'

        }
        response = self.client.post(reverse(self.pre_invoice_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_pre_invoice_custom(self):
        data = {
            'client': 1,
            'assignments': [1, 2],
            'from_date': '2020-02-25',
            'to_date': '2020-02-27',
            'type': 'custom'

        }
        response = self.client.post(reverse(self.pre_invoice_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_pre_invoice_expense(self):
        data = {
            'client': 1,
            'assignments': [1, 2],
            'from_date': '2020-02-25',
            'to_date': '2020-02-27',
            'type': 'expenses'

        }
        response = self.client.post(reverse(self.pre_invoice_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
