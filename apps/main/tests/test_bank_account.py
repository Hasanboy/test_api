from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest


class BankAccountTest(BaseTest):
    list_url = 'main:bank_account-list'
    detail_url = 'main:bank_account-detail'

    def test_create(self):
        data = dict(
            branch=1,
            currency=1,
            name='Hasanboy',
            address='Hasanboy',
            bank_details='Hasanboy',
        )
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        return response.data['id']

    def test_update(self):
        pk = self.test_create()

        data = dict(
            branch=1,
            currency=1,
            name='Hasanboy',
            code='Hasanboy',
            address='Hasanboy',
            bank_details='Hasanboy',
        )
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)), data)

        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_list(self):
        self.test_create()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_detail(self):
        pk = self.test_create()
        response = self.client.get(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_delete(self):
        pk = self.test_create()
        response = self.client.delete(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED, response.data)
