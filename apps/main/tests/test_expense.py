from django.urls import reverse
from rest_framework import status

from core.rest_framework.tests import BaseTest
from main.models import Expense


class ExpenseTest(BaseTest):
    list_url = 'main:expense-list'
    detail_url = 'main:expense-detail'
    reject_url = 'main:expense-reject'
    change_status_approve_url = 'main:expense-change-status-approve'

    def setUp(self):
        super(ExpenseTest, self).setUp()
        self.set_user('test1@example.com')

    def test_create_bulk_with_outsource(self):
        data = {
            'assignment': 1,
            'outsource': 1,
            'cashier': 1,
            'expenses': [
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                },
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                },
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                }
            ],
        }
        response = self.client.post(reverse(self.list_url), data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_create_bulk(self):
        data = {
            'assignment': "1",
            'cashier': 1,
            'expenses': [
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                },
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                },
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                }
            ],
        }
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_create_bulk_fail(self):
        data = {
            'assignment': 1,
            'expenses': [
                {
                    'amount': 1000,
                    'description': "Hello",
                },
                {
                    'amount': 1000,
                    'description': "Hello",
                },
                {
                    'amount': 1000,
                    'description': "Hello",
                }
            ],

        }
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

        data = {
            'assignment': 1,
            'date': "2010-01-01",
        }
        response = self.client.post(reverse(self.list_url), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_create_bulk_cashier_fail(self):
        data = {
            'assignment': 1,
            'cashier': 2,
            'expenses': [
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                },
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                },
                {
                    'amount': 1000,
                    'description': "Hello",
                    'date': "2010-01-01",

                }
            ],

        }
        response = self.client.post(reverse(self.list_url), data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_reject(self):
        data = [

            {
                'expense': 8,
                'comment': 'Hello'
            },
            {
                'expense': 9,
                'comment': 'Hello'
            }

        ]
        response = self.client.post(reverse(self.reject_url), data)
        expenses_count = Expense.objects.filter(id__in=[8, 9], status_approvement=Expense.REJECTED).count()
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(expenses_count, 2)

    def test_reject_fail(self):
        data = [

            {
                'expense': 1,
                'comment': 'Hello'
            },
            {
                'expense': 2,
                'comment': 'Hello'
            }

        ]
        response = self.client.post(reverse(self.reject_url), data)
        expenses_count = Expense.objects.filter(id__in=[5, 6], status_approvement=Expense.REJECTED).count()
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertNotEqual(expenses_count, 2)

    def test_list(self):
        self.test_create_bulk()
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_update_fail(self):
        self.test_create_bulk()
        pk = Expense.objects.all().last().id
        response = self.client.put(reverse(self.detail_url, kwargs=dict(pk=pk)))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_change_status_approve(self):
        data = dict(
            status_approvement=Expense.APPROVED,
            expenses=[8, 9]

        )
        response = self.client.post(reverse(self.change_status_approve_url), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_change_status_approve_fail(self):
        data = dict(
            status_approvement=Expense.REJECTED,
            expenses=[8, 9]

        )
        response = self.client.post(reverse(self.change_status_approve_url), data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)

    def test_delete(self):
        self.test_create_bulk()
        response = self.client.delete(reverse(self.detail_url, kwargs=dict(pk=1)))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
