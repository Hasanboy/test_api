from django_filters.rest_framework import FilterSet, CharFilter
from main.models import Expense


class ExpenseFilterSet(FilterSet):
    submitted = CharFilter(method='get_submitted')
    # all = CharFilter(method='get_all')
    status_approvement = CharFilter(method='filter_status_approvement')

    class Meta:
        model = Expense
        fields = ['assignment', 'cashier', 'user']

    def get_submitted(self, query, name, value: str):
        return query.filter(assignment=value, status_approvement=Expense.SUBMITTED)

    def get_all(self, query, name, value: str):
        if value == 'true':
            return Expense.objects.all()
        return query

    def filter_status_approvement(self, query, name, value: str):
        value_list = value.split('-')
        if value_list:
            return query.filter(status_approvement__in=value_list)
        return query
