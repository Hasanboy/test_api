from django_filters.rest_framework import CharFilter, NumberFilter

from core.rest_framework.filter import BaseFilter
from main.models import Assignment


class AssignmentFilterSet(BaseFilter):
    work_group = CharFilter(method='get_work_group')
    contract = NumberFilter(field_name='contract_config__contract')

    class Meta:
        model = Assignment
        fields = ['team_leader', 'client', 'status', 'is_billable', 'branch']

    def get_work_group(self, query, name, value: str):
        if value:
            return query.filter(work_group__id=value)
        return query
