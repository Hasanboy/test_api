from django_filters import NumberFilter
from django_filters.rest_framework import FilterSet
from main.models import Currency


class CurrencyFilterSet(FilterSet):
    bank_account = NumberFilter(field_name='bank_accounts__id')

    class Meta:
        model = Currency
        fields = ['bank_account']
