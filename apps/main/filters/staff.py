from django_filters.rest_framework import CharFilter, NumberFilter

from core.rest_framework.filter import BaseFilter
from main.models import User, CashBox


class StaffFilterSet(BaseFilter):
    cashbox = CharFilter(method='get_cashier')

    class Meta:
        model = User
        fields = ['branch']

    def get_cashier(self, query, name, value: str):
        if value == 'true':
            cashier_list = []
            for quer in query:
                cashbox = CashBox.objects.filter(cashier=quer)
                if not cashbox:
                    cashier_list.append(quer.id)
            return query.exclude(id__in=cashier_list)

        else:
            return query
