from django_filters.rest_framework import FilterSet, CharFilter
from main.models import Transaction


class TransactionFilterSet(FilterSet):
    status = CharFilter(method='filter_statust')

    class Meta:
        model = Transaction
        fields = ['cashbox', 'created_by', 'type', 'category']

    def filter_statust(self, query, name, value: str):
        value_list = value.split('-')
        if value_list:
            return query.filter(status__in=value_list)
        return query
