from core.rest_framework.filter import BaseFilter
from main.models import Client


class ClientFilterSet(BaseFilter):
    class Meta:
        model = Client
        fields = ['tags']
