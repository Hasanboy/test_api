from django_filters.rest_framework import FilterSet, CharFilter, DateFilter
from main.models import Fee


class FeeFilterSet(FilterSet):
    submitted = CharFilter(method='get_submitted')
    status_approvement = CharFilter(method='filter_status_approvement')
    from_date = DateFilter(field_name='date', lookup_expr='date__gte')
    to_date = DateFilter(field_name='date', lookup_expr='date__lte')

    class Meta:
        model = Fee
        fields = ['assignment', 'user', 'date']

    def get_submitted(self, query, name, value: str):
        return query.filter(assignment=value, status_approvement=Fee.SUBMITTED)

    def filter_status_approvement(self, query, name, value: str):
        value_list = value.split('-')
        if value_list:
            return query.filter(status_approvement__in=value_list)
        return query
