from django.db import models
from django.db.models.functions import Coalesce

from core.django.queryset import BaseQuerySet, ManagerMixin
from django.db.models import Manager, Sum, Q, OuterRef, Subquery


class QuerySet(BaseQuerySet):
    # def hours(self):
    #     queryset = self
    #     print(queryset)
    #     queryset = queryset.annotate(
    #         uninvoiced_hours=Sum('fees__spent_time', filter=Q(fees__status_payment='unpaid', fees__is_delete=False)),
    #         invoiced_hours=Sum('fees__spent_time', filter=Q(fees__status_payment='paid', fees__is_delete=False)),
    #         uninvoiced_amount=Sum('expenses__amount',
    #                               filter=Q(expenses__status_payment='unpaid', expenses__is_delete=False)),
    #         invoiced_amount=Sum('expenses__amount',
    #                             filter=Q(expenses__status_payment='paid', expenses__is_delete=False)),
    #     )
    #
    #     return queryset

    def get_invoices_type(self):
        from main.models import Fee, Expense

        fee_paid_hours = Fee.objects.filter(assignment=OuterRef('pk'), status_invoice=Fee.CLOSED,
                                            status_approvement=Fee.APPROVED)
        fee_paid_hours = fee_paid_hours.values(total=Sum('total_duration')).order_by()
        fee_paid_hours.query.group_by = []

        fee_unpaid_hours = Fee.objects.filter(assignment=OuterRef('pk'), status_invoice=Fee.OPEN,
                                              status_approvement=Fee.APPROVED)
        fee_unpaid_hours = fee_unpaid_hours.values(total=Sum('total_duration')).order_by()
        fee_unpaid_hours.query.group_by = []

        fee_paid_amount = Fee.objects.filter(assignment=OuterRef('pk'), status_invoice=Fee.CLOSED,
                                             status_approvement=Fee.APPROVED)
        fee_paid_amount = fee_paid_amount.values(total=Sum('amount')).order_by()
        fee_paid_amount.query.group_by = []

        fee_unpaid_amount = Fee.objects.filter(assignment=OuterRef('pk'), status_invoice=Fee.OPEN,
                                               status_approvement=Fee.APPROVED)
        fee_unpaid_amount = fee_unpaid_amount.values(total=Sum('amount')).order_by()
        fee_unpaid_amount.query.group_by = []

        expense_paid_amount = Expense.objects.filter(assignment=OuterRef('pk'), status_invoice=Expense.CLOSED,
                                                     status_approvement=Expense.APPROVED)
        expense_paid_amount = expense_paid_amount.values(total=Sum('amount')).order_by()
        expense_paid_amount.query.group_by = []

        expense_unpaid_amount = Expense.objects.filter(assignment=OuterRef('pk'), status_invoice=Expense.OPEN,
                                                       status_approvement=Expense.APPROVED)
        expense_unpaid_amount = expense_unpaid_amount.values(total=Sum('amount')).order_by()
        expense_unpaid_amount.query.group_by = []

        queryset = self
        queryset = queryset.annotate(
            invoice_expense_amount=Coalesce(Subquery(expense_paid_amount[:1],
                                                     models.DecimalField(max_digits=11, decimal_places=2)), 0),
            uninvoice_expense_amount=Coalesce(Subquery(expense_unpaid_amount[:1],
                                                       models.DecimalField(max_digits=11, decimal_places=2)), 0),
            invoice_fee_amount=Coalesce(Subquery(fee_paid_amount[:1],
                                                 models.DecimalField(max_digits=11, decimal_places=2)), 0),
            uninvoice_fee_amount=Coalesce(Subquery(fee_unpaid_amount[:1],
                                                   models.DecimalField(max_digits=11, decimal_places=2)), 0),
            invoice_fee_hours=Coalesce(Subquery(fee_paid_hours[:1], models.IntegerField()), 0),
            uninvoice_fee_hours=Coalesce(Subquery(fee_unpaid_hours[:1], models.IntegerField()), 0),
        )

        return queryset


class AssignmentManager(ManagerMixin, Manager.from_queryset(QuerySet)):
    def get_uninvoices_time(self, contract_config):
        from main.models import Fee, Expense, Assignment

        fee_paid_hours = Fee.objects.filter(assignment=OuterRef('pk'), status_invoice=Fee.CLOSED,
                                            status_approvement=Fee.APPROVED)
        fee_paid_hours = fee_paid_hours.values(total=Sum('total_duration')).order_by()
        fee_paid_hours.query.group_by = []

        fee_unpaid_hours = Fee.objects.filter(assignment=OuterRef('pk'), status_invoice=Fee.OPEN,
                                              status_approvement=Fee.APPROVED)
        fee_unpaid_hours = fee_unpaid_hours.values(total=Sum('total_duration')).order_by()
        fee_unpaid_hours.query.group_by = []

        fee_paid_amount = Fee.objects.filter(assignment=OuterRef('pk'), status_invoice=Fee.CLOSED,
                                             status_approvement=Fee.APPROVED)
        fee_paid_amount = fee_paid_amount.values(total=Sum('amount')).order_by()
        fee_paid_amount.query.group_by = []

        fee_unpaid_amount = Fee.objects.filter(assignment=OuterRef('pk'), status_invoice=Fee.OPEN,
                                               status_approvement=Fee.APPROVED)
        fee_unpaid_amount = fee_unpaid_amount.values(total=Sum('amount')).order_by()
        fee_unpaid_amount.query.group_by = []

        expense_paid_amount = Expense.objects.filter(assignment=OuterRef('pk'), status_invoice=Expense.CLOSED,
                                                     status_approvement=Expense.APPROVED)
        expense_paid_amount = expense_paid_amount.values(total=Sum('amount')).order_by()
        expense_paid_amount.query.group_by = []

        expense_unpaid_amount = Expense.objects.filter(assignment=OuterRef('pk'), status_invoice=Expense.OPEN,
                                                       status_approvement=Expense.APPROVED)
        expense_unpaid_amount = expense_unpaid_amount.values(total=Sum('amount')).order_by()
        expense_unpaid_amount.query.group_by = []

        assignment = Assignment.objects.annotate(
            invoice_expense_amount=Coalesce(Subquery(expense_paid_amount[:1],
                                                     models.DecimalField(max_digits=11, decimal_places=2)), 0),
            uninvoice_expense_amount=Coalesce(Subquery(expense_unpaid_amount[:1],
                                                       models.DecimalField(max_digits=11, decimal_places=2)), 0),
            invoice_fee_amount=Coalesce(Subquery(fee_paid_amount[:1],
                                                 models.DecimalField(max_digits=11, decimal_places=2)), 0),
            uninvoice_fee_amount=Coalesce(Subquery(fee_unpaid_amount[:1],
                                                   models.DecimalField(max_digits=11, decimal_places=2)), 0),
            invoice_fee_hours=Coalesce(Subquery(fee_paid_hours[:1], models.IntegerField()), 0),
            uninvoice_fee_hours=Coalesce(Subquery(fee_unpaid_hours[:1], models.IntegerField()), 0),
        ).filter(contract_config=contract_config)[0]

        return assignment
