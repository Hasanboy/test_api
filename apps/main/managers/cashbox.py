from django.db import models
from django.db.models.functions import Coalesce

from core.django.queryset import BaseQuerySet, ManagerMixin
from django.db.models import Manager, Sum, OuterRef, Subquery, F


class QuerySet(BaseQuerySet):

    def get_balance(self):
        from main.models import Transaction

        plus = Transaction.objects.filter(cashbox=OuterRef('pk'),
                                          type=Transaction.IN,
                                          status=Transaction.CONFIRM)
        plus = plus.values(
            total=Sum('amount')).order_by()
        plus.query.group_by = []

        minus = Transaction.objects.filter(cashbox=OuterRef('pk'),
                                           type=Transaction.OUT,
                                           status=Transaction.CONFIRM)
        minus = minus.values(
            total=Sum('amount')).order_by()
        minus.query.group_by = []
        queryset = self
        queryset = queryset.annotate(
            plus=Coalesce(Subquery(plus[:1],
                                   models.DecimalField(
                                       max_digits=11,
                                       decimal_places=2)),
                          0),
            minus=Coalesce(Subquery(minus[:1],
                                    models.DecimalField(
                                        max_digits=11,
                                        decimal_places=2)),
                           0),

        )
        queryset = queryset.annotate(
            balance=F('plus') - F(
                'minus'))

        return queryset


class CashBoxManager(ManagerMixin, Manager.from_queryset(QuerySet)):
    pass
