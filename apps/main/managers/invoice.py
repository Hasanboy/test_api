from django.db import models
from django.db.models import Count, Sum, Subquery, IntegerField, Value, \
    OuterRef, F
from django.db.models.functions import TruncDate, TruncMonth
from django.db.models.manager import Manager

from core.django.queryset import BaseQuerySet, ManagerMixin


class QuerySet(BaseQuerySet):
    def stat_invoice_status(self, client=None, from_date=None, to_date=None,
                            assignment=None, all=None, user=None):
        from main.models import Invoice
        queryset = self
        if not all:
            queryset = queryset.filter(user=user)
        if from_date:
            queryset = queryset.filter(issue_date__gte=from_date)
        if to_date:
            queryset = queryset.filter(issue_date__lte=to_date)
        if assignment:
            queryset = queryset.filter(assignment=assignment)
        if client:
            queryset = queryset.filter(status=client)
        queryset = queryset.filter(
            status_payment__in=[Invoice.PAID, Invoice.OPEN])

        if not queryset:
            queryset = Invoice.objects.all()
            queryset = queryset.annotate(
                total_paid_amount=Value(0, models.DecimalField(max_digits=11,
                                                               decimal_places=2)))
            queryset = queryset.values(
                'total_paid_amount',
            )
            queryset.query.group_by = []
        else:
            queryset = queryset.annotate(
                total_all_amount=Sum('pc_amount')).order_by()
            queryset = queryset.annotate(
                total_left_amount=Sum('pc_balance')).order_by()
            queryset = queryset.values(
                'total_all_amount',
                'total_left_amount'
            )
            queryset = queryset.annotate(
                total_paid_amount=F('total_all_amount') - F(
                    'total_left_amount'))
            queryset.query.group_by = []

        total_amount = Invoice.objects.all()
        total_amount = total_amount.values(total=Sum('pc_amount')).order_by()
        total_amount.query.group_by = []

        queryset = queryset.annotate(
            total_amount=Subquery(total_amount[:1],
                                  models.DecimalField(max_digits=11,
                                                      decimal_places=2)),
        )

        return queryset

    def stat_paid_invoice_month(self, from_date=None, to_date=None, all=None,
                                user=None):
        queryset = self

        if not all:
            queryset = queryset.filter(user=user)
        if from_date:
            queryset = queryset.filter(issue_date__gte=from_date)
        if to_date:
            queryset = queryset.filter(issue_date__lte=to_date)

        queryset = queryset.order_by()
        queryset = queryset.annotate(month=TruncMonth('issue_date'))
        queryset = queryset.annotate(total_all_amount=Sum('pc_amount'))
        queryset = queryset.annotate(total_balance=Sum('pc_balance'))
        queryset = queryset.values('month', 'total_all_amount',
                                   'total_balance')
        queryset = queryset.annotate(
            total_paid=F('total_all_amount') - F('total_balance'))
        queryset = queryset.values('month', 'total_all_amount', 'total_paid')
        queryset = queryset.order_by('month')
        return queryset


class InvoiceManager(ManagerMixin, Manager.from_queryset(QuerySet)):

    def check_due_date(self):
        from main.models import Invoice
        from datetime import datetime
        queryset_prepare = Invoice.objects.filter(due_date__lte=datetime.now())

        queryset_prepare.update(status_payment=Invoice.DUE)
        queryset = Invoice.objects.all()
        return queryset

    def create_calculate_amount(self, assignments, invoice, due_date):
        from main.models import Assignment, AssignmentInvoice, Fee, Expense

        total_amount_invoice = 0
        total_amount_pre_invoice = 0
        total_amount_invoice_pc = 0
        assignment_invoice_list = []
        fees_ids = []
        expenses_ids = []
        for assignment in assignments:

            # TODO  FEE
            if assignment[
                'assignment'].billing_type == Assignment.HOURLY_BILLING:
                total_amount = 0
                total_amount_pc = 0
                constant_fee = 1
                constant_fee_pc = 1 * invoice.pc_rate
                fees = assignment.pop('fees', [])
                for fee in fees:
                    total_amount += fee.amount * constant_fee
                    total_amount_pc += fee.amount / constant_fee_pc
                    fees_ids.append(fee.id)

                # TODO EXPENSE
                expenses = assignment.pop('expenses', [])
                for expense in expenses:
                    total_amount += expense.amount
                    total_amount_pc += expense.amount / invoice.pc_rate
                    expenses_ids.append(expense.id)
                assignment_invoice_list.append(AssignmentInvoice(
                    invoice=invoice,
                    assignment=assignment['assignment'],
                    due_date=due_date,
                    total_amount=total_amount,
                    pc_amount=total_amount_pc,
                    description='Hello',
                ))
                invoiced_fee = Fee.objects.filter(id__in=fees_ids)
                invoiced_fee.update(
                    invoice=invoice,
                    status_invoice=Fee.CLOSED,
                    pre_invoice=None,
                )

                invoiced_expense = Expense.objects.filter(id__in=expenses_ids)
                invoiced_expense.update(
                    invoice=invoice,
                    status_invoice=Expense.CLOSED,
                    pre_invoice=None,
                )
                total_amount_invoice += total_amount
                total_amount_invoice_pc += total_amount_pc
            else:
                pre_payment = assignment.pop('pre_payment', None)
                if pre_payment:
                    total_amount_pre_invoice = + pre_payment

                amount = assignment.pop('amount', None)
                total_amount_invoice = +amount
                total_amount_invoice_pc = +amount
                assignment_invoice_list.append(AssignmentInvoice(
                    invoice=invoice,
                    assignment=assignment['assignment'],
                    due_date=due_date,
                    total_amount=amount,
                    pc_amount=amount,
                    description='Hello',
                ))

        invoice.total_amount = total_amount_invoice
        invoice.balance = total_amount_invoice
        invoice.pc_amount = total_amount_invoice_pc
        invoice.pc_balance = total_amount_invoice_pc
        if total_amount_pre_invoice > 0:
            invoice.balance -= total_amount_pre_invoice
        invoice.save()
        AssignmentInvoice.objects.bulk_create(assignment_invoice_list)

    def update_calculate_amount(self, assignments, invoice, due_date):
        from main.models import Assignment, AssignmentInvoice, Fee, Expense

        total_amount_invoice = 0
        total_amount_invoice_pc = 0
        total_amount_pre_invoice = 0

        invoiced_fee = Fee.objects.filter(invoice=invoice)
        invoiced_fee.update(
            invoice=None,
            status_invoice=Fee.OPEN
        )
        invoiced_expense = Expense.objects.filter(invoice=invoice)
        invoiced_expense.update(
            invoice=None,
            status_invoice=Expense.OPEN
        )
        assignment_invoice_ids = []
        fees_ids = []
        expenses_ids = []

        for assignment in assignments:
            if assignment[
                'assignment'].billing_type == Assignment.HOURLY_BILLING:
                total_amount = 0
                total_amount_pc = 0
                constant_fee = 1
                constant_fee_pc = 1 * invoice.pc_rate

                # TODO  FEE
                for fee in assignment['fees']:
                    total_amount += fee.amount * constant_fee
                    total_amount_pc += fee.amount / constant_fee_pc
                    fees_ids.append(fee.id)
                # TODO EXPENSE
                for expense in assignment['expenses']:
                    total_amount += expense.amount
                    total_amount_pc += expense.amount / invoice.pc_rate
                    expenses_ids.append(expense.id)
                assignment_invoice, _ = AssignmentInvoice.objects.update_or_create(
                    invoice=invoice,
                    assignment=assignment['assignment'],
                    defaults=dict(
                        due_date=due_date,
                        total_amount=total_amount,
                        pc_amount=total_amount_pc,
                        description='Hello',
                    ))

                invoiced_fee = Fee.objects.filter(id__in=fees_ids)
                invoiced_fee.update(
                    invoice=invoice,
                    status_invoice=Fee.CLOSED,
                    pre_invoice=None,
                )

                invoiced_expense = Expense.objects.filter(id__in=expenses_ids)
                invoiced_expense.update(
                    invoice=invoice,
                    status_invoice=Expense.CLOSED,
                    pre_invoice=None,
                )
                assignment_invoice_ids.append(assignment_invoice.id)
                total_amount_invoice += total_amount
                total_amount_invoice_pc += total_amount_pc
            else:
                pre_payment = assignment.pop('pre_payment', None)
                if pre_payment:
                    total_amount_pre_invoice = + pre_payment
                amount = assignment.pop('amount', None)
                total_amount_invoice = +amount
                total_amount_invoice_pc = +amount
                assignment_invoice, _ = AssignmentInvoice.objects.update_or_create(
                    invoice=invoice,
                    assignment=assignment['assignment'],
                    defaults=dict(
                        due_date=due_date,
                        total_amount=amount,
                        pc_amount=amount,
                        description='Hello',
                    ))
                assignment_invoice_ids.append(assignment_invoice.id)
        AssignmentInvoice.objects.exclude(
            id__in=assignment_invoice_ids).delete()
        invoice.total_amount = total_amount_invoice
        invoice.balance = total_amount_invoice
        invoice.pc_amount = total_amount_invoice_pc
        invoice.pc_balance = total_amount_invoice_pc
        if total_amount_pre_invoice > 0:
            invoice.balance -= total_amount_pre_invoice
        invoice.save()
