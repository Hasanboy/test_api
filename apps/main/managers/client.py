from django.db import models
from django.db.models.functions import Coalesce

from core.django.queryset import BaseQuerySet, ManagerMixin
from django.db.models import Manager, Sum, OuterRef, Subquery


class QuerySet(BaseQuerySet):

    def get_client_balance(self):
        from main.models import ClientBalance

        balance = ClientBalance.objects.filter(client=OuterRef('pk'), type=ClientBalance.INDIVIDUAL)
        balance = balance.values(total=Sum('balance')).order_by()
        balance.query.group_by = []

        queryset = self
        queryset = queryset.annotate(
            client_balance=Coalesce(Subquery(balance[:1],
                                             models.DecimalField(max_digits=11, decimal_places=2)), 0),

        )

        return queryset


class ClientManager(ManagerMixin, Manager.from_queryset(QuerySet)):
    def new_fn(self):
        pass
