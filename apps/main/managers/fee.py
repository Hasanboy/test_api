from django.db.models.manager import Manager
from django.db.models import Sum, F
from django.db.models.functions import TruncDate

from core.django.queryset import BaseQuerySet, ManagerMixin


class QuerySet(BaseQuerySet):
    def stat_fee_by_date(self, staff=None, from_date=None, to_date=None,
                         all=None, user=None):
        queryset = self
        if not all:
            queryset = queryset.filter(user=user)
        if from_date:
            queryset = queryset.filter(date__gte=from_date)
        if to_date:
            queryset = queryset.filter(date__lte=to_date)

        if staff:
            queryset = queryset.filter(user=staff)

        queryset = queryset.order_by()
        queryset = queryset.annotate(dates=TruncDate('date'))
        queryset = queryset.values('dates')

        queryset = queryset.annotate(total_all_duration=Sum('total_duration'))
        queryset = queryset.values('dates', 'total_all_duration')
        queryset = queryset.order_by('dates')
        return queryset


class FeeTimeSheetManager(ManagerMixin, Manager.from_queryset(QuerySet)):
    def check_expire(self, user):
        from main.models import Fee
        from datetime import datetime
        queryset_prepare = Fee.objects.filter(user=user, status_time=Fee.PLAY,
                                              expired_time__lte=datetime.now())

        queryset_prepare.update(
            status_time=Fee.PAUSED,
            total_duration=F('total_duration') + 540,
            start_time=None,
            end_time=None,
            expired_time=None,
        )
        queryset = Fee.objects.filter(user=user)
        return queryset
