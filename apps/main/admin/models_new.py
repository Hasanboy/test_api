from django.contrib import admin
from main.models import Assignment, Fee, Contract, Expense, Invoice, Client, ClientContact
from django.contrib.auth.models import Permission, Group


@admin.register(Assignment)
class Admin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Client)
class Admin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(ClientContact)
class Admin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Fee)
class Admin(admin.ModelAdmin):
    list_display = ['id']


@admin.register(Contract)
class Admin(admin.ModelAdmin):
    list_display = ['contract_number']


@admin.register(Expense)
class Admin(admin.ModelAdmin):
    list_display = ['id']


@admin.register(Invoice)
class Admin(admin.ModelAdmin):
    list_display = ['id']

@admin.register(Permission)
class Admin(admin.ModelAdmin):
    list_display = ['name']
