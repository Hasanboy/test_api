from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel


class BankAccount(DeleteModel, BaseModel):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255, null=True)
    branch = models.ForeignKey('main.Branch', CASCADE, related_name='bank_accounts', null=True)
    currency = models.ForeignKey('main.Currency', CASCADE, related_name='bank_accounts', null=True)
    address = models.CharField(max_length=500, null=True)
    bank_details = models.TextField(null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
