from django.db import models

from core.django.model import DeleteModel

from django.contrib.contenttypes.models import ContentType


class Tags(DeleteModel, models.Model):
    name = models.CharField(max_length=500)

    model = models.ForeignKey(ContentType, models.CASCADE,
                              related_name='content_type', null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
