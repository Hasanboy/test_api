from django.db import models

from core.django.model import DeleteModel, BaseModel
from main.managers.cashbox import CashBoxManager


class CashBox(DeleteModel, BaseModel):
    BANK = 'bank'
    CASH = 'cash'
    BANK_CARD = 'bank_card'
    TYPES = (
        (BANK, 'Bank'),
        (CASH, 'Cash'),
        (BANK_CARD, 'Bank card'),
    )
    cashier = models.ForeignKey('main.User', on_delete=models.PROTECT)
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=40, choices=TYPES)
    currency = models.ForeignKey('main.Currency', on_delete=models.PROTECT)
    branch = models.ForeignKey('main.Branch', on_delete=models.PROTECT,
                               related_name='cashboxes')
    bank_account = models.OneToOneField('main.BankAccount',
                                        on_delete=models.PROTECT,
                                        related_name='cashboxes',
                                        null=True)

    objects = CashBoxManager()

    class Meta:
        ordering = ['-id']
        unique_together = ['bank_account']

    def __str__(self):
        return self.name
