from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel


class PreInvoice(BaseModel):
    ALL = 'all'
    CUSTOM = 'custom'
    EXPENSES = 'expenses'

    TYPE = (
        (ALL, 'all'),
        (CUSTOM, 'custom'),
        (EXPENSES, 'expenses'),
    )
    user = models.ForeignKey('main.User', CASCADE, related_name='pre_invoices')

    client = models.ForeignKey('main.Client', CASCADE, related_name='pre_invoices')
    from_date = models.DateField(null=True)
    to_date = models.DateField(null=True)
    date = models.DateField(auto_now_add=True)
    type = models.CharField(max_length=50, choices=TYPE)

    class Meta:
        ordering = ['-id']


class AssignmentPreInvoice(DeleteModel, BaseModel):
    pre_invoice = models.ForeignKey('main.PreInvoice', CASCADE, related_name='assignment_preinvoices')
    assignment = models.ForeignKey('main.Assignment', CASCADE, related_name='assignment_preinvoices')

    class Meta:
        ordering = ['-id']
