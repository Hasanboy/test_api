from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel


class Outsource(DeleteModel, BaseModel):
    name = models.CharField(max_length=500)
    email = models.CharField(max_length=500, null=True)
    address = models.TextField(null=True)
    tags = models.ManyToManyField('main.Tags')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']


class OutsourceContact(BaseModel):
    outsource = models.ForeignKey('main.Outsource', CASCADE,
                                  related_name='outsource_contacts')
    name = models.CharField(max_length=500)
    email = models.CharField(max_length=500)
    phone = models.CharField(max_length=500)
    position = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
