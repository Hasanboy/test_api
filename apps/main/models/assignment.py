from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel
from main.managers.assignment import AssignmentManager


class Assignment(DeleteModel, BaseModel):
    owner = models.ForeignKey('main.User', CASCADE, related_name='assignments',
                              null=True)
    name = models.CharField(max_length=500)
    contract_config = models.ForeignKey('main.ContractConfig', CASCADE,
                                        related_name='assignments', null=True)
    client = models.ForeignKey('main.Client', CASCADE,
                               related_name='assignments')
    client_contact = models.ForeignKey('main.ClientContact', CASCADE,
                                       related_name='assignments', null=True)
    service_delivered_to = models.ForeignKey('main.Client', CASCADE,
                                             related_name='deliver_assignments',
                                             null=True)
    branch = models.ForeignKey('main.Branch', CASCADE,
                               related_name='assignments')
    tags = models.ManyToManyField('main.Tags')
    originated_by = models.ForeignKey('main.User', CASCADE,
                                      related_name='origin_assignments')
    team_leader = models.ForeignKey('main.User', CASCADE,
                                    related_name='team_assignment')
    work_group = models.ManyToManyField('main.User')
    created_date = models.DateField()
    dead_line = models.DateField()
    is_billable = models.BooleanField(default=False)

    FIXED_FEE = 'fixed_fee'
    HOURLY_BILLING = 'hourly_billing'

    BILLING_TYPE = (
        (FIXED_FEE, 'fixed fee'),
        (HOURLY_BILLING, 'hourly billing')
    )

    ACTIVE = 'active'
    COMPLETED = 'completed'
    ISSUED = 'issued'

    STATUS_TYPE = (
        (ACTIVE, 'active'),
        (COMPLETED, 'completed'),
        (ISSUED, 'issued')
    )

    payment_destination = models.ForeignKey('main.Branch', CASCADE,
                                            related_name='assignments_payment',
                                            null=True)
    bank_account = models.ForeignKey('main.BankAccount', CASCADE,
                                     related_name='assignments', null=True)
    currency = models.ForeignKey('main.Currency', CASCADE,
                                 related_name='assignments', null=True)
    billing_type = models.CharField(choices=BILLING_TYPE, max_length=255,
                                    null=True)

    # TODO for FIXED_FEE
    fixed_fee_amount = models.DecimalField(max_digits=11, decimal_places=2,
                                           null=True)
    fixed_fee_expenses_included_in_fee = models.BooleanField(default=False)
    fixed_fee_pre_amount = models.DecimalField(max_digits=11, decimal_places=2,
                                               null=True)
    # TODO for HOURLY_BILLING
    hourly_has_fee_ceiling = models.BooleanField(default=False)
    hourly_fee_ceiling = models.DecimalField(max_digits=11, decimal_places=2,
                                             null=True)

    status = models.CharField(max_length=50, choices=STATUS_TYPE,
                              default=ACTIVE)
    payment_duration = models.PositiveSmallIntegerField(null=True)
    objects = AssignmentManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
