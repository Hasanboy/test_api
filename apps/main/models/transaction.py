from django.db import models
from django.db.models import PROTECT

from core.django.model import DeleteModel, BaseModel


class Transaction(DeleteModel, BaseModel):
    INTERNAL = 'internal'
    PAYMENT = 'payment'
    RENTS_UTILITIES = 'rents_utilities'
    STATIONARY_GROCERY = 'stationary_grocery'
    REMUNERATION = 'remuneration'
    BUSINESS_DEVELOPMENT = 'business_development'
    TAX = 'tax'
    INTERNAL_EXPENSES = 'internal_expenses'
    ASSIGNMENT_EXPENSES = 'assignment_expenses'

    CATEGORY_OUT = (
        (RENTS_UTILITIES, 'Rents & utilities'),
        (STATIONARY_GROCERY, 'Stationary $ grocery'),
        (REMUNERATION, 'Remuneration'),
        (BUSINESS_DEVELOPMENT, 'Business & development'),
        (TAX, 'Tax'),
        (INTERNAL_EXPENSES, 'Internal Expenses'),
        (INTERNAL, 'internal'),
        (PAYMENT, 'payment'),
        (ASSIGNMENT_EXPENSES, 'assignment expenses'),

    )
    PENDING = 'pending'
    CONFIRM = 'confirm'
    CANCEL = 'cancel'

    STATUS = (
        (PENDING, 'pending'),
        (CONFIRM, 'confirm'),
        (CANCEL, 'cancel'),
    )

    IN = 'in'
    OUT = 'out'

    TYPE = (
        (IN, 'in'),
        (OUT, 'out'),
    )

    # TODO IN TYPE

    # TODO: INTERNAL_EXPENSES
    expense = models.ForeignKey('main.Expense', PROTECT, 'transactions',
                                null=True)

    # TODO: Remuneration
    branch = models.ForeignKey('main.Branch', PROTECT, 'transactions',
                               null=True)
    month = models.DateField(null=True)

    # TODO: Internal Transfers

    from_branch = models.ForeignKey('main.Branch', PROTECT,
                                    'from_transactions', null=True)
    to_branch = models.ForeignKey('main.Branch', PROTECT, 'to_transactions',
                                  null=True)
    to_cashbox = models.ForeignKey('main.CashBox', PROTECT, 'to_transactions',
                                   null=True)

    # TODO OUT TYPE
    invoice = models.ForeignKey('main.Invoice', PROTECT,
                                related_name='transactions', null=True)
    contract = models.ForeignKey('main.Contract', PROTECT,
                                 related_name='transactions', null=True)

    # TODO: Others
    cashbox = models.ForeignKey('main.CashBox', PROTECT, 'transactions')
    payment_date = models.DateField()
    description = models.CharField(max_length=255, null=True)
    type = models.CharField(max_length=255, choices=TYPE)
    status = models.CharField(max_length=45, choices=STATUS)
    amount = models.DecimalField(max_digits=20, decimal_places=9)
    pc_amount = models.DecimalField(max_digits=20, decimal_places=9, null=True)
    category = models.CharField(max_length=45, choices=CATEGORY_OUT,
                                default=PAYMENT)

    created_by = models.ForeignKey('main.User', PROTECT,
                                   related_name='transactions')

    period_from = models.DateField(null=True)
    period_to = models.DateField(null=True)

    class Meta:
        ordering = ['-id']


class SalaryTransaction(DeleteModel, BaseModel):
    transaction = models.ForeignKey('main.Transaction', PROTECT,
                                    'salary_transactions', )
    staff = models.ForeignKey('main.User', PROTECT,
                              related_name='salary_transactions')
    salary = models.DecimalField(max_digits=11, decimal_places=2)

    class Meta:
        ordering = ['-id']
