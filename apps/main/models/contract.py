from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel


class Contract(DeleteModel, BaseModel):
    contract_number = models.CharField(max_length=255, unique=True, null=True)
    client = models.ForeignKey('main.Client', CASCADE,
                               related_name='contracts')
    branch = models.ForeignKey('main.Branch', CASCADE,
                               related_name='contracts')
    currency = models.ForeignKey('main.Currency', CASCADE,
                                 related_name='contracts')
    bank_account = models.ForeignKey('main.BankAccount', CASCADE,
                                     related_name='contracts')
    payment_duration = models.PositiveSmallIntegerField()
    is_multiple = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)

    class Meta:
        ordering = ['-id']


class ContractConfig(DeleteModel, BaseModel):
    FIXED_FEE = 'fixed_fee'
    HOURLY_BILLING = 'hourly_billing'

    BILLING_TYPE = (
        (FIXED_FEE, 'fixed fee'),
        (HOURLY_BILLING, 'hourly billing')
    )
    contract = models.ForeignKey('main.Contract', CASCADE,
                                 related_name='contract_configs')
    external_assignment_name = models.CharField(max_length=255)
    internal_assignment_name = models.CharField(max_length=255)
    team_leader = models.ForeignKey('main.User', CASCADE,
                                    related_name='team_leader')
    billing_type = models.CharField(max_length=100, choices=BILLING_TYPE)

    fixed_fee_amount = models.DecimalField(max_digits=11, decimal_places=2,
                                           null=True)
    fixed_fee_pre_amount = models.DecimalField(max_digits=11, decimal_places=2,
                                               null=True)
    fixed_fee_expenses_included_in_fee = models.BooleanField(default=False)

    hourly_has_fee_ceiling = models.BooleanField(default=False)
    hourly_fee_ceiling = models.DecimalField(max_digits=11, decimal_places=2,
                                             null=True)
    is_paid = models.BooleanField(default=False)
    is_invoiced = models.BooleanField(default=False)

    class Meta:
        ordering = ['-id']
