from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel
from main.managers.invoice import InvoiceManager


class Invoice(DeleteModel, BaseModel):
    PRE_PAYMENT = 'pre_payment'
    SIMPLE = 'simple'

    TYPE_INVOICE = (
        (PRE_PAYMENT, 'pre payment'),
        (SIMPLE, 'simple'),
    )

    DRAFT = 'draft'
    DELIVERED = 'delivered'
    OPEN = 'open'
    DUE = 'due'
    PAID = 'paid'

    STATUS_PAYMENT = (
        (PAID, 'paid'),
        (DRAFT, 'draft'),
        (DELIVERED, 'delivered'),
        (OPEN, 'open'),
        (DUE, 'due'),
    )
    contract = models.ForeignKey('main.Contract', CASCADE,
                                 related_name='invoices', null=True)
    contract_config = models.ForeignKey('main.ContractConfig', CASCADE,
                                        related_name='invoices', null=True)
    settings = models.ForeignKey('main.InvoiceSettings', CASCADE,
                                 related_name='invoices', null=True)
    user = models.ForeignKey('main.User', CASCADE, related_name='invoices')
    client = models.ForeignKey('main.Client', CASCADE, related_name='invoices')
    total_amount = models.DecimalField(max_digits=11, decimal_places=2)
    pc_amount = models.DecimalField(max_digits=11, decimal_places=2, null=True)
    pc_balance = models.DecimalField(max_digits=11, decimal_places=2,
                                     null=True)
    pc_rate = models.DecimalField(max_digits=11, decimal_places=2, null=True)
    pc_date = models.DateField(null=True)
    due_date = models.DateField()
    delivered_date = models.DateField(null=True)
    issue_date = models.DateField(null=True)
    status_payment = models.CharField(max_length=50, choices=STATUS_PAYMENT,
                                      default=DRAFT)
    type = models.CharField(max_length=50, choices=TYPE_INVOICE,
                            default=SIMPLE)
    balance = models.DecimalField(max_digits=11, decimal_places=2, null=True)
    description = models.TextField()
    objects = InvoiceManager()

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.client.name


class AssignmentInvoice(DeleteModel, BaseModel):
    invoice = models.ForeignKey('main.Invoice', CASCADE,
                                related_name='assignment_invoices')
    assignment = models.ForeignKey('main.Assignment', CASCADE,
                                   related_name='assignment_invoices')
    due_date = models.DateField()
    total_amount = models.DecimalField(max_digits=11, decimal_places=2)
    pc_amount = models.DecimalField(max_digits=11, decimal_places=2, null=True)
    description = models.TextField()

    class Meta:
        ordering = ['-id']


class InvoiceSettings(DeleteModel, BaseModel):
    DAY = 'day'
    MONTH = 'month'
    YEAR = 'year'

    FREQUENCY_TYPE = (
        (DAY, 'day'),
        (MONTH, 'month'),
        (YEAR, 'year'),
    )
    client = models.ForeignKey('main.Client', CASCADE,
                               related_name='invoice_settings')
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    issue_date = models.DateField()
    due_date_interval = models.DateField(null=True)
    next_date_interval = models.DateField(null=True)
    frequency_interval = models.PositiveIntegerField()
    frequency_type = models.CharField(max_length=255, choices=FREQUENCY_TYPE,
                                      default=DAY)
    deactivated = models.BooleanField(default=False)

    class Meta:
        ordering = ['-id']
