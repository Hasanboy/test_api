from django.db import models
from django.db.models import CASCADE

from core.django.model import DeleteModel


class Currency(DeleteModel, models.Model):
    name = models.CharField(max_length=255)
    sign = models.CharField(max_length=20)
    is_primary = models.BooleanField(default=False)
    rate = models.DecimalField(max_digits=11, decimal_places=2, null=True)

    class Meta:
        ordering = ['-id']


class CurrencyHistory(DeleteModel, models.Model):
    currency = models.ForeignKey('main.Currency', on_delete=CASCADE,
                                 related_name='currency_histories')
    rate = models.DecimalField(max_digits=11, decimal_places=2)
    user = models.ForeignKey('main.User', on_delete=CASCADE,
                             related_name='currency_histories')
    created_date = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ['-id']
