from django.db import models
from django.db.models import CASCADE


class ContractRate(models.Model):
    contract_config = models.ForeignKey('main.ContractConfig', CASCADE, related_name='rates', null=True)
    position = models.ForeignKey('main.Position', CASCADE, related_name='contract_rates')
    amount_per_hour = models.DecimalField(max_digits=11, decimal_places=2)

    class Meta:
        unique_together = ['position', 'contract_config']
        ordering = ['-id']


class AssignmentRate(models.Model):
    assignment = models.ForeignKey('main.Assignment', CASCADE, related_name='rates')
    contract_config = models.ForeignKey('main.ContractConfig', CASCADE, related_name='contract_rates', null=True)
    position = models.ForeignKey('main.Position', CASCADE, related_name='assignment_rates')
    amount_per_hour = models.DecimalField(max_digits=11, decimal_places=2)

    class Meta:
        unique_together = ['position', 'assignment']
        ordering = ['-id']
