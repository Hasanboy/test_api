from django.db import models

from core.django.model import DeleteModel


class Branch(DeleteModel, models.Model):
    name = models.CharField(max_length=500)
    code = models.CharField(max_length=500, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
