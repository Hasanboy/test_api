from django.db import models

from core.django.model import BaseModel, DeleteModel


class Role(DeleteModel, BaseModel):
    name = models.CharField(max_length=255)
    groups = models.ManyToManyField('auth.Group', related_name='roles')

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name
