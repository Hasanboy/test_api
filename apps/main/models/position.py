from django.db import models

from core.django.model import BaseModel, DeleteModel


class Position(DeleteModel, BaseModel):
    name = models.CharField(max_length=255)
    rate = models.DecimalField(max_digits=11, decimal_places=2, default=0)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name



