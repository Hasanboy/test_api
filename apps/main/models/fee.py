from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel
from main.managers.fee import FeeTimeSheetManager


class Fee(DeleteModel, BaseModel):
    PLAY = 'play'
    PAUSED = 'paused'
    STATUS_TIME = (
        (PLAY, 'play'),
        (PAUSED, 'paused'),
    )

    UNSUBMITTED = 'unsubmitted'
    SUBMITTED = 'submitted'
    APPROVED = 'approved'
    REJECTED = 'rejected'

    STATUS_APPROVEMENT = (
        (UNSUBMITTED, 'UNSUBMITTED'),
        (SUBMITTED, 'SUBMITTED'),
        (APPROVED, 'APPROVED'),
        (REJECTED, 'REJECTED'),
    )

    PAID = 'paid'
    UNPAID = 'unpaid'

    STATUS_PAYMENT = (
        (PAID, 'paid'),
        (UNPAID, 'unpaid'),
    )

    OPEN = 'open'
    CLOSED = 'closed'

    STATUS_INVOICE = (
        (OPEN, 'open'),
        (CLOSED, 'closed'),
    )

    assignment = models.ForeignKey('main.Assignment', CASCADE,
                                   related_name='fees', null=True)
    user = models.ForeignKey('main.User', CASCADE, related_name='fees')
    amount = models.DecimalField(max_digits=11, decimal_places=2, null=True)
    date = models.DateField()
    description = models.TextField()
    invoice = models.ForeignKey('main.Invoice', CASCADE, related_name='fees',
                                null=True)
    pre_invoice = models.ForeignKey('main.AssignmentPreInvoice', CASCADE,
                                    related_name='fees', null=True)
    status_payment = models.CharField(max_length=50, choices=STATUS_PAYMENT,
                                      default=UNPAID)
    status_invoice = models.CharField(max_length=50, choices=STATUS_INVOICE,
                                      default=OPEN)
    status_approvement = models.CharField(max_length=50,
                                          choices=STATUS_APPROVEMENT,
                                          default=UNSUBMITTED)

    # TODO Time sheet fields
    status_time = models.CharField(max_length=25, choices=STATUS_TIME,
                                   default=PAUSED)
    start_time = models.DateTimeField(null=True)
    end_time = models.DateTimeField(null=True, blank=True)
    expired_time = models.DateTimeField(null=True)
    total_duration = models.PositiveIntegerField(default=0)
    objects = FeeTimeSheetManager()

    class Meta:
        ordering = ['id']
