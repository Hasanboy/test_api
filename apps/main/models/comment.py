from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel


class Comment(BaseModel):
    comment = models.TextField()
    user = models.ForeignKey('main.User', CASCADE, related_name='comments')
    expense = models.ForeignKey('main.Expense', CASCADE,
                                related_name='comments', null=True)
    fee = models.ForeignKey('main.Fee', CASCADE, related_name='comments',
                            null=True)

    def __str__(self):
        return self.comment

    class Meta:
        ordering = ['-id']
