from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel
from main.managers.client import ClientManager


class Client(DeleteModel, BaseModel):
    name = models.CharField(max_length=500)
    email = models.CharField(max_length=500, null=True)
    address = models.TextField(null=True)
    tags = models.ManyToManyField('main.Tags', related_name='clients')
    balance = models.DecimalField(max_digits=11, decimal_places=2, default=0)
    objects = ClientManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']


class ClientContact(BaseModel):
    client = models.ForeignKey('main.Client', CASCADE, related_name='client_contacts')
    name = models.CharField(max_length=500)
    email = models.CharField(max_length=500)
    phone = models.CharField(max_length=500)
    position = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']


class ClientBalance(DeleteModel, BaseModel):
    GENERAL = 'general'
    INDIVIDUAL = 'individual'

    TYPE = (
        (GENERAL, 'general'),
        (INDIVIDUAL, 'individual'),
    )

    client = models.ForeignKey('main.Client', CASCADE, related_name='client_balances')
    contract = models.ForeignKey('main.Contract', CASCADE, related_name='client_balances', null=True)
    type = models.CharField(max_length=255, choices=TYPE, default=GENERAL)

    balance = models.DecimalField(max_digits=11, decimal_places=2)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.client.name
