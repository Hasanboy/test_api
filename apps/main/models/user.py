from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import PROTECT


class User(AbstractUser):
    ADMIN = 'admin'
    CLIENT = 'client'
    OUTSOURCE = 'outsource'
    STAFF = 'staff'

    USER_TYPES = (
        (ADMIN, 'Admin'),
        (CLIENT, 'Client'),
        (OUTSOURCE, 'Outsource'),
        (STAFF, 'Staff'),
    )
    first_name = None
    last_name = None
    email = models.CharField(max_length=500, null=True, blank=True)
    full_name = models.CharField(max_length=500, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    user_type = models.CharField(max_length=20, choices=USER_TYPES, default=ADMIN)
    client = models.OneToOneField('main.Client', PROTECT, related_name='user', null=True)
    outsource = models.OneToOneField('main.Outsource', PROTECT, related_name='user', null=True)
    position = models.ForeignKey('main.Position', PROTECT, related_name='user', null=True, blank=True)
    role = models.ForeignKey('main.Role', PROTECT, related_name='user', null=True, blank=True)
    photo = models.ForeignKey('main.File', PROTECT, related_name='user', null=True, blank=True)
    salary = models.DecimalField(max_digits=11, decimal_places=2, null=True)
    branch = models.ForeignKey('main.Branch', PROTECT, related_name='user', null=True, blank=True)


    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    @property
    def person(self):
        if not bool(self.user_type):
            return None, self
        if self.user_type != self.ADMIN:
            return self.user_type, getattr(self, self.user_type)
        return self.user_type, self

    class Meta:
        unique_together = ['client']
        ordering = ['-id']
