from django.db import models
from django.db.models import CASCADE

from core.django.model import DeleteModel, BaseModel


class Expense(DeleteModel, BaseModel):
    UNSUBMITTED = 'unsubmitted'
    SUBMITTED = 'submitted'
    APPROVED = 'approved'
    REJECTED = 'rejected'

    STATUS_APPROVEMENT = (
        (UNSUBMITTED, 'UNSUBMITTED'),
        (SUBMITTED, 'SUBMITTED'),
        (APPROVED, 'APPROVED'),
        (REJECTED, 'REJECTED'),
    )

    PAID = 'paid'
    UNPAID = 'unpaid'
    STATUS_PAYMENT = (
        (PAID, 'paid'),
        (UNPAID, 'unpaid'),
    )

    OPEN = 'open'
    CLOSED = 'closed'
    STATUS_INVOICE = (
        (OPEN, 'open'),
        (CLOSED, 'closed'),
    )
    cashier = models.ForeignKey('main.User', CASCADE,
                                related_name='cashier_expenses', null=True)
    outsource = models.ForeignKey('main.Outsource', CASCADE,
                                  related_name='expense', null=True)
    assignment = models.ForeignKey('main.Assignment', CASCADE,
                                   related_name='expenses')
    user = models.ForeignKey('main.User', CASCADE, related_name='expenses')
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    date = models.DateField(null=True)
    description = models.TextField()
    invoice = models.ForeignKey('main.Invoice', CASCADE,
                                related_name='expenses', null=True)
    pre_invoice = models.ForeignKey('main.AssignmentPreInvoice', CASCADE,
                                    related_name='expenses', null=True)
    status_payment = models.CharField(max_length=50, choices=STATUS_PAYMENT,
                                      default=UNPAID)
    status_invoice = models.CharField(max_length=50, choices=STATUS_INVOICE,
                                      default=OPEN)
    status_approvement = models.CharField(max_length=50,
                                          choices=STATUS_APPROVEMENT,
                                          default=UNSUBMITTED)

    class Meta:
        ordering = ['id']
