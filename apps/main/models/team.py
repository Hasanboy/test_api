from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel, DeleteModel


class Team(DeleteModel, BaseModel):
    name = models.CharField(max_length=255)
    team_leader = models.ForeignKey('main.User', CASCADE, related_name='team_leaders')
    branch = models.ForeignKey('main.Branch', CASCADE, related_name='teams')
    team_members = models.ManyToManyField('main.User', related_name='teams')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
