from django.db import models
from django.db.models import CASCADE

from core.django.model import BaseModel


class Logs(BaseModel):
    fee = models.ForeignKey('main.Fee', CASCADE, related_name='logs', null=True)
    description = models.TextField()

    def __str__(self):
        return str(self.id)

    class Meta:
        ordering = ['-id']
