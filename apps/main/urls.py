from django.urls import path, include
from rest_framework.routers import DefaultRouter

from main.views.assignment import AssignmentViewSet
from main.views.bank_account import BankAccountViewSet
from main.views.cashbox import CashBoxViewSet
from main.views.client import ClientViewSet
from main.views.client_contact import ClientContactViewSet
from main.views.comment import CommentViewSet
from main.views.content_type import ContentTypeViewSet
from main.views.contract import ContractViewSet
from main.views.contract_config import ContractConfigViewSet
from main.views.expense import ExpenseViewSet
from main.views.fee import FeeViewSet
from main.views.file import FileViewSet
from main.views.invoice import InvoiceViewSet
from main.views.invoice_settings import InvoiceSettingsViewSet
from main.views.invoice_statistics import OrderStatistics
from main.views.logs import LogsViewSet
from main.views.outsource_contact import OutsourceContactViewSet
from main.views.staff import StaffViewSet, GroupViewSet, PermissionViewSet
from main.views.outsource import OutsourceViewSet
from main.views.position import PositionViewSet
from main.views.pre_invoice import PreInvoiceViewSet
from main.views.rate import RateViewSet
from main.views.role import RoleViewSet
from main.views.tags import TagsViewSet
from main.views.team import TeamViewSet
from main.views.transaction import TransactionViewSet
from main.views.user import UserViewSet
from main.views.branch import BranchViewSet
from main.views.currency import CurrencyViewSet

router = DefaultRouter()
router.register('', UserViewSet, 'user')
router.register('group', GroupViewSet, 'group')
router.register('permission', PermissionViewSet, 'permission')
router.register('staff', StaffViewSet, 'staff')
router.register('file', FileViewSet, 'file')
router.register('branch', BranchViewSet, 'branch')
router.register('tags', TagsViewSet, 'tags')
router.register('currency', CurrencyViewSet, 'currency')
router.register('client', ClientViewSet, 'client')
router.register('outsource', OutsourceViewSet, 'outsource')
router.register('outsource_contact', OutsourceContactViewSet,
                'outsource_contact')
router.register('bank_account', BankAccountViewSet, 'bank_account')
router.register('assignment', AssignmentViewSet, 'assignment')
router.register('contract', ContractViewSet, 'contract')
router.register('contract_config', ContractConfigViewSet, 'contract_config')
router.register('client_contact', ClientContactViewSet, 'client_contact')
router.register('position', PositionViewSet, 'position')
router.register('fee', FeeViewSet, 'fee')
router.register('expense', ExpenseViewSet, 'expense')
router.register('logs', LogsViewSet, 'logs')
router.register('role', RoleViewSet, 'role')
router.register('rate', RateViewSet, 'rate')
router.register('invoice', InvoiceViewSet, 'invoice')
router.register('pre_invoice', PreInvoiceViewSet, 'pre_invoice')
router.register('invoice_statistics', OrderStatistics, 'invoice_statistics')
router.register('transactions', TransactionViewSet, 'transactions')
router.register('invoice_recurring', InvoiceSettingsViewSet,
                'invoice_recurring')
router.register('teams', TeamViewSet, 'teams')
router.register('comments', CommentViewSet, 'comments')
router.register('cashbox', CashBoxViewSet, 'cashbox')
router.register('content_types', ContentTypeViewSet, 'content_types')

urlpatterns = [
    path('', include(router.urls)),
]
