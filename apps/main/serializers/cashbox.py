from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import CashBox, BankAccount, User, Currency
from main.serializers.select_serializer import SelectUserSerializer, \
    SelectCurrencyUserSerializer, \
    SelectBankAccountSerializer, SelectBranchSerializer


class CashBoxModelSerializer(serializers.ModelSerializer):
    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.all(),
        required=False, allow_null=True)
    currency = serializers.PrimaryKeyRelatedField(
        queryset=Currency.objects.all(), required=False)
    balance = serializers.DecimalField(max_digits=11, decimal_places=2,
                                       required=False)

    class Meta:
        model = CashBox
        fields = '__all__'
        read_only_fields = ['cashier']

    def __init__(self, *args, **kwargs):
        super(CashBoxModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        bank_account = attrs.get('bank_account', None)
        currency = attrs.get('currency', None)
        type = attrs.get('type', None)
        if not self.instance:
            attrs.update(cashier=self.user)

        if bank_account:
            assignment = CashBox.objects.filter(bank_account=bank_account)
            if self.instance:
                assignment = assignment.exclude(pk=self.instance.id)
            if assignment.exists():
                errors['non_field_errors'].append(
                    'contract_config has already taken')

        if type == CashBox.BANK:
            if not bank_account:
                errors['non_field_errors'].append('bank_account is required')
            else:
                if bank_account and not bank_account.currency:
                    errors['non_field_errors'].append(
                        'bank_account must have currency ')
                else:
                    attrs.update(currency=bank_account.currency)

        if type != CashBox.BANK and not currency:
            errors['non_field_errors'].append('currency is required ')
        if type != CashBox.BANK:
            attrs.update(
                bank_account=None
            )

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def to_representation(self, instance):
        self.fields['cashier'] = SelectUserSerializer()
        self.fields['currency'] = SelectCurrencyUserSerializer()
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['bank_account'] = SelectBankAccountSerializer(
            required=False)
        return super().to_representation(instance)
