from rest_framework import serializers

from main.models import Position


class PositionModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = [
            'id',
            'name',
            'rate'
        ]
        extra_kwargs = dict(
            rate=dict(required=True)
        )


class PhotoSerializers(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    file = serializers.FileField()
    content_type = serializers.CharField()
