from rest_framework import serializers

from main.models import Branch


class BranchModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Branch
        fields = '__all__'
