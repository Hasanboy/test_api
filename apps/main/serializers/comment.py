from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Comment, User, Fee, Expense
from main.serializers.select_serializer import SelectUserSerializer, SelectExpenseSerializer, SelectFeeSerializer


class CommentModelSerializer(serializers.ModelSerializer):
    fee = serializers.PrimaryKeyRelatedField(queryset=Fee.objects.all(), required=False)
    expense = serializers.PrimaryKeyRelatedField(queryset=Expense.objects.all(), required=False)

    class Meta:
        model = Comment
        fields = '__all__'
        read_only_fields = ['user']

    def __init__(self, *args, **kwargs):
        super(CommentModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        if not self.instance:
            attrs.update(
                user=self.user
            )
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def to_representation(self, instance):
        self.fields['user'] = SelectUserSerializer()
        self.fields['fee'] = SelectFeeSerializer(required=False)
        self.fields['expense'] = SelectExpenseSerializer(required=False)
        return super().to_representation(instance)
