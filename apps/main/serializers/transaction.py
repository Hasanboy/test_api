from collections import defaultdict
import decimal

from rest_framework import serializers
from rest_framework.request import Request
from django.db import transaction

from main.models import CashBox, User, Transaction, Branch, SalaryTransaction, \
    Invoice, ContractConfig
from main.serializers.select_serializer import SelectUserSerializer, \
    SelectCashboxSerializer, SelectBranchSerializer, \
    SelectContractSerializer, SelectInvoiceSerializer


class SalarySerializers(serializers.Serializer):
    staff = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.filter(user_type=User.ADMIN))
    salary = serializers.DecimalField(max_digits=11, decimal_places=2)


class TransactionModelSerializer(serializers.ModelSerializer):
    salaries = SalarySerializers(many=True, required=False, write_only=True)
    cashbox = serializers.PrimaryKeyRelatedField(
        queryset=CashBox.objects.all())
    to_cashbox = serializers.PrimaryKeyRelatedField(
        queryset=CashBox.objects.all(), required=False)
    from_branch = serializers.PrimaryKeyRelatedField(
        queryset=Branch.objects.all(), required=False)
    to_branch = serializers.PrimaryKeyRelatedField(
        queryset=Branch.objects.all(), required=False)
    amount = serializers.DecimalField(max_digits=11, decimal_places=2,
                                      required=False)

    class Meta:
        model = Transaction
        fields = '__all__'
        read_only_fields = ['created_by', 'status']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate_salaries(self, values):
        errors = list()
        staff_list = list()
        for value in values:
            staff = value.get('staff', None)
            staff_list.append(staff)
        if len(set(staff_list)) != len(staff_list):
            errors.append('staff are duplicated')

        if errors:
            raise serializers.ValidationError(errors)
        return values

    def validate(self, attrs):
        errors = defaultdict(list)
        amount = attrs.get('amount', None)
        salaries = attrs.get('salaries', None)
        branch = attrs.get('branch', None)
        type = attrs.get('type', None)
        from_branch = attrs.get('from_branch', None)
        to_branch = attrs.get('to_branch', None)
        to_cashbox = attrs.get('to_cashbox', None)
        category = attrs.get('category', None)
        cashbox = attrs.get('cashbox', None)
        invoice = attrs.get('invoice', None)
        if not self.instance:
            attrs.update(created_by=self.user,
                         status=Transaction.CONFIRM)

        if self.instance:

            if self.instance.type != type:
                errors['non_field_errors'].append('you cannot change type')
            if self.instance.category != category:
                errors['non_field_errors'].append(
                    'you cannot change category out')
            if self.instance.cashbox != cashbox:
                errors['non_field_errors'].append('you cannot change cashbox')

        if type == Transaction.OUT:
            if not category:
                errors['non_field_errors'].append('category is required ')

            if category != Transaction.REMUNERATION and not amount:
                errors['non_field_errors'].append('amount is required ')

            if category == Transaction.INTERNAL_EXPENSES:
                attrs.update(status=Transaction.PENDING)
                if not from_branch:
                    errors['non_field_errors'].append(
                        'from_branch is required ')
                if not to_branch:
                    errors['non_field_errors'].append('to_branch is required ')
                if not to_cashbox:
                    errors['non_field_errors'].append(
                        'to_cashbox is required ')

            if category == Transaction.REMUNERATION:
                attrs.update(amount=0)

                if not salaries:
                    errors['non_field_errors'].append('salaries is required')
                if not branch:
                    errors['non_field_errors'].append('branch is required')

        if type == Transaction.IN:
            if self.instance:
                errors['non_field_errors'].append('you cannot update')

            if not invoice:
                errors['non_field_errors'].append('invoice is required')
            if not amount:
                errors['non_field_errors'].append('amount is required')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    @transaction.atomic
    def create(self, validated_data):
        salaries = validated_data.pop('salaries', None)
        invoice = validated_data.get('invoice', None)
        amount = validated_data.get('amount', None)
        transaction = super().create(validated_data)
        if transaction.category == Transaction.REMUNERATION and salaries:
            amount = 0
            salary_list = []
            for salary in salaries:
                amount += salary['salary']
                salary_list.append(SalaryTransaction(
                    staff=salary['staff'],
                    salary=salary['salary'],
                    transaction=transaction,
                ))
            SalaryTransaction.objects.bulk_create(salary_list)
            transaction.amount = amount
            transaction.save()

        if transaction.category == Transaction.PAYMENT:
            if invoice.type == Invoice.SIMPLE:
                pc_amount = decimal.Decimal(amount) / invoice.pc_rate
            else:
                pc_amount = amount

            if invoice.type == Invoice.PRE_PAYMENT:
                contract_config = ContractConfig.objects.filter(
                    id=invoice.contract_config).first()
                if contract_config:
                    contract_config.is_paid = True
                    contract_config.save()

            if invoice.balance == amount:
                invoice.balance = 0
                invoice.pc_balance = 0
                invoice.status_payment = Invoice.PAID
                pc_amount = pc_amount
            else:
                pc_amount = pc_amount
                invoice.balance -= amount
                invoice.pc_balance -= pc_amount
                invoice.status_payment = Invoice.OPEN
            transaction.pc_amount = pc_amount
            transaction.contract = invoice.contract
            transaction.save()
            invoice.save()
        return transaction

    @transaction.atomic
    def update(self, instance, validated_data):
        salaries = validated_data.pop('salaries', None)
        transaction = super().update(instance, validated_data)
        if transaction.category == Transaction.REMUNERATION and salaries:
            amount = 0
            salary_list = []
            for salary in salaries:
                amount += salary['salary']
                id = salary.pop('id', None)
                staff = salary.pop('staff', None)
                salary, _ = SalaryTransaction.objects.update_or_create(
                    id=id,
                    transaction=transaction,
                    staff=staff,
                    defaults=dict(salary=salary['salary'])
                )
                salary_list.append(salary.id)
            salaries = SalaryTransaction.objects.filter(
                transaction=transaction)
            salaries.exclude(id__in=salary_list).delete()
            transaction.amount = amount
            transaction.save()
        return transaction

    def to_representation(self, instance):
        self.fields['salaries'] = SalarySerializers(
            source='salary_transactions', many=True, required=False, )
        self.fields['created_by'] = SelectUserSerializer()
        self.fields['contract'] = SelectContractSerializer(required=False)
        self.fields['invoice'] = SelectInvoiceSerializer(required=False)
        self.fields['staff'] = SelectUserSerializer(required=False)
        self.fields['cashbox'] = SelectCashboxSerializer()
        self.fields['to_cashbox'] = SelectCashboxSerializer(required=False)
        self.fields['from_branch'] = SelectBranchSerializer(required=False)
        self.fields['to_branch'] = SelectBranchSerializer(required=False)
        return super().to_representation(instance)
