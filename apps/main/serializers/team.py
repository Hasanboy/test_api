from collections import defaultdict

from rest_framework import serializers

from main.models import Team
from main.serializers.select_serializer import SelectBranchSerializer, \
    SelectUserSerializer


class TeamModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = '__all__'

    def validate(self, attrs):
        errors = defaultdict(list)
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def to_representation(self, instance):
        self.fields['team_leader'] = SelectUserSerializer()
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['team_members'] = SelectUserSerializer(many=True)
        return super().to_representation(instance)
