import decimal
from collections import defaultdict
import datetime
from rest_framework import serializers
from rest_framework.request import Request
from main.models import Assignment, User, Fee, ContractRate
from main.serializers.select_serializer import SelectUserSerializer, \
    SelectAssignmentSerializer


class FeeTimeSheetChangeStatus(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    start_time = serializers.DateTimeField(required=False)
    end_time = serializers.DateTimeField(required=False)
    expired_time = serializers.DateTimeField(required=False)
    status_time = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    total_duration = serializers.IntegerField(required=False)
    amount = serializers.DecimalField(max_digits=12, decimal_places=2,
                                      required=False)

    def __init__(self, *args, **kwargs):
        super(FeeTimeSheetChangeStatus, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        user = self.user
        position = user.position
        assignment = attrs.get('assignment', None)
        time_sheet = Fee.objects.filter(user=self.user, status_time=Fee.PLAY)

        if self.instance and self.instance.user.id != user.id:
            errors['non_field_errors'].append(
                "this time sheet doesn't belong to this user{} ".format(
                    self.user))

        if position:
            if assignment and assignment.contract_config and assignment.billing_type == Assignment.HOURLY_BILLING:
                rate_obj = ContractRate.objects.filter(
                    contract_config=assignment.contract_config,
                    position=position).first()
                if rate_obj and rate_obj.amount_per_hour:
                    attrs.update(rate=rate_obj.amount_per_hour)
                else:
                    errors['non_field_errors'].append(
                        'assignment must have rate')
        else:
            errors['non_field_errors'].append('user must have position')

        if self.instance:
            time_sheet = time_sheet.exclude(pk=self.instance.id)

        if self.instance and self.instance.status_approvement != Fee.UNSUBMITTED:
            errors['non_field_errors'].append(
                'status_approvement is not UNSUBMITTED ')
            time_sheet = time_sheet.exclude(pk=self.instance.id)

        if time_sheet.exists():
            errors['non_field_errors'].append(
                'you have already started  time sheet')

        if self.instance and self.instance.start_time:
            # attrs.update(end_time=datetime.datetime.now() + datetime.timedelta(seconds=300))
            attrs.update(end_time=datetime.datetime.now())
        if self.instance and not self.instance.start_time:
            attrs.update(start_time=datetime.datetime.now())
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        time_sheet: Fee = instance
        start_time = time_sheet.start_time
        end_time = validated_data.pop('end_time', None)
        start_time_new = validated_data.pop('start_time', None)
        rate = validated_data.pop('rate', None)
        if start_time and end_time and time_sheet.assignment:
            total_time = end_time - start_time
            total_duration = round(total_time.total_seconds() / 60)
            time_sheet.total_duration += total_duration
            time_sheet.start_time = None
            time_sheet.end_time = None
            time_sheet.expired_time = None
            time_sheet.status_time = Fee.PAUSED
            total_duration_amount = round(time_sheet.total_duration)
            if rate:
                amount = (decimal.Decimal(rate) / 60) * decimal.Decimal(
                    total_duration_amount)
            else:
                amount = None
            time_sheet.amount = amount
            time_sheet.save()
        if start_time and end_time and not time_sheet.assignment:
            total_time = end_time - start_time
            total_duration = round(total_time.total_seconds() / 60)
            time_sheet.total_duration += total_duration
            time_sheet.start_time = None
            time_sheet.end_time = None
            time_sheet.expired_time = None
            time_sheet.status_time = Fee.PAUSED
            time_sheet.total_duration = round(time_sheet.total_duration)
            time_sheet.amount = 0
            time_sheet.save()
        if start_time_new:
            time_sheet.start_time = start_time_new
            time_sheet.status_time = Fee.PLAY
            time_sheet.save()
        validated_data.update(
            id=time_sheet.id,
            start_time=time_sheet.start_time,
            end_time=time_sheet.end_time,
            status_time=time_sheet.status_time,
            total_duration=time_sheet.total_duration,
            description=time_sheet.description,
            user=time_sheet.user,
            expired_time=time_sheet.expired_time,
            amount=time_sheet.amount,

        )
        if time_sheet.assignment:
            validated_data.update(
                assignment=time_sheet.assignment
            )

        return validated_data

    def to_representation(self, instance):
        self.fields['user'] = SelectUserSerializer(required=False)
        self.fields['assignment'] = SelectAssignmentSerializer(required=False)

        return super(FeeTimeSheetChangeStatus, self).to_representation(
            instance)
