import decimal
from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request
import datetime

from main.models import Fee, Assignment, User, ContractRate
from main.serializers.select_serializer import SelectUserSerializer, \
    SelectAssignmentSerializer, SelectCommentSerializer


class FeeTimeSheetModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fee
        fields = "__all__"
        read_only_fields = ['user']
        extra_kwargs = dict(
            amount=dict(required=False),
            description=dict(required=False),
        )

    def __init__(self, *args, **kwargs):
        super(FeeTimeSheetModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        user = self.user
        position = user.position
        assignment = attrs.get('assignment', None)
        time_sheet = Fee.objects.filter(user=self.user, status_time=Fee.PLAY)

        if self.instance and self.instance.user.id != user.id:
            errors['non_field_errors'].append(
                "this time sheet doesn't belong to this user{} ".format(
                    self.user))

        if self.instance and self.instance.status_invoice == Fee.CLOSED:
            errors['non_field_errors'].append("status_invoice is closed ")

        if self.instance and self.instance.status_approvement == Fee.APPROVED:
            errors['non_field_errors'].append(
                "status_approvement is APPROVED ")

        if assignment:
            if assignment.team_leader.id != user.id and user not in assignment.work_group.all():
                errors['non_field_errors'].append(
                    "user not belongs to this assignment ")

        if position:
            if assignment and assignment.contract_config and assignment.billing_type == Assignment.HOURLY_BILLING:
                rate_obj = ContractRate.objects.filter(
                    contract_config=assignment.contract_config,
                    position=position).first()
                if rate_obj and rate_obj.amount_per_hour:
                    attrs.update(rate=rate_obj.amount_per_hour)
                else:
                    errors['non_field_errors'].append(
                        'assignment must have rate')
        else:
            errors['non_field_errors'].append('user must have position')

        if self.instance and self.instance.assignment and not assignment:
            errors['non_field_errors'].append('instance must have assignment')

        if self.instance:
            time_sheet = time_sheet.exclude(pk=self.instance.id)

        if self.instance and self.instance.assignment and self.instance.assignment != assignment \
                and self.instance.status_time == Fee.PLAY:
            errors['non_field_errors'].append(
                'before update assignment you must paused current time sheet ')

        if time_sheet.exists():
            errors['non_field_errors'].append(
                'you have already started  time sheet')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        user = self.user
        assignment = validated_data.pop('assignment', None)
        description = validated_data.pop('description', None)
        total_duration = validated_data.pop('total_duration', None)
        date = validated_data.pop('date', None)
        amount = None

        if not total_duration:
            start_time = datetime.datetime.now()
            expired_time = start_time + datetime.timedelta(hours=9)
            time_sheet = Fee.objects.create(
                assignment=assignment,
                date=date,
                user=self.user,
                description=description,
                status_time=Fee.PLAY,
                start_time=start_time,
                expired_time=expired_time,
            )
            time_sheet.save()
            validated_data.update(
                id=time_sheet.id,
                assignment=time_sheet.assignment,
                user=time_sheet.user,
                description=time_sheet.description,
                status_time=time_sheet.status_time,
                start_time=time_sheet.start_time,
                expired_time=time_sheet.expired_time,
                total_duration=time_sheet.total_duration,
                amount=time_sheet.amount,
                date=time_sheet.date,
            )
        else:
            if assignment:
                if assignment.contract_config and assignment.billing_type == Assignment.HOURLY_BILLING:
                    position = user.position
                    rate = ContractRate.objects.filter(
                        contract_config=assignment.contract_config,
                        position=position).first()
                    rate = rate.amount_per_hour
                    amount = 0
                    amount = decimal.Decimal(total_duration) * decimal.Decimal(
                        rate / 60)

            time_sheet = Fee.objects.create(
                assignment=assignment,
                date=date,
                user=self.user,
                description=description,
                total_duration=total_duration,
                amount=amount,
            )
            time_sheet.save()
            validated_data.update(
                id=time_sheet.id,
                assignment=time_sheet.assignment,
                user=time_sheet.user,
                description=time_sheet.description,
                status_time=time_sheet.status_time,
                start_time=time_sheet.start_time,
                expired_time=time_sheet.expired_time,
                total_duration=time_sheet.total_duration,
                amount=time_sheet.amount,
                date=time_sheet.date,
            )

        return validated_data

    def update(self, instance, validated_data):
        assignment = validated_data.pop('assignment', None)
        description = validated_data.pop('description', None)
        total_duration = validated_data.pop('total_duration', None)
        date = validated_data.pop('date', None)
        fee: Fee = instance
        if total_duration:
            fee.total_duration = total_duration
            # fee.total_duration = round(fee.total_duration)
            fee.save()

        if assignment:
            user = fee.user
            total_duration_fee = fee.total_duration
            if assignment.contract_config and assignment.billing_type == Assignment.HOURLY_BILLING:
                position = user.position
                rate = ContractRate.objects.filter(
                    contract_config=assignment.contract_config,
                    position=position).first()
                rate = rate.amount_per_hour
                fee.amount = decimal.Decimal(
                    total_duration_fee) * decimal.Decimal(rate / 60)
            fee.assignment = assignment
            fee.save()
        if description:
            fee.description = description
            fee.save()

        if date:
            fee.date = date
            fee.save()
        return fee

    def to_representation(self, instance):
        self.fields['user'] = SelectUserSerializer(required=False)
        self.fields['comments'] = SelectCommentSerializer(required=False,
                                                          many=True)
        self.fields['assignment'] = SelectAssignmentSerializer(required=False)
        return super(FeeTimeSheetModelSerializer, self).to_representation(
            instance)
