from collections import defaultdict
from rest_framework import serializers
from rest_framework.request import Request
from main.models import User, Fee
from main.serializers.select_serializer import SelectFeeSerializer


class FeeTimeSheetChangeApproveStatus(serializers.Serializer):
    status_approvement = serializers.ChoiceField(
        choices=Fee.STATUS_APPROVEMENT)
    # fees = serializers.ListSerializer(
    #     child=serializers.PrimaryKeyRelatedField(
    #         queryset=Fee.objects.filter(assignment__isnull=False,
    #                                     status_time=Fee.PAUSED)))
    fees = serializers.ListSerializer(
        child=serializers.PrimaryKeyRelatedField(
            queryset=Fee.objects.all()))

    def __init__(self, *args, **kwargs):
        super(FeeTimeSheetChangeApproveStatus, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    # def validate_fees(self, values):
    #
    #     errors = defaultdict(list)
    #     contract_list = []
    #     for value in values:
    #         contract_list.append(value.assignment.contract_config.id)
    #     if len(set(contract_list)) != 1:
    #         errors['non_field_errors'].append('contract  is not same fees')
    #
    #     for fee in values:
    #         if fee.status_approvement == Fee.APPROVED:
    #             errors['non_field_errors'].append('fee is approved: {}'.format(fee.id))
    #     if errors:
    #         raise serializers.ValidationError(errors)
    #     return values

    def validate(self, attrs):
        errors = defaultdict(list)
        status_approvement = attrs.get('status_approvement', None)
        fees = attrs.get('fees', None)
        user = self.user
        if status_approvement in [Fee.SUBMITTED, Fee.UNSUBMITTED]:
            for fee in fees:
                if fee.user.id != user.id:
                    errors['non_field_errors'].append(
                        "this time sheet doesn't belong to this user: {} ".format(
                            self.user))
                if fee.status_time != Fee.PAUSED:
                    errors['non_field_errors'].append(
                        "Fee is not PAUSED ")
                if not fee.assignment:
                    errors['non_field_errors'].append(
                        "Fee must have  assignment ")

        if status_approvement == Fee.APPROVED:
            for fee in fees:
                if fee.assignment.team_leader.id != user.id:
                    errors['non_field_errors'].append(
                        "user doesn't team leader: {}  ".format(self.user))

        if status_approvement == Fee.REJECTED:
            errors['non_field_errors'].append("status cannot be reject")

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        fees = validated_data.get('fees', [])
        status_approvement = validated_data.get('status_approvement', [])

        for fee in fees:
            fee.status_approvement = status_approvement
            fee.save()

        return validated_data

    def to_representation(self, instance):
        self.fields['fees'] = SelectFeeSerializer(many=True, required=False)
        return super(FeeTimeSheetChangeApproveStatus, self).to_representation(
            instance)
