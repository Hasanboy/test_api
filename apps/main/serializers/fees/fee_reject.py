from collections import defaultdict
from rest_framework import serializers
from rest_framework.request import Request
from main.models import User, Fee, Comment


class FeeRejectSerializer(serializers.Serializer):
    fee = serializers.PrimaryKeyRelatedField(
        queryset=Fee.objects.all())
    comment = serializers.CharField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        fee = attrs.get('fee', None)
        if fee.status_approvement == Fee.APPROVED:
            errors['non_field_errors'].append(
                "fee already  approved".format(self.user))
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        fee = validated_data.get('fee', None)
        comment = validated_data.get('comment', None)
        fee.status_approvement = Fee.REJECTED
        fee.save()
        Comment.objects.create(
            user=self.user,
            comment=comment,
            fee=fee
        )
        return validated_data
