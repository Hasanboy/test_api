from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Currency, CurrencyHistory, User
from main.serializers.select_serializer import SelectUserSerializer


class CurrencyHistorySerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    rate = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['user'] = SelectUserSerializer()
        return super(CurrencyHistorySerializer, self).to_representation(instance)


class CurrencyModelSerializer(serializers.ModelSerializer):
    currency_histories = CurrencyHistorySerializer(many=True, required=False)

    class Meta:
        model = Currency
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CurrencyModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        is_primary = attrs.get('is_primary')
        rate = attrs.get('rate')
        primary_currency = Currency.objects.all()
        if self.instance and self.instance.is_primary:
            primary_currency = primary_currency.exclude(id=self.instance.id)
        if is_primary:
            primary_currency = primary_currency.filter(is_primary=True).first()
            if primary_currency:
                errors['non_field_errors'].append('primary_currency  already exist')
        if not is_primary and not rate:
            errors['non_field_errors'].append('rate  is  required')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def update(self, instance, validated_data):
        old_rate = instance.rate

        currency: Currency = super().update(instance, validated_data)
        currency.save()

        if currency.rate != old_rate:
            CurrencyHistory.objects.create(
                currency=currency,
                rate=currency.rate,
                user=self.user
            )

        return currency
