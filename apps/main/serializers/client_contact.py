from rest_framework import serializers

from main.models import ClientContact, OutsourceContact
from main.serializers.select_serializer import SelectClientSerializer, \
    SelectOutSourceSerializer


class ClientContactModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientContact
        fields = '__all__'

    def to_representation(self, instance):
        self.fields['client'] = SelectClientSerializer()
        return super().to_representation(instance)


class ClientContactSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField()
    email = serializers.CharField()
    phone = serializers.CharField()
    position = serializers.CharField()


class OutsourceContactModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = OutsourceContact
        fields = '__all__'

    def to_representation(self, instance):
        self.fields['outsource'] = SelectOutSourceSerializer()
        return super().to_representation(instance)
