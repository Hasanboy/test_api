from collections import defaultdict
from rest_framework import serializers
from rest_framework.request import Request
from main.models import User, Expense, Comment


class ExpenseRejectSerializer(serializers.Serializer):
    expense = serializers.PrimaryKeyRelatedField(
        queryset=Expense.objects.all())
    comment = serializers.CharField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        expense = attrs.get('expense', None)
        if expense.status_approvement == Expense.APPROVED:
            errors['non_field_errors'].append(
                "expense already  approved".format(self.user))
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        expense = validated_data.get('expense', None)
        comment = validated_data.get('comment', None)
        expense.status_approvement = Expense.REJECTED
        expense.save()
        Comment.objects.create(
            user=self.user,
            comment=comment,
            expense=expense
        )
        return validated_data
