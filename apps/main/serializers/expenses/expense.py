from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Expense, Assignment, User, Outsource, CashBox
from main.serializers.select_serializer import SelectAssignmentSerializer, \
    SelectUserSerializer, \
    SelectOutSourceSerializer, SelectCommentSerializer


class ExpenseSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    amount = serializers.DecimalField(max_digits=11, decimal_places=2)
    description = serializers.CharField()
    date = serializers.DateField()

    def to_representation(self, instance):
        self.fields['outsource'] = SelectOutSourceSerializer(required=False)
        self.fields['assignment'] = SelectAssignmentSerializer()
        self.fields['user'] = SelectUserSerializer(required=False)
        self.fields['cashier'] = SelectUserSerializer(required=False)
        return super(ExpenseSerializer, self).to_representation(instance)


class ExpenseModelSerializer(serializers.ModelSerializer):
    outsource = serializers.PrimaryKeyRelatedField(
        queryset=Outsource.objects.all(), required=False)
    cashier = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    expenses = ExpenseSerializer(
        many=True,
        required=False
    )
    assignment = serializers.PrimaryKeyRelatedField(
        queryset=Assignment.objects.all())

    class Meta:
        model = Expense
        exclude = ['modified_date']
        read_only_fields = ['user']
        extra_kwargs = dict(
            amount=dict(required=False),
            description=dict(required=False),
            date=dict(required=False),
        )

    def __init__(self, *args, **kwargs):
        super(ExpenseModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        expenses = attrs.get('expenses', None)
        cashier = attrs.get('cashier', None)
        assignment = attrs.get('assignment', None)
        user = self.user

        cashbox = CashBox.objects.filter(cashier=cashier)
        if not cashbox:
            errors['non_field_errors'].append("cashier has not cashbox")

        if assignment and assignment.team_leader.id != user.id and user not in assignment.work_group.all():
            errors['non_field_errors'].append(
                "user not belong to this assignment ")

        if assignment and not assignment.contract_config:
            errors['non_field_errors'].append(
                "you can not create expense for this  assignment")

        if self.instance and self.instance.user.id != user.id:
            errors['non_field_errors'].append(
                "this time sheet doesn't belong to this user{} ".format(
                    self.user))

        if self.instance and self.instance.status_invoice == Expense.CLOSED:
            errors['non_field_errors'].append("status_invoice is closed ")

        if self.instance and self.instance.status_approvement == Expense.APPROVED:
            errors['non_field_errors'].append(
                "status_approvement is APPROVED ")

        if not self.instance and not expenses:
            errors['non_field_errors'].append('expenses type is required')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        expenses = validated_data.pop('expenses', None)
        cashier = validated_data.pop('cashier', None)
        outsource = validated_data.pop('outsource', None)
        expenses_list = []
        if expenses:
            assignment = validated_data.get('assignment', None)
            for expense in expenses:
                expense_instance = Expense.objects.create(
                    user=self.user,
                    outsource=outsource,
                    assignment=assignment,
                    description=expense['description'],
                    amount=expense['amount'],
                    date=expense['date'],
                    cashier=cashier,
                )
                expenses_list.append(dict(
                    id=expense_instance.id,
                    description=expense_instance.description,
                    amount=expense_instance.amount,
                    date=expense_instance.date,
                    outsource=expense_instance.outsource,
                    assignment=expense_instance.assignment,
                    user=expense_instance.user,
                    cashier=expense_instance.cashier,
                ))
                validated_data.update(expenses=expenses_list)
        return validated_data

    def to_representation(self, instance):
        self.fields['user'] = SelectUserSerializer(required=False)
        self.fields['cashier'] = SelectUserSerializer(required=False)
        self.fields['assignment'] = SelectAssignmentSerializer(required=False)
        self.fields['amount'] = serializers.DecimalField(max_digits=11,
                                                         decimal_places=2,
                                                         required=False)
        self.fields['comments'] = SelectCommentSerializer(required=False,
                                                          many=True)
        self.fields['outsource'] = SelectOutSourceSerializer(required=False)
        return super(ExpenseModelSerializer, self).to_representation(instance)
