from collections import defaultdict
from datetime import datetime

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Expense, User, Transaction, CashBox
from main.serializers.select_serializer import SelectExpenseSerializer


class ExpenseChangeApproveStatus(serializers.Serializer):
    status_approvement = serializers.ChoiceField(
        choices=Expense.STATUS_APPROVEMENT)
    expenses = serializers.ListSerializer(
        child=serializers.PrimaryKeyRelatedField(
            queryset=Expense.objects.filter(assignment__isnull=False)))

    def __init__(self, *args, **kwargs):
        super(ExpenseChangeApproveStatus, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate_expenses(self, values):
        errors = defaultdict(list)
        for expense in values:
            if expense.status_approvement == Expense.APPROVED:
                errors['non_field_errors'].append(
                    'expense is approved: {}'.format(expense.id))
        if errors:
            raise serializers.ValidationError(errors)
        return values

    def validate(self, attrs):
        errors = defaultdict(list)
        status_approvement = attrs.get('status_approvement', None)
        expenses = attrs.get('expenses', None)
        user = self.user

        if status_approvement in [Expense.SUBMITTED, Expense.UNSUBMITTED]:
            for expense in expenses:
                if expense.user.id != user.id:
                    errors['expense '].append(
                        "this expense doesn't belong to this user: {} ".format(
                            self.user))
        if status_approvement == Expense.APPROVED:
            for expense in expenses:
                if expense.assignment.team_leader.id != user.id:
                    errors['expense '].append(
                        "user doesn't team leader: {} ".format(self.user))

        if status_approvement == Expense.REJECTED:
            errors['non_field_errors'].append("status cannot be reject")
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        expenses = validated_data.get('expenses', [])
        status_approvement = validated_data.get('status_approvement', [])

        for expense in expenses:
            expense.status_approvement = status_approvement
            expense.save()
            cashbox = CashBox.objects.filter(cashier=expense.cashier).first()
            Transaction.objects.create(
                expense=expense,
                cashbox=cashbox,
                created_by=self.user,
                amount=expense.amount,
                pc_amount=expense.amount,
                description=expense.description,
                payment_date=datetime.now(),
                type=Transaction.OUT,
                category=Transaction.ASSIGNMENT_EXPENSES,
                status=Transaction.PENDING,
            )

        return validated_data


def to_representation(self, instance):
    self.fields['expenses'] = SelectExpenseSerializer(many=True,
                                                      required=False)
    return super(ExpenseChangeApproveStatus, self).to_representation(instance)
