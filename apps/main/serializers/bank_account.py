from rest_framework import serializers

from main.models import BankAccount, Currency
from main.serializers.select_serializer import SelectBranchSerializer, SelectCurrencySerializer


class BankAccountModelSerializer(serializers.ModelSerializer):
    currency = serializers.PrimaryKeyRelatedField(queryset=Currency.objects.all())

    class Meta:
        model = BankAccount
        fields = '__all__'
        extra_kwargs = dict(
            code=dict(required=False)
        )

    def to_representation(self, instance):
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['currency'] = SelectCurrencySerializer()
        return super(BankAccountModelSerializer, self).to_representation(instance)
