from rest_framework import serializers


class InvoiceFilterSerializer(serializers.Serializer):
    from_date = serializers.DateField(required=False)
    to_date = serializers.DateField(required=False)
    assignment = serializers.IntegerField(required=False)
    client = serializers.IntegerField(required=False)
    status = serializers.CharField(required=False)
    all = serializers.BooleanField(default=False)


class InvoiceStatisticsStatusSerializer(serializers.Serializer):
    total_paid_amount = serializers.DecimalField(max_digits=11,
                                                 decimal_places=2)
    total_amount = serializers.DecimalField(max_digits=11, decimal_places=2)


class InvoiceStatisticsPeriodSerializer(serializers.Serializer):
    month = serializers.DateField()
    total_paid = serializers.DecimalField(max_digits=11, decimal_places=2,
                                          required=False)
    total_amount = serializers.DecimalField(max_digits=11, decimal_places=2,
                                            required=False,
                                            source='total_all_amount')


class FeeFilterSerializer(serializers.Serializer):
    from_date = serializers.DateField(required=False)
    to_date = serializers.DateField(required=False)
    assignment = serializers.IntegerField(required=False)
    staff = serializers.IntegerField(required=False)
    all = serializers.BooleanField(default=False)


class FeeStatisticsPeriodSerializer(serializers.Serializer):
    date = serializers.DateField(source='dates')
    total_duration = serializers.DecimalField(max_digits=11, decimal_places=2,
                                              required=False,
                                              source='total_all_duration')
