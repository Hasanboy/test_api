from collections import defaultdict

from rest_framework import serializers

from main.models import Transaction, CashBox


class TransactionPendingSerializer(serializers.Serializer):
    cashbox = serializers.PrimaryKeyRelatedField(
        queryset=CashBox.objects.all(), required=False)
    status = serializers.CharField(max_length=244)

    def validate(self, attrs):
        errors = defaultdict(list)
        invoice: Transaction = self.instance
        if invoice.category not in [Transaction.INTERNAL_EXPENSES,
                                    Transaction.ASSIGNMENT_EXPENSES]:
            errors['non_field_errors'].append(
                'Transaction is not INTERNAL_EXPENSES or ASSIGNMENT_EXPENSES')
        cashbox = attrs.get('cashbox', None)
        status = attrs.get('status', None)

        if status == "relocate" and not cashbox:
            errors['non_field_errors'].append('cashbox is required ')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def update(self, instance, validated_data):

        cashbox = validated_data.get('cashbox', None)
        status = validated_data.get('status', None)
        transaction: Transaction = self.instance

        if cashbox and status == 'relocate':
            transaction.to_cashbox = cashbox
            transaction.save()
        else:
            transaction.status = Transaction.CONFIRM
            transaction.save()

        return validated_data
