from rest_framework import serializers

from main.models import AssignmentRate, Assignment, Position


class RateModelSerializer(serializers.ModelSerializer):
    assignment = serializers.PrimaryKeyRelatedField(
        queryset=Assignment.objects.all())
    position = serializers.PrimaryKeyRelatedField(
        queryset=Position.objects.all())

    class Meta:
        model = AssignmentRate
        fields = '__all__'

    def create(self, validated_data):
        validated_data.pop('contract', None)
        rate = super(RateModelSerializer, self).create(validated_data)
        return rate
