from collections import defaultdict

from rest_framework import serializers

from main.models import Outsource, User, OutsourceContact
from main.serializers.tags import TagsSelectSerializer


class OutsourceContactSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField()
    email = serializers.CharField()
    phone = serializers.CharField()
    position = serializers.CharField()


class OutsourceModelSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=255, write_only=True,
                                     required=False)
    contacts = OutsourceContactSerializer(
        many=True,
        required=False
    )

    class Meta:
        model = Outsource
        fields = '__all__'
        extra_kwargs = dict(
            tags=dict(allow_empty=True)
        )

    def validate(self, attrs):
        errors = defaultdict(list)

        users = User.objects.filter(username=attrs['email'])
        if not self.instance:
            if not attrs.get('password'):
                errors['non_field_errors'].append('This field is required')
        if self.instance:
            users = users.exclude(outsource=self.instance.id)
        if users.exists():
            errors['non_field_errors'].append('This email is used')
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        email = validated_data.pop('email', None)
        contacts = validated_data.pop('contacts', None)

        outsource = super(OutsourceModelSerializer, self).create(
            validated_data)
        user = User.objects.create(username=email, outsource=outsource,
                                   user_type=User.OUTSOURCE)
        user.set_password(password)
        user.save()

        if contacts:
            client_list = []
            for contact in contacts:
                name = contact.pop('name', None)
                email = contact.pop('email', None)
                phone = contact.pop('phone', None)
                position = contact.pop('position', None)
                client_list.append(OutsourceContact(
                    outsource=outsource,
                    name=name,
                    phone=phone,
                    email=email,
                    position=position
                ))
            OutsourceContact.objects.bulk_create(client_list)

        return outsource

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        email = validated_data.get('email', None)
        contacts = validated_data.pop('contacts', None)

        outsource: Outsource = super(OutsourceModelSerializer, self).update(
            instance, validated_data)
        user = outsource.user
        if password:
            user.set_password(password)
        if email:
            user.username = email
        user.save()

        if contacts:
            contact_ids = []
            for contact in contacts:
                id = contact.pop('id', None)
                name = contact.pop('name', None)
                email = contact.pop('email', None)
                phone = contact.pop('phone', None)
                position = contact.pop('position', None)

                contact = OutsourceContact.objects.update_or_create(
                    id=id,
                    outsource=outsource,
                    defaults=dict(
                        name=name,
                        phone=phone,
                        email=email,
                        position=position,
                    )
                )
                contact_ids.append(contact[0].id)
            OutsourceContact.objects.exclude(id__in=contact_ids).delete()

        return outsource

    def to_representation(self, instance):
        self.fields['contacts'] = OutsourceContactSerializer(many=True,
                                                             required=False,
                                                             source='outsource_contacts')
        self.fields['tags'] = TagsSelectSerializer(many=True, required=False)
        return super().to_representation(instance)
