from rest_framework import serializers

from main.models import Position
from main.serializers.file import FileSerializer
from main.serializers.position import PositionModelSerializer


class SelectAssignmentSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    created_date = serializers.ReadOnlyField()
    dead_line = serializers.ReadOnlyField()
    is_billable = serializers.ReadOnlyField()
    billing_type = serializers.ReadOnlyField()
    fixed_fee_amount = serializers.ReadOnlyField()
    fixed_fee_expenses_included_in_fee = serializers.ReadOnlyField()
    hourly_has_fee_ceiling = serializers.ReadOnlyField()
    hourly_fee_ceiling = serializers.ReadOnlyField()
    invoice_delivered_by = serializers.ReadOnlyField()
    payment_duration = serializers.ReadOnlyField()
    payment_date = serializers.ReadOnlyField()

    invoice_expense_amount = serializers.DecimalField(max_digits=11,
                                                      decimal_places=2,
                                                      required=False,
                                                      allow_null=True)
    uninvoice_expense_amount = serializers.DecimalField(max_digits=11,
                                                        decimal_places=2,
                                                        required=False,
                                                        allow_null=True)
    invoice_fee_amount = serializers.DecimalField(max_digits=11,
                                                  decimal_places=2,
                                                  required=False,
                                                  allow_null=True)
    uninvoice_fee_amount = serializers.DecimalField(max_digits=11,
                                                    decimal_places=2,
                                                    required=False,
                                                    allow_null=True)
    invoice_fee_hours = serializers.IntegerField(required=False,
                                                 allow_null=True)
    uninvoice_fee_hours = serializers.IntegerField(required=False,
                                                   allow_null=True)

    def to_representation(self, instance):
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['currency'] = SelectCurrencySerializer()
        return super(SelectAssignmentSerializer, self).to_representation(
            instance)


class SelectInvoiceSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    total_amount = serializers.ReadOnlyField()
    balance = serializers.ReadOnlyField()
    pc_amount = serializers.ReadOnlyField()
    pc_balance = serializers.ReadOnlyField()
    pc_rate = serializers.ReadOnlyField()
    pc_date = serializers.ReadOnlyField()
    due_date = serializers.ReadOnlyField()
    issue_date = serializers.ReadOnlyField()
    status_payment = serializers.ReadOnlyField()
    type = serializers.ReadOnlyField()
    description = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['client'] = SelectClientSerializer()
        self.fields['user'] = SelectUserSerializer()
        self.fields['contract'] = SelectContractSerializer()
        return super(SelectInvoiceSerializer, self).to_representation(instance)


class SelectContractSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    contract_number = serializers.ReadOnlyField()
    created_date = serializers.ReadOnlyField()
    dead_line = serializers.ReadOnlyField()
    billing_type = serializers.ReadOnlyField()
    fixed_fee_amount = serializers.ReadOnlyField()
    fixed_fee_expenses_included_in_fee = serializers.ReadOnlyField()
    hourly_has_fee_ceiling = serializers.ReadOnlyField()
    hourly_fee_ceiling = serializers.ReadOnlyField()
    payment_duration = serializers.ReadOnlyField()
    payment_date = serializers.ReadOnlyField()
    is_multiple = serializers.ReadOnlyField()
    success_fee = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['currency'] = SelectCurrencySerializer()
        self.fields['client'] = SelectClientSerializer()
        self.fields['bank_account'] = SelectBankAccountSerializer()
        self.fields['rates'] = SelectRateSerializers(many=True, required=False)
        return super(SelectContractSerializer, self).to_representation(
            instance)


class SelectContractConfigSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    external_assignment_name = serializers.ReadOnlyField()
    internal_assignment_name = serializers.ReadOnlyField()
    billing_type = serializers.ReadOnlyField()
    fixed_fee_amount = serializers.ReadOnlyField()
    fixed_fee_expenses_included_in_fee = serializers.ReadOnlyField()
    fixed_fee_pre_amount = serializers.ReadOnlyField()
    hourly_has_fee_ceiling = serializers.ReadOnlyField()
    hourly_fee_ceiling = serializers.ReadOnlyField()
    payment_date = serializers.ReadOnlyField()
    is_paid = serializers.ReadOnlyField()
    is_invoiced = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['team_leader'] = SelectUserSerializer()
        self.fields['contract'] = SelectContractSerializer()
        self.fields['rates'] = SelectRateSerializers(many=True, required=False)
        return super().to_representation(instance)


class SelectBranchSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()


class SelectCommentSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    comment = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['user'] = SelectUserSerializer(required=False)
        return super().to_representation(instance)


class SelectClientBalanceSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    type = serializers.ReadOnlyField()
    balance = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['client'] = SelectClientSerializer()
        self.fields['contract'] = SelectContractSerializer(required=False)
        return super().to_representation(instance)


class SelectClientContactSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    email = serializers.ReadOnlyField()
    phone = serializers.ReadOnlyField()
    position = serializers.ReadOnlyField()


class SelectFeeSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    amount = serializers.ReadOnlyField()
    total_duration = serializers.IntegerField()
    description = serializers.CharField()
    status_invoice = serializers.CharField()
    status_payment = serializers.CharField()
    status_approvement = serializers.CharField()
    date = serializers.DateField()

    def to_representation(self, instance):
        self.fields['user'] = SelectUserSerializer()
        return super(SelectFeeSerializer, self).to_representation(instance)


class SelectExpenseSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    amount = serializers.ReadOnlyField()
    date = serializers.ReadOnlyField()
    description = serializers.ReadOnlyField()
    status_invoice = serializers.CharField()
    status_payment = serializers.CharField()
    status_approvement = serializers.CharField()

    def to_representation(self, instance):
        self.fields['user'] = SelectUserSerializer()
        return super(SelectExpenseSerializer, self).to_representation(instance)


class SelectBankAccountSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    code = serializers.ReadOnlyField()
    address = serializers.ReadOnlyField()
    bank_details = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['currency'] = SelectCurrencySerializer()
        return super(SelectBankAccountSerializer, self).to_representation(
            instance)


class SelectCurrencySerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    sign = serializers.ReadOnlyField()
    rate = serializers.ReadOnlyField()
    is_primary = serializers.ReadOnlyField()


class SelectCurrencyUserSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    sign = serializers.CharField()
    rate = serializers.DecimalField(max_digits=11, decimal_places=2)
    is_primary = serializers.BooleanField()


class SelectUserSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    username = serializers.ReadOnlyField()
    full_name = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['position'] = PositionModelSerializer(required=False)
        self.fields['photo'] = FileSerializer(context=self.context,
                                              required=False)
        self.fields['branch'] = SelectBranchSerializer(required=False)
        return super(SelectUserSerializer, self).to_representation(instance)


class SelectOutSourceSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()


class SelectCashboxSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    type = serializers.ReadOnlyField()

    def to_representation(self, instance):
        self.fields['cashier'] = SelectUserSerializer()
        self.fields['currency'] = SelectCurrencyUserSerializer()
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['bank_account'] = SelectBankAccountSerializer(
            required=False)
        return super().to_representation(instance)


class SelectClientSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()


class SelectTagsSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()


class SelectRateSerializers(serializers.Serializer):
    position = serializers.PrimaryKeyRelatedField(
        queryset=Position.objects.all())
    amount_per_hour = serializers.DecimalField(max_digits=11, decimal_places=2)

    def to_representation(self, instance):
        self.fields['position'] = PositionModelSerializer()
        return super(SelectRateSerializers, self).to_representation(instance)
