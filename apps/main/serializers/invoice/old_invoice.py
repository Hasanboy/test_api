from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Invoice, Fee, Expense, User, Assignment, AssignmentInvoice, Currency, Contract
from main.serializers.select_serializer import SelectClientSerializer, SelectUserSerializer, SelectAssignmentSerializer, \
    SelectFeeSerializer, SelectExpenseSerializer, SelectContractSerializer


class AssignmentInvoiceSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    due_date = serializers.DateField()
    total_amount = serializers.DecimalField(max_digits=11, decimal_places=2)
    pc_amount = serializers.DecimalField(max_digits=11, decimal_places=2)
    description = serializers.CharField()
    assignment = SelectAssignmentSerializer()
    fees = serializers.SerializerMethodField(required=False)
    expenses = serializers.SerializerMethodField(required=False)

    def get_fees(self, obj: AssignmentInvoice):
        query = Fee.objects.filter(invoice=obj.invoice, assignment=obj.assignment)
        serializer = SelectFeeSerializer(query, many=True)
        return serializer.data

    def get_expenses(self, obj: AssignmentInvoice):
        query = Expense.objects.filter(invoice=obj.invoice, assignment=obj.assignment)
        serializer = SelectExpenseSerializer(query, many=True)
        return serializer.data


class AssignmentSerializer(serializers.Serializer):
    assignment = serializers.PrimaryKeyRelatedField(queryset=Assignment.objects.all())
    fees = serializers.ListSerializer(child=serializers.PrimaryKeyRelatedField(
        queryset=Fee.objects.filter(status_time=Fee.PAUSED, status_approvement=Fee.APPROVED)), required=False)
    expenses = serializers.ListSerializer(child=serializers.PrimaryKeyRelatedField(
        queryset=Expense.objects.filter(status_approvement=Expense.APPROVED)),
        required=False)

    def validate(self, attrs):
        errors = defaultdict(list)
        fees = attrs.get('fees', [])
        expenses = attrs.get('expenses', [])
        assignment = attrs.get('assignment', [])
        if assignment and not assignment.currency:
            errors['non_field_errors'].append('assignment currency is required ')

        if assignment and not assignment.contract_config:
            errors['non_field_errors'].append('assignment contract_configs is required ')

        if assignment and not assignment.currency.rate:
            errors['non_field_errors'].append('assignment currency rate is not  defined ')

        for fee in fees:
            if fee.assignment != assignment:
                errors['non_field_errors'].append('fee {} is not belong this {}  assignment'.format(fee, assignment))

        for expense in expenses:
            if expense.assignment != assignment:
                errors['non_field_errors'].append(
                    'expense {} is not belong this {}  assignment'.format(expense, assignment))

        if errors:
            raise serializers.ValidationError(errors)
        return attrs


class InvoiceModelSerializer(serializers.ModelSerializer):
    assignments = AssignmentSerializer(many=True, required=False)

    # approved_fees = serializers.SerializerMethodField(required=False)
    # approved_expenses = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Invoice
        fields = '__all__'
        read_only_fields = ['user', 'pc_rate', 'pc_amount', 'pc_date', 'pc_balance']
        extra_kwargs = dict(
            total_amount=dict(required=False)
        )

    def __init__(self, *args, **kwargs):
        super(InvoiceModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        issue_date = attrs.get('issue_date', None)
        total_amount = attrs.get('total_amount', None)
        type = attrs.get('type', None)
        assignments = attrs.get('assignments', None)

        attrs.update(
            user=self.user,
        )

        if not type:
            errors['non_field_errors'].append('type is required')

        if self.instance and self.instance.type != type:
            errors['non_field_errors'].append('type is cannot be changed')

        if type == Invoice.PRE_PAYMENT and not total_amount:
            errors['non_field_errors'].append('total_amount is required')
        else:
            attrs.update(
                pc_balance=total_amount,
                pc_amount=total_amount,
                balance=total_amount,
            )

        if type == Invoice.SIMPLE:
            attrs.update(
                total_amount=0,
            )
            currency = assignments[0]
            currency_list = []
            contract_list = []
            for assignment in assignments:
                currency_list.append(assignment['assignment'].currency.id)
                if assignment['assignment'].contract_config.contract:
                    contract_list.append(assignment['assignment'].contract_config.contract.id)
            count_contract_list = len(set(contract_list))
            if count_contract_list and count_contract_list != 1:
                errors['non_field_errors'].append('contract is not same')

            if count_contract_list:
                contract = Contract.objects.filter(id=contract_list[0]).first()
                if contract:
                    attrs.update(
                        contract=contract,
                    )

            primary_currency = Currency.objects.filter(is_primary=True).first()
            if not primary_currency:
                errors['non_field_errors'].append('primary currency is not defined')
            else:
                attrs.update(
                    pc_rate=currency['assignment'].currency.rate,
                )
        if not issue_date:
            errors['non_field_errors'].append('issue_date is  required ')
        if self.instance and self.instance.status_payment != Invoice.UNPAID:
            errors['non_field_errors'].append('Invoice already paid ')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        type = validated_data.get('type', None)
        assignments = validated_data.pop('assignments', None)
        due_date = validated_data.get('due_date', None)

        invoice = super(InvoiceModelSerializer, self).create(validated_data)

        if type == Invoice.SIMPLE:
            total_amount_invoice = 0
            total_amount_invoice_pc = 0
            assignment_invoice_list = []
            fees_ids = []
            expenses_ids = []
            for assignment in assignments:
                total_amount = 0
                total_amount_pc = 0
                constant_fee = 1
                constant_fee_pc = 1

                if assignment['assignment'].billing_type == Assignment.FIXED_FEE:
                    constant_fee = constant_fee * invoice.pc_rate
                else:
                    constant_fee_pc = constant_fee_pc * invoice.pc_rate

                # TODO  FEE
                fees = assignment.pop('fees', [])
                for fee in fees:
                    total_amount += fee.amount * constant_fee
                    total_amount_pc += fee.amount / constant_fee_pc
                    fees_ids.append(fee.id)

                # TODO EXPENSE
                expenses = assignment.pop('expenses', [])
                for expense in expenses:
                    total_amount += expense.amount
                    total_amount_pc += expense.amount / invoice.pc_rate
                    expenses_ids.append(expense.id)
                assignment_invoice_list.append(AssignmentInvoice(
                    invoice=invoice,
                    assignment=assignment['assignment'],
                    due_date=due_date,
                    total_amount=total_amount,
                    pc_amount=total_amount_pc,
                    description='Hello',
                ))
                invoiced_fee = Fee.objects.filter(id__in=fees_ids)
                invoiced_fee.update(
                    invoice=invoice,
                    status_invoice=Fee.CLOSED,
                    pre_invoice=None,
                )

                invoiced_expense = Expense.objects.filter(id__in=expenses_ids)
                invoiced_expense.update(
                    invoice=invoice,
                    status_invoice=Expense.CLOSED,
                    pre_invoice=None,
                )
                total_amount_invoice += total_amount
                total_amount_invoice_pc += total_amount_pc
            invoice.total_amount = total_amount_invoice
            invoice.balance = total_amount_invoice
            invoice.pc_amount = total_amount_invoice_pc
            invoice.pc_balance = total_amount_invoice_pc
            invoice.save()

            AssignmentInvoice.objects.bulk_create(assignment_invoice_list)

        return invoice

    def update(self, instance, validated_data):
        type = validated_data.get('type', None)

        assignments = validated_data.pop('assignments', None)
        due_date = validated_data.get('due_date', None)

        invoice: Invoice = super(InvoiceModelSerializer, self).update(instance, validated_data)
        if type == Invoice.SIMPLE:
            total_amount_invoice = 0
            total_amount_invoice_pc = 0
            invoiced_fee = Fee.objects.filter(invoice=invoice)
            invoiced_fee.update(
                invoice=None,
                status_invoice=Fee.OPEN
            )
            invoiced_expense = Expense.objects.filter(invoice=invoice)
            invoiced_expense.update(
                invoice=None,
                status_invoice=Expense.OPEN
            )
            assignment_invoice_ids = []
            fees_ids = []
            expenses_ids = []

            for assignment in assignments:
                total_amount = 0
                total_amount_pc = 0
                constant_fee = 1
                constant_fee_pc = 1

                if assignment['assignment'].billing_type == Assignment.FIXED_FEE:
                    constant_fee = constant_fee * invoice.pc_rate
                else:
                    constant_fee_pc = constant_fee_pc * invoice.pc_rate
                # TODO  FEE
                for fee in assignment['fees']:
                    total_amount += fee.amount * constant_fee
                    total_amount_pc += fee.amount / constant_fee_pc
                    fees_ids.append(fee.id)
                # TODO EXPENSE
                for expense in assignment['expenses']:
                    total_amount += expense.amount
                    total_amount_pc += expense.amount / invoice.pc_rate
                    expenses_ids.append(expense.id)
                assignment_invoice, _ = AssignmentInvoice.objects.update_or_create(
                    invoice=invoice,
                    assignment=assignment['assignment'],
                    defaults=dict(
                        due_date=due_date,
                        total_amount=total_amount,
                        pc_amount=total_amount_pc,
                        description='Hello',
                    ))

                invoiced_fee = Fee.objects.filter(id__in=fees_ids)
                invoiced_fee.update(
                    invoice=invoice,
                    status_invoice=Fee.CLOSED,
                    pre_invoice=None,
                )

                invoiced_expense = Expense.objects.filter(id__in=expenses_ids)
                invoiced_expense.update(
                    invoice=invoice,
                    status_invoice=Expense.CLOSED,
                    pre_invoice=None,
                )
                assignment_invoice_ids.append(assignment_invoice.id)
                total_amount_invoice += total_amount
                total_amount_invoice_pc += total_amount_pc
            AssignmentInvoice.objects.exclude(id__in=assignment_invoice_ids).delete()
            invoice.total_amount = total_amount_invoice
            invoice.balance = total_amount_invoice
            invoice.pc_amount = total_amount_invoice_pc
            invoice.pc_balance = total_amount_invoice_pc
            invoice.save()
        return invoice

    # def get_approved_fees(self, obj):
    #     if obj.type == Invoice.PRE_PAYMENT:
    #         query = Fee.objects.filter(assignment__contract=obj.contract, status_approvement=Fee.APPROVED,
    #                                    status_invoice=Fee.OPEN)
    #         serializer = SelectFeeSerializer(query, many=True)
    #         return serializer.data
    #     return None
    #
    # def get_approved_expenses(self, obj):
    #     if obj.type == Invoice.PRE_PAYMENT:
    #         query = Expense.objects.filter(assignment__contract=obj.contract, status_approvement=Expense.APPROVED,
    #                                        status_invoice=Expense.OPEN)
    #         serializer = SelectExpenseSerializer(query, many=True)
    #         return serializer.data
    #     return None

    def to_representation(self, instance):
        self.fields['assignments'] = AssignmentInvoiceSerializer(many=True, required=False,
                                                                 source='assignment_invoices')
        self.fields['user'] = SelectUserSerializer(required=False)
        self.fields['client'] = SelectClientSerializer()
        self.fields['contract'] = SelectContractSerializer()
        return super().to_representation(instance)
