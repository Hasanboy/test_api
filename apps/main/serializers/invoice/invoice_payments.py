import decimal
from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Invoice, User, Transaction, CashBox, ContractConfig


class InvoicePaymentSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=11, decimal_places=2)
    notes = serializers.CharField(required=False)
    payment_date = serializers.DateField()

    def __init__(self, *args, **kwargs):
        super(InvoicePaymentSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        invoice: Invoice = self.instance
        amount = attrs.get('amount', None)

        if invoice.type == Invoice.PRE_PAYMENT:
            if invoice.balance != amount:
                errors['non_field_errors'].append(
                    'prepayment should be fully invoiced at once')

        if invoice and not invoice.contract:
            errors['non_field_errors'].append('Invoice must have contract ')
        else:
            cashbox = \
                CashBox.objects.filter(
                    bank_account=invoice.contract.bank_account)[
                    0]

            if cashbox:
                attrs.update(cashbox=cashbox)
            else:
                errors['non_field_errors'].append('CashBox not exist')

        if invoice.status_payment == Invoice.PAID:
            errors['non_field_errors'].append('Invoice already paid ')
        if invoice.balance < amount and amount > 0:
            errors['non_field_errors'].append(
                'you send  amount more than amount invoice  or amount must be zero ')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def update(self, instance, validated_data):

        amount = validated_data['amount']
        description = validated_data['notes']
        payment_date = validated_data['payment_date']
        cashbox = validated_data['cashbox']
        invoice: Invoice = self.instance

        if not description:
            description = 'By System'
        # TODO: PAY INVOICE
        if invoice.type == Invoice.SIMPLE:
            pc_amount = decimal.Decimal(amount) / invoice.pc_rate
        else:
            pc_amount = amount

        if invoice.type == Invoice.PRE_PAYMENT:
            contract_config = ContractConfig.objects.filter(
                id=invoice.contract_config.id).first()
            if contract_config:
                contract_config.is_paid = True
                contract_config.save()

        if invoice.balance == amount:
            invoice.balance = 0
            invoice.pc_balance = 0
            invoice.status_payment = Invoice.PAID
            pc_amount = pc_amount
        else:
            pc_amount = pc_amount
            invoice.balance -= amount
            invoice.pc_balance -= pc_amount
            invoice.status_payment = Invoice.OPEN
        invoice.save()

        transaction = Transaction.objects.create(
            cashbox=cashbox,
            invoice=invoice,
            contract=invoice.contract,
            created_by=self.request.user,
            amount=amount,
            pc_amount=pc_amount,
            description=description,
            payment_date=payment_date,
            type=Transaction.IN,
            category=Transaction.PAYMENT,
            status=Transaction.CONFIRM,

        )
        transaction.save()

        return validated_data
