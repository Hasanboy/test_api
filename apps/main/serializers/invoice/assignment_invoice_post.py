from collections import defaultdict

from rest_framework import serializers

from main.models import Fee, Expense, Assignment, ContractConfig


class AssignmentSerializer(serializers.Serializer):
    assignment = serializers.PrimaryKeyRelatedField(
        queryset=Assignment.objects.all())
    fees = serializers.ListSerializer(child=serializers.PrimaryKeyRelatedField(
        queryset=Fee.objects.filter(status_time=Fee.PAUSED,
                                    status_approvement=Fee.APPROVED)),
        required=False)
    expenses = serializers.ListSerializer(
        child=serializers.PrimaryKeyRelatedField(
            queryset=Expense.objects.filter(
                status_approvement=Expense.APPROVED)),
        required=False)

    amount = serializers.DecimalField(max_digits=11, decimal_places=2,
                                      required=False)

    def validate(self, attrs):
        errors = defaultdict(list)
        fees = attrs.get('fees', [])
        expenses = attrs.get('expenses', [])
        assignment = attrs.get('assignment', [])
        amount = attrs.get('amount', [])

        if assignment and not assignment.currency:
            errors['non_field_errors'].append(
                'assignment currency is required ')

        if assignment and not assignment.contract_config:
            errors['non_field_errors'].append(
                'assignment contract_configs is required ')

        if assignment and not assignment.currency.rate:
            errors['non_field_errors'].append(
                'assignment currency rate is not  defined ')

        if assignment.billing_type == Assignment.FIXED_FEE and not amount:
            errors['non_field_errors'].append(' amount is required ')

        if assignment.billing_type == Assignment.FIXED_FEE and assignment.status != Assignment.COMPLETED:
            errors['non_field_errors'].append(' assignment is not complete ')

        if assignment.billing_type == Assignment.FIXED_FEE:
            contract_config = ContractConfig.objects.filter(
                id=assignment.contract_config.id, is_paid=True).first()
            if contract_config:
                attrs.update(
                    pre_payment=contract_config.fixed_fee_pre_amount
                )

        # if assignment.billing_type == Assignment.HOURLY_BILLING and not (fees and expenses):
        #     errors['non_field_errors'].append(' fees and expenses is required ')
        for fee in fees:
            if fee.assignment != assignment:
                errors['non_field_errors'].append(
                    'fee {} is not belong this {}  assignment'.format(fee,
                                                                      assignment))
        for expense in expenses:
            if expense.assignment != assignment:
                errors['non_field_errors'].append(
                    'expense {} is not belong this {}  assignment'.format(
                        expense, assignment))

        if errors:
            raise serializers.ValidationError(errors)
        return attrs
