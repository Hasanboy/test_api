from django.core.mail import EmailMessage
from rest_framework import serializers
from rest_framework.request import Request

from config import settings
from main.models import User


class InvoiceSendSerializer(serializers.Serializer):
    body = serializers.CharField()
    subject = serializers.CharField()
    contacts = serializers.ListSerializer(child=serializers.CharField())
    send_me = serializers.BooleanField()

    def __init__(self, *args, **kwargs):
        super(InvoiceSendSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def update(self, instance, validated_data):
        body = validated_data.get('body', [])
        subject = validated_data.get('subject', [])
        contacts = validated_data.get('contacts', [])
        send_me = validated_data.get('send_me', [])
        if send_me:
            mail = self.request.user.email
            contacts.append(mail)

        message = EmailMessage(
            subject=subject,
            body=body,
            from_email=settings.EMAIL_FROM,
            to=contacts,
        )
        message.send()
        return validated_data
