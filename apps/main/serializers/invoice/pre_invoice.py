from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Fee, Expense, PreInvoice, User, Assignment
from main.models import AssignmentPreInvoice
from main.serializers.select_serializer import SelectAssignmentSerializer, SelectUserSerializer, SelectClientSerializer, \
    SelectFeeSerializer, SelectExpenseSerializer


class PreInvoiceAssignment(serializers.ModelSerializer):
    fees = SelectFeeSerializer(many=True, required=False)
    expenses = SelectExpenseSerializer(many=True, required=False)
    assignment = SelectAssignmentSerializer()

    class Meta:
        model = AssignmentPreInvoice
        fields = '__all__'


class InvoicePreviewModelSerializer(serializers.ModelSerializer):
    assignments = serializers.ListSerializer(
        child=serializers.PrimaryKeyRelatedField(queryset=Assignment.objects.all()), required=False)

    class Meta:
        model = PreInvoice
        fields = '__all__'
        read_only_fields = ['user']

    def __init__(self, *args, **kwargs):
        super(InvoicePreviewModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        type = attrs.get('type', None)
        to_date = attrs.get('to_date', None)
        from_date = attrs.get('from_date', None)
        if type == 'custom' and not (to_date and from_date):
            errors['non_field_errors'].append('to_date and from_date are required')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        assignments = validated_data.pop('assignments', None)
        type = validated_data.get('type', None)
        from_date = validated_data.get('from_date', None)
        to_date = validated_data.get('to_date', None)

        validated_data.update(user=self.user)
        pre_invoice_instance = super(InvoicePreviewModelSerializer, self).create(validated_data)
        fees = Fee.objects.filter(status_time=Fee.PAUSED, status_approvement=Fee.APPROVED, status_invoice=Fee.OPEN,
                                  status_payment=Fee.UNPAID)
        expenses = Expense.objects.filter(
            status_approvement=Expense.APPROVED, status_invoice=Expense.OPEN,
            status_payment=Expense.UNPAID

        )
        if type == 'custom':
            for assignment in assignments:
                assignment_preinvoice = AssignmentPreInvoice.objects.create(
                    pre_invoice=pre_invoice_instance,
                    assignment=assignment,

                )
                assignment_preinvoice.save()
                fees_list = list()
                expenses_list = list()

                fees = Fee.objects.filter(status_time=Fee.PAUSED, status_approvement=Fee.APPROVED,
                                          status_invoice=Fee.OPEN,
                                          status_payment=Fee.UNPAID, assignment=assignment, date__gte=from_date,
                                          date__lte=to_date)

                expenses = Expense.objects.filter(
                    status_approvement=Expense.APPROVED, status_invoice=Expense.OPEN,
                    status_payment=Expense.UNPAID, assignment=assignment, date__gte=from_date, date__lte=to_date

                )

                for fee in fees:
                    fees_list.append(fee.id)

                for expense in expenses:
                    expenses_list.append(expense.id)
                pre_invoiced_expense = Expense.objects.filter(id__in=expenses_list)
                pre_invoiced_expense.update(
                    pre_invoice=assignment_preinvoice,
                )

                pre_invoiced_fee = Fee.objects.filter(id__in=fees_list)
                pre_invoiced_fee.update(
                    pre_invoice=assignment_preinvoice,
                )
        if type == 'all':
            for assignment in assignments:
                assignment_preinvoice = AssignmentPreInvoice.objects.create(
                    pre_invoice=pre_invoice_instance,
                    assignment=assignment,
                )
                assignment_preinvoice.save()

                fees_list = list()
                expenses_list = list()

                fees = Fee.objects.filter(status_time=Fee.PAUSED, status_approvement=Fee.APPROVED,
                                          status_invoice=Fee.OPEN,
                                          status_payment=Fee.UNPAID, assignment=assignment)

                expenses = Expense.objects.filter(
                    status_approvement=Expense.APPROVED, status_invoice=Expense.OPEN,
                    status_payment=Expense.UNPAID, assignment=assignment

                )

                for fee in fees:
                    fees_list.append(fee.id)

                for expense in expenses:
                    expenses_list.append(
                        expense.id)
                pre_invoiced_expense = Expense.objects.filter(id__in=expenses_list)
                pre_invoiced_expense.update(
                    pre_invoice=assignment_preinvoice,
                )

                pre_invoiced_fee = Fee.objects.filter(id__in=fees_list)
                pre_invoiced_fee.update(
                    pre_invoice=assignment_preinvoice
                )

        if type == 'expenses':
            for assignment in assignments:
                assignment_preinvoice = AssignmentPreInvoice.objects.create(
                    pre_invoice=pre_invoice_instance,
                    assignment=assignment,
                )
                assignment_preinvoice.save()
                expenses_list = list()
                expenses = Expense.objects.filter(
                    status_approvement=Expense.APPROVED, status_invoice=Expense.OPEN,
                    status_payment=Expense.UNPAID, assignment=assignment

                )
                for expense in expenses:
                    expenses_list.append(expense.id)
                pre_invoiced_expense = Expense.objects.filter(id__in=expenses_list)
                pre_invoiced_expense.update(
                    pre_invoice=assignment_preinvoice
                )

        return pre_invoice_instance

    def to_representation(self, instance):
        self.fields['assignments'] = PreInvoiceAssignment(many=True, required=False,
                                                          source='assignment_preinvoices')
        self.fields['user'] = SelectUserSerializer(required=False)
        self.fields['fees'] = SelectFeeSerializer(many=True, required=False)
        self.fields['client'] = SelectClientSerializer()
        return super().to_representation(instance)
