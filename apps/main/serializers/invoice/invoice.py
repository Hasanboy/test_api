from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Invoice, User, Currency, Contract, ContractConfig
from main.serializers.invoice.assignment_invoice_get import \
    AssignmentInvoiceSerializer
from main.serializers.invoice.assignment_invoice_post import \
    AssignmentSerializer
from main.serializers.select_serializer import SelectClientSerializer, \
    SelectUserSerializer, SelectContractSerializer, \
    SelectContractConfigSerializer


class InvoiceModelSerializer(serializers.ModelSerializer):
    assignments = AssignmentSerializer(many=True, required=False)
    contract_config = serializers.PrimaryKeyRelatedField(
        queryset=ContractConfig.objects.filter(is_paid=False,
                                               is_invoiced=False,
                                               billing_type=ContractConfig.FIXED_FEE),
        required=False)

    class Meta:
        model = Invoice
        fields = '__all__'
        read_only_fields = ['user', 'pc_rate', 'pc_amount', 'pc_date',
                            'pc_balance']
        extra_kwargs = dict(
            total_amount=dict(required=False)
        )

    def __init__(self, *args, **kwargs):
        super(InvoiceModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        issue_date = attrs.get('issue_date', None)
        total_amount = attrs.get('total_amount', None)
        type = attrs.get('type', None)
        assignments = attrs.get('assignments', None)
        contract_config = attrs.get('contract_config', None)

        attrs.update(
            user=self.user,
        )

        if not type:
            errors['non_field_errors'].append('type is required')

        if self.instance and self.instance.type != type:
            errors['non_field_errors'].append('type is cannot be changed')

        # if type == Invoice.PRE_PAYMENT and not total_amount:
        #     errors['non_field_errors'].append('total_amount is required')
        # else:
        #     attrs.update(
        #         pc_balance=total_amount,
        #         pc_amount=total_amount,
        #         balance=total_amount,
        #     )
        if self.instance and self.instance.contract_config:
            errors['non_field_errors'].append('you cannot update')

        if type == Invoice.PRE_PAYMENT and not contract_config:
            errors['non_field_errors'].append('total amount is required')
        if type == Invoice.PRE_PAYMENT and contract_config:
            if contract_config.fixed_fee_pre_amount <= 0:
                errors['non_field_errors'].append(
                    'fixed_fee_pre_amount is null')
            else:
                if contract_config.contract.currency.rate:
                    rate = contract_config.contract.currency.rate
                else:
                    rate = 1
                attrs.update(
                    pc_balance=contract_config.fixed_fee_pre_amount / rate,
                    pc_amount=contract_config.fixed_fee_pre_amount / rate,
                    balance=contract_config.fixed_fee_pre_amount,
                    total_amount=contract_config.fixed_fee_pre_amount,
                    contract=contract_config.contract,
                )
                contract_config.is_invoiced = True
                contract_config.save()
        if type == Invoice.SIMPLE:
            attrs.update(
                total_amount=0,
            )
            currency = assignments[0]
            currency_list = []
            contract_list = []
            for assignment in assignments:
                currency_list.append(assignment['assignment'].currency.id)
                if assignment['assignment'].contract_config.contract:
                    contract_list.append(
                        assignment['assignment'].contract_config.contract.id)
            count_contract_list = len(set(contract_list))
            if count_contract_list and count_contract_list != 1:
                errors['non_field_errors'].append('contract is not same')

            if count_contract_list:
                contract = Contract.objects.filter(id=contract_list[0]).first()
                if contract:
                    attrs.update(
                        contract=contract,
                    )

            primary_currency = Currency.objects.filter(is_primary=True).first()
            if not primary_currency:
                errors['non_field_errors'].append(
                    'primary currency is not defined')
            else:
                attrs.update(
                    pc_rate=currency['assignment'].currency.rate,
                )
        if not issue_date:
            errors['non_field_errors'].append('issue_date is  required ')
        if self.instance and self.instance.status_payment != Invoice.DRAFT:
            errors['non_field_errors'].append('Invoice already paid ')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        type = validated_data.get('type', None)
        assignments = validated_data.pop('assignments', None)
        due_date = validated_data.get('due_date', None)
        invoice = super(InvoiceModelSerializer, self).create(validated_data)
        if type == Invoice.SIMPLE:
            Invoice.objects.create_calculate_amount(assignments, invoice,
                                                    due_date)
        return invoice

    def update(self, instance, validated_data):
        type = validated_data.get('type', None)
        assignments = validated_data.pop('assignments', None)
        due_date = validated_data.get('due_date', None)
        invoice: Invoice = super(InvoiceModelSerializer, self).update(instance,
                                                                      validated_data)
        if type == Invoice.SIMPLE:
            Invoice.objects.update_calculate_amount(assignments, invoice,
                                                    due_date)
        return invoice

    def to_representation(self, instance):
        self.fields['assignments'] = AssignmentInvoiceSerializer(many=True,
                                                                 required=False,
                                                                 source='assignment_invoices')
        self.fields['user'] = SelectUserSerializer(required=False)
        self.fields['client'] = SelectClientSerializer()
        self.fields['contract'] = SelectContractSerializer()
        self.fields['contract_config'] = SelectContractConfigSerializer(
            required=False)
        return super().to_representation(instance)
