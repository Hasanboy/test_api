from datetime import datetime
from datetime import timedelta
from rest_framework import serializers

from main.models import InvoiceSettings
from main.serializers.select_serializer import SelectClientSerializer


class InvoiceSettingsModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceSettings
        fields = [
            'id',
            'issue_date',
            'client',
            'amount',
            'frequency_type',
            'frequency_interval',
            'deactivated',
            'due_date_interval',
            'next_date_interval',
        ]
        read_only_fields = ['due_date_interval', 'next_date_interval']

    def create(self, validated_data):
        type = validated_data.get('frequency_type', [])
        frequency_interval = validated_data.get('frequency_interval', [])
        today_date = datetime.now()
        validated_data.update(
            due_date_interval=today_date.date()

        )
        if type == InvoiceSettings.DAY:
            next_date_interval = datetime.now() + timedelta(
                days=frequency_interval)
            validated_data.update(
                next_date_interval=next_date_interval.date()
            )
        if type == InvoiceSettings.MONTH:
            next_date_interval = datetime.now() + timedelta(
                days=(frequency_interval * 30))
            validated_data.update(
                next_date_interval=next_date_interval.date()
            )
        if type == InvoiceSettings.YEAR:
            next_date_interval = datetime.now() + timedelta(
                days=(frequency_interval * 30 * 12))
            validated_data.update(
                next_date_interval=next_date_interval.date()
            )

        invoice_settings = super(InvoiceSettingsModelSerializer, self).create(
            validated_data)
        return invoice_settings

    def to_representation(self, instance):
        self.fields['client'] = SelectClientSerializer()
        return super(InvoiceSettingsModelSerializer, self).to_representation(
            instance)
