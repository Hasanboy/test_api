from rest_framework import serializers

from main.models import Invoice
from datetime import datetime


class InvoiceDeliveredSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        invoice: Invoice = self.instance
        invoice.status_payment = Invoice.DELIVERED
        invoice.delivered_date = datetime.now()
        invoice.save()
        return validated_data
