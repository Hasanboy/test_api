from rest_framework import serializers

from main.models import Fee, Expense, AssignmentInvoice
from main.serializers.select_serializer import SelectAssignmentSerializer, \
    SelectFeeSerializer, SelectExpenseSerializer


class AssignmentInvoiceSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    due_date = serializers.DateField()
    total_amount = serializers.DecimalField(max_digits=11, decimal_places=2)
    pc_amount = serializers.DecimalField(max_digits=11, decimal_places=2)
    description = serializers.CharField()
    assignment = SelectAssignmentSerializer()
    fees = serializers.SerializerMethodField(required=False)
    expenses = serializers.SerializerMethodField(required=False)

    def get_fees(self, obj: AssignmentInvoice):
        query = Fee.objects.filter(invoice=obj.invoice, assignment=obj.assignment)
        serializer = SelectFeeSerializer(query, many=True)
        return serializer.data

    def get_expenses(self, obj: AssignmentInvoice):
        query = Expense.objects.filter(invoice=obj.invoice, assignment=obj.assignment)
        serializer = SelectExpenseSerializer(query, many=True)
        return serializer.data
