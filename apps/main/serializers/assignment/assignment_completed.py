from collections import defaultdict

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Assignment, User


class AssignmentCompletedSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        assignment: Assignment = self.instance

        if assignment.team_leader.id != self.user.id:
            errors['non_field_errors'].append(
                'user is not team leader')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def update(self, instance, validated_data):
        assignment: Assignment = self.instance
        assignment.status = Assignment.COMPLETED
        assignment.save()
        return validated_data
