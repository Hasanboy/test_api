from rest_framework import serializers
from rest_framework.request import Request

from main.models import Assignment, BankAccount, Currency, Branch, Contract, \
    User, Fee, Expense
from main.serializers.select_serializer import SelectBranchSerializer, \
    SelectUserSerializer, SelectClientSerializer, \
    SelectTagsSerializer, SelectBankAccountSerializer, \
    SelectCurrencySerializer, SelectContractSerializer, \
    SelectClientContactSerializer, SelectFeeSerializer, \
    SelectExpenseSerializer, SelectRateSerializers, \
    SelectContractConfigSerializer


class AssignmentSubmitSerializer(serializers.ModelSerializer):
    invoice_expense_amount = serializers.DecimalField(max_digits=11,
                                                      decimal_places=2,
                                                      required=False,
                                                      allow_null=True)
    uninvoice_expense_amount = serializers.DecimalField(max_digits=11,
                                                        decimal_places=2,
                                                        required=False,
                                                        allow_null=True)
    invoice_fee_amount = serializers.DecimalField(max_digits=11,
                                                  decimal_places=2,
                                                  required=False,
                                                  allow_null=True)
    uninvoice_fee_amount = serializers.DecimalField(max_digits=11,
                                                    decimal_places=2,
                                                    required=False,
                                                    allow_null=True)
    invoice_fee_hours = serializers.IntegerField(required=False,
                                                 allow_null=True)
    uninvoice_fee_hours = serializers.IntegerField(required=False,
                                                   allow_null=True)
    billing_type = serializers.ChoiceField(choices=Assignment.BILLING_TYPE,
                                           required=False)
    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.all(), required=False)
    contract = serializers.PrimaryKeyRelatedField(
        queryset=Contract.objects.all(), required=False)
    currency = serializers.PrimaryKeyRelatedField(
        queryset=Currency.objects.all(), required=False)
    payment_destination = serializers.PrimaryKeyRelatedField(
        queryset=Branch.objects.all(), required=False)
    rates = SelectRateSerializers(many=True, required=False)
    fees = serializers.SerializerMethodField(required=False)
    expenses = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Assignment
        fields = '__all__'
        extra_kwargs = dict(
            tags=dict(allow_empty=True),
            success_fee=dict(required=False)
        )
        read_only_fields = ['owner']

    def __init__(self, *args, **kwargs):
        super(AssignmentSubmitSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def get_fees(self, obj):
        query = Fee.objects.filter(assignment=obj.id,
                                   status_approvement=Fee.SUBMITTED)
        serializer = SelectFeeSerializer(query, many=True)
        return serializer.data

    def get_expenses(self, obj):
        query = Expense.objects.filter(assignment=obj.id,
                                       status_approvement=Expense.SUBMITTED)
        serializer = SelectExpenseSerializer(query, many=True)
        return serializer.data

    def to_representation(self, instance):
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['payment_destination'] = SelectBranchSerializer()
        self.fields['client_contact'] = SelectClientContactSerializer()
        self.fields['currency'] = SelectCurrencySerializer()
        self.fields['bank_account'] = SelectBankAccountSerializer()
        self.fields['client'] = SelectClientSerializer()
        self.fields['service_delivered_to'] = SelectClientSerializer()
        self.fields['originated_by'] = SelectUserSerializer()
        self.fields['team_leader'] = SelectUserSerializer()
        self.fields['owner'] = SelectUserSerializer(required=False)
        self.fields['work_group'] = SelectUserSerializer(many=True)
        self.fields['tags'] = SelectTagsSerializer(many=True)
        return super(AssignmentSubmitSerializer, self).to_representation(
            instance)


class AssignmentApprovedSerializer(serializers.ModelSerializer):
    invoice_expense_amount = serializers.DecimalField(max_digits=11,
                                                      decimal_places=2,
                                                      required=False,
                                                      allow_null=True)
    uninvoice_expense_amount = serializers.DecimalField(max_digits=11,
                                                        decimal_places=2,
                                                        required=False,
                                                        allow_null=True)
    invoice_fee_amount = serializers.DecimalField(max_digits=11,
                                                  decimal_places=2,
                                                  required=False,
                                                  allow_null=True)
    uninvoice_fee_amount = serializers.DecimalField(max_digits=11,
                                                    decimal_places=2,
                                                    required=False,
                                                    allow_null=True)
    invoice_fee_hours = serializers.IntegerField(required=False,
                                                 allow_null=True)
    uninvoice_fee_hours = serializers.IntegerField(required=False,
                                                   allow_null=True)
    billing_type = serializers.ChoiceField(choices=Assignment.BILLING_TYPE,
                                           required=False)
    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.all(), required=False)
    contract = serializers.PrimaryKeyRelatedField(
        queryset=Contract.objects.all(), required=False)
    currency = serializers.PrimaryKeyRelatedField(
        queryset=Currency.objects.all(), required=False)
    payment_destination = serializers.PrimaryKeyRelatedField(
        queryset=Branch.objects.all(), required=False)
    rates = SelectRateSerializers(many=True, required=False)
    fees = serializers.SerializerMethodField(required=False)
    expenses = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Assignment
        fields = '__all__'
        extra_kwargs = dict(
            tags=dict(allow_empty=True),
            success_fee=dict(required=False)
        )
        read_only_fields = ['owner']

    def __init__(self, *args, **kwargs):
        super(AssignmentApprovedSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def get_fees(self, obj):
        query = Fee.objects.filter(assignment=obj.id,
                                   status_approvement=Fee.APPROVED)
        serializer = SelectFeeSerializer(query, many=True)
        return serializer.data

    def get_expenses(self, obj):
        query = Expense.objects.filter(assignment=obj.id,
                                       status_approvement=Expense.APPROVED)
        serializer = SelectExpenseSerializer(query, many=True)
        return serializer.data

    def to_representation(self, instance):
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['payment_destination'] = SelectBranchSerializer()
        self.fields['client_contact'] = SelectClientContactSerializer()
        self.fields['currency'] = SelectCurrencySerializer()
        self.fields['bank_account'] = SelectBankAccountSerializer()
        self.fields['client'] = SelectClientSerializer()
        self.fields['service_delivered_to'] = SelectClientSerializer()
        self.fields['originated_by'] = SelectUserSerializer()
        self.fields['team_leader'] = SelectUserSerializer()
        self.fields['owner'] = SelectUserSerializer(required=False)
        self.fields['work_group'] = SelectUserSerializer(many=True)
        self.fields['tags'] = SelectTagsSerializer(many=True)
        return super(AssignmentApprovedSerializer, self).to_representation(
            instance)


class AssignmentPureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assignment
        # fields = '__all__'
        fields = ['id', 'name', 'client', 'team_leader', 'tags', 'status',
                  'billing_type']
        extra_kwargs = dict(
            tags=dict(allow_empty=True),
            success_fee=dict(required=False)
        )
        read_only_fields = ['owner']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def to_representation(self, instance):
        # self.fields['branch'] = SelectBranchSerializer()
        # self.fields['payment_destination'] = SelectBranchSerializer()
        # self.fields['client_contact'] = SelectClientContactSerializer()
        # self.fields['contract_config'] = SelectContractConfigSerializer()
        # self.fields['currency'] = SelectCurrencySerializer()
        # self.fields['bank_account'] = SelectBankAccountSerializer()
        # self.fields['service_delivered_to'] = SelectClientSerializer()
        # self.fields['originated_by'] = SelectUserSerializer()
        # self.fields['owner'] = SelectUserSerializer(required=False)
        # self.fields['work_group'] = SelectUserSerializer(many=True)
        self.fields['client'] = SelectClientSerializer()
        self.fields['team_leader'] = SelectUserSerializer()
        self.fields['tags'] = SelectTagsSerializer(many=True)
        return super().to_representation(instance)
