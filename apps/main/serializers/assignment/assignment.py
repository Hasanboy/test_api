from collections import defaultdict
import decimal

from rest_framework import serializers
from rest_framework.request import Request

from main.models import Assignment, BankAccount, Currency, Branch, Contract, \
    User, ContractConfig, Fee, ContractRate
from main.serializers.select_serializer import SelectBranchSerializer, \
    SelectUserSerializer, SelectClientSerializer, \
    SelectTagsSerializer, SelectBankAccountSerializer, \
    SelectCurrencySerializer, \
    SelectClientContactSerializer, SelectContractConfigSerializer


class AssignmentModelSerializer(serializers.ModelSerializer):
    invoice_expense_amount = serializers.DecimalField(max_digits=11,
                                                      decimal_places=2,
                                                      required=False,
                                                      allow_null=True)
    uninvoice_expense_amount = serializers.DecimalField(max_digits=11,
                                                        decimal_places=2,
                                                        required=False,
                                                        allow_null=True)
    invoice_fee_amount = serializers.DecimalField(max_digits=11,
                                                  decimal_places=2,
                                                  required=False,
                                                  allow_null=True)
    uninvoice_fee_amount = serializers.DecimalField(max_digits=11,
                                                    decimal_places=2,
                                                    required=False,
                                                    allow_null=True)
    invoice_fee_hours = serializers.IntegerField(required=False,
                                                 allow_null=True)
    uninvoice_fee_hours = serializers.IntegerField(required=False,
                                                   allow_null=True)
    billing_type = serializers.ChoiceField(choices=Assignment.BILLING_TYPE,
                                           required=False)
    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.all(), required=False)
    contract_config = serializers.PrimaryKeyRelatedField(
        queryset=ContractConfig.objects.all(), required=False)
    currency = serializers.PrimaryKeyRelatedField(
        queryset=Currency.objects.all(), required=False)
    payment_destination = serializers.PrimaryKeyRelatedField(
        queryset=Branch.objects.all(), required=False)

    class Meta:
        model = Assignment
        fields = '__all__'
        extra_kwargs = dict(
            tags=dict(allow_empty=True),
        )
        read_only_fields = ['owner', 'contract']

    def __init__(self, *args, **kwargs):
        super(AssignmentModelSerializer, self).__init__(*args, **kwargs)
        self.request: Request = self.context['request']
        if self.request:
            self.user: User = self.request.user

    def validate(self, attrs):
        errors = defaultdict(list)
        contract_config = attrs.get('contract_config', None)
        is_billable = attrs.get('is_billable', None)
        billing_type = attrs.get('billing_type', None)

        payment_destination = attrs.get('payment_destination', None)
        bank_account = attrs.get('bank_account', None)
        currency = attrs.get('currency', None)
        payment_duration = attrs.get('payment_duration', None)

        # TODO FIXED_FEE
        fixed_fee_amount = attrs.get('fixed_fee_amount', None)
        fixed_fee_expenses_included_in_fee = attrs.get(
            'fixed_fee_expenses_included_in_fee', None)
        # TODO HOURLY
        hourly_has_fee_ceiling = attrs.get('hourly_has_fee_ceiling', None)
        hourly_fee_ceiling = attrs.get('hourly_fee_ceiling', None)
        attrs.pop('rates', [])

        if contract_config:
            assignment = Assignment.objects.filter(
                contract_config=contract_config)
            if self.instance:
                assignment = assignment.exclude(pk=self.instance.id)
            if assignment.exists():
                errors['non_field_errors'].append(
                    'contract_config has already taken')
        if not self.instance:
            attrs.update(owner=self.user)

        if self.instance and self.instance.contract_config and not contract_config:
            errors['non_field_errors'].append(
                'you cannot remove contract_config after create')

        # TODO NOT CONTRACT
        if not contract_config:
            if is_billable and not billing_type:
                errors['non_field_errors'].append('billing_type is required')
            if is_billable and (
                    billing_type == Assignment.FIXED_FEE or billing_type == Assignment.HOURLY_BILLING):
                if not payment_destination:
                    errors['non_field_errors'].append(
                        'payment_destination is required')
                if not bank_account:
                    errors['non_field_errors'].append(
                        'bank_account is required')
                if not currency:
                    errors['non_field_errors'].append('currency is required')
                if not payment_duration:
                    errors['non_field_errors'].append(
                        'payment_duration is required')
            if billing_type == Assignment.FIXED_FEE and fixed_fee_amount <= 0:
                errors['non_field_errors'].append(
                    'fixed_fee_amount must be greater than ZERO')

            if hourly_has_fee_ceiling and not hourly_fee_ceiling:
                errors['non_field_errors'].append(
                    'hourly_fee_ceiling is required')

            if fixed_fee_expenses_included_in_fee and not fixed_fee_amount:
                errors['non_field_errors'].append(
                    'fixed_fee_amount is required')

        # TODO CONTRACT
        if is_billable and contract_config:
            if not contract_config.rates.exists() and contract_config.billing_type == ContractConfig.HOURLY_BILLING:
                errors['non_field_errors'].append(
                    'contract_config must have rate')
            attrs.update(
                name=contract_config.internal_assignment_name,
                client=contract_config.contract.client,
                branch=contract_config.contract.branch,
                currency=contract_config.contract.currency,
                bank_account=contract_config.contract.bank_account,
                payment_duration=contract_config.contract.payment_duration,
                billing_type=contract_config.billing_type,
                team_leader=contract_config.team_leader,
                hourly_has_fee_ceiling=contract_config.hourly_has_fee_ceiling,
                hourly_fee_ceiling=contract_config.hourly_fee_ceiling,
                fixed_fee_pre_amount=contract_config.fixed_fee_pre_amount,
                fixed_fee_amount=contract_config.fixed_fee_amount,
                fixed_fee_expenses_included_in_fee=contract_config.fixed_fee_expenses_included_in_fee,
            )

            if is_billable and not billing_type:
                errors['non_field_errors'].append('billing_type is required')
            if is_billable and (
                    billing_type == Assignment.FIXED_FEE or billing_type == Assignment.HOURLY_BILLING):
                if not payment_destination:
                    errors['non_field_errors'].append(
                        'payment_destination is required')
                if not bank_account:
                    errors['non_field_errors'].append(
                        'bank_account is required')
                if not currency:
                    errors['non_field_errors'].append('currency is required')
                if not payment_duration:
                    errors['non_field_errors'].append(
                        'payment_duration is required')
            if billing_type == Assignment.FIXED_FEE and fixed_fee_amount <= 0:
                errors['non_field_errors'].append(
                    'fixed_fee_amount must be greater than ZERO')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def update(self, instance, validated_data):
        assignment: Assignment = super(AssignmentModelSerializer, self).update(
            instance, validated_data)
        assignment.save()
        fees = Fee.objects.filter(assignment=assignment)
        if assignment.billing_type == Assignment.HOURLY_BILLING and assignment.contract_config:
            for fee in fees:
                rate = ContractRate.objects.filter(
                    contract_config=assignment.contract_config,
                    position=fee.user.position
                ).first()
                rate = rate.amount_per_hour
                fee.amount = decimal.Decimal(
                    fee.total_duration) * decimal.Decimal(rate / 60)
                fee.save()

        if assignment.billing_type == Assignment.FIXED_FEE and assignment.contract_config:
            for fee in fees:
                fee.amount = None
                fee.start_time = None
                fee.end_time = None
                fee.status_time = Fee.PAUSED
                fee.save()

        return assignment

    def to_representation(self, instance):
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['payment_destination'] = SelectBranchSerializer()
        self.fields['client_contact'] = SelectClientContactSerializer()
        self.fields['contract_config'] = SelectContractConfigSerializer()
        self.fields['currency'] = SelectCurrencySerializer()
        self.fields['bank_account'] = SelectBankAccountSerializer()
        self.fields['client'] = SelectClientSerializer()
        self.fields['service_delivered_to'] = SelectClientSerializer()
        self.fields['originated_by'] = SelectUserSerializer()
        self.fields['team_leader'] = SelectUserSerializer()
        self.fields['owner'] = SelectUserSerializer(required=False)
        self.fields['work_group'] = SelectUserSerializer(many=True)
        self.fields['tags'] = SelectTagsSerializer(many=True)
        return super(AssignmentModelSerializer, self).to_representation(
            instance)
