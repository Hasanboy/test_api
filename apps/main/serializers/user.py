from collections import defaultdict
from rest_framework import serializers
from main.models import User, Role, Position, Branch
from main.serializers.file import FileSerializer
from main.serializers.position import PositionModelSerializer
from main.serializers.role import RoleModelSerializer
from main.serializers.select_serializer import SelectBranchSerializer


class CheckTokenSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=255)


class UserCreateModelSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=255, required=False,
                                     write_only=True)
    role = serializers.PrimaryKeyRelatedField(queryset=Role.objects.all())
    position = serializers.PrimaryKeyRelatedField(
        queryset=Position.objects.all())
    branch = serializers.PrimaryKeyRelatedField(queryset=Branch.objects.all(),
                                                required=False)

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'full_name', 'position',
                  'photo', 'role', 'user_type', 'salary',
                  'branch']
        extra_kwargs = dict(
            position=dict(required=True),
            user_type=dict(required=True)
        )
        read_only_fields = ['user_type']

    def validate(self, attrs):
        errors = defaultdict(list)
        users = User.objects.all()
        users = users.filter(username=attrs['username'])

        if self.instance:
            users = users.exclude(pk=self.instance.id)
        if users.exists():
            errors['username'].append('Username has already taken')
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        role = validated_data.get('role')
        validated_data.update(user_type=User.ADMIN)
        password = validated_data.pop('password', None)
        user = super(UserCreateModelSerializer, self).create(validated_data)
        if password:
            user.set_password(password)
            user.save()
        role_groups = list(role.groups.all())
        user.groups.add(*role_groups)
        return user

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        role = validated_data.get('role', None)
        user = super(UserCreateModelSerializer, self).update(instance,
                                                             validated_data)
        if role:
            role_groups = list(role.groups.all())
            instance.groups.set(role_groups)
        if password:
            user.set_password(password)
            user.save()
        return user

    def to_representation(self, instance):
        self.fields['position'] = PositionModelSerializer()
        self.fields['role'] = RoleModelSerializer()
        self.fields['branch'] = SelectBranchSerializer(required=False)
        self.fields['photo'] = FileSerializer(context=self.context,
                                              required=False)
        return super(UserCreateModelSerializer, self).to_representation(
            instance)
