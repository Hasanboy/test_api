from collections import defaultdict

from rest_framework import serializers

from main.models import Client, User, ClientContact
from main.serializers.client_contact import ClientContactSerializer
from main.serializers.select_serializer import SelectClientBalanceSerializer
from main.serializers.tags import TagsSelectSerializer


class ClientModelSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=255, write_only=True, required=False)
    client_balance = serializers.DecimalField(max_digits=11, decimal_places=2, required=False)
    client_debt = serializers.DecimalField(max_digits=11, decimal_places=2, required=False)
    contacts = ClientContactSerializer(
        many=True,
        required=False
    )

    client_balances = SelectClientBalanceSerializer(
        many=True,
        required=False
    )

    class Meta:
        model = Client
        fields = '__all__'
        extra_kwargs = dict(
            tags=dict(allow_empty=True),
            email=dict(required=True)
        )

    def validate(self, attrs):
        errors = defaultdict(list)
        users = User.objects.filter(username=attrs['email'])
        if not self.instance:
            if not attrs.get('password'):
                errors['password'].append('This field is required')
        if self.instance:
            users = users.exclude(client=self.instance.id)
        if users.exists():
            errors['email'].append('This email is used')
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        email = validated_data.get('email', None)
        contacts = validated_data.pop('contacts', None)

        client = super(ClientModelSerializer, self).create(validated_data)
        user = User.objects.create(username=email, client=client, user_type=User.CLIENT)
        user.set_password(password)
        user.save()
        if contacts:
            client_list = []
            for contact in contacts:
                name = contact.pop('name', None)
                email = contact.pop('email', None)
                phone = contact.pop('phone', None)
                position = contact.pop('position', None)
                client_list.append(ClientContact(
                    client=client,
                    name=name,
                    phone=phone,
                    email=email,
                    position=position
                ))
            ClientContact.objects.bulk_create(client_list)
        return client

    def update(self, instance, validated_data):
        validated_data.pop('contacts', None)
        password = validated_data.pop('password', None)
        email = validated_data.get('email', None)
        client: Client = super(ClientModelSerializer, self).update(instance, validated_data)
        user = client.user
        if password:
            user.set_password(password)
        if email:
            user.username = email
        user.save()

        return client

    def to_representation(self, instance):
        self.fields['contacts'] = ClientContactSerializer(many=True, required=False, source='client_contacts')
        self.fields['tags'] = TagsSelectSerializer(many=True, required=False)
        return super().to_representation(instance)
