from rest_framework import serializers

from main.models import Branch, Contract


class NextContractSerializer(serializers.Serializer):
    branch = serializers.PrimaryKeyRelatedField(queryset=Branch.objects.filter(code__isnull=False))
    next_contract = serializers.CharField(required=False)

    def create(self, validated_data):
        branch = validated_data.get('branch', [])
        last_contract = Contract.objects.all().first()
        next_contract_id = last_contract.id + 1
        if next_contract_id < 10:
            next_contract = 'C{}000{}'.format(branch.code, next_contract_id)
        elif next_contract_id < 100:
            next_contract = 'C{}00{}'.format(branch.code, next_contract_id)
        elif next_contract_id < 1000:
            next_contract = 'C{}0{}'.format(branch.code, next_contract_id)
        else:
            next_contract = 'C{}{}'.format(branch.code, next_contract_id)
        validated_data.update(
            next_contract=next_contract
        )
        return validated_data
