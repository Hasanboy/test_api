# def update(self, instance, validated_data):
#     rates = validated_data.pop('rates', None)
#     billing_type = validated_data.get('billing_type', None)
#     contract: Contract = super(ContractModelSerializer, self).update(instance, validated_data)
#     contract.save()
#     assignments = Assignment.objects.filter(
#         contract=contract
#     )
#     if assignments:
#         assignments.update(
#             client=contract.client,
#             branch=contract.branch,
#             currency=contract.currency,
#             bank_account=contract.bank_account,
#             success_fee=contract.success_fee,
#             dead_line=contract.dead_line,
#             payment_duration=contract.payment_duration,
#             fixed_fee_amount=contract.fixed_fee_amount,
#             fixed_fee_expenses_included_in_fee=contract.fixed_fee_expenses_included_in_fee,
#
#         )
#     if billing_type == Contract.HOURLY_BILLING and rates:
#         contract_ids = []
#         for rate in rates:
#             position = rate.pop('position', None)
#             amount_per_hour = rate.pop('amount_per_hour', None)
#             contract_rate, _ = ContractRate.objects.update_or_create(
#                 position=position,
#                 contract=contract,
#                 defaults=dict(amount_per_hour=amount_per_hour)
#             )
#             assignment_rate = AssignmentRate.objects.filter(contract=contract_rate.contract,
#                                                             position=contract_rate.position).first()
#
# if assignment_rate:
#     assignment_rate.amount_per_hour = contract_rate.amount_per_hour
#
#             contract_ids.append(contract_rate.id)
#         ContractRate.objects.exclude(id__in=contract_ids).delete()
#     return contract
