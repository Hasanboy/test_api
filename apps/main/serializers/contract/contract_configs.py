from collections import defaultdict

from rest_framework import serializers

from main.models import ContractConfig, Assignment, Position, User
from main.serializers.select_serializer import SelectAssignmentSerializer, \
    SelectUserSerializer, SelectRateSerializers


class ContractConfigSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    external_assignment_name = serializers.CharField()
    internal_assignment_name = serializers.CharField()
    rates = SelectRateSerializers(many=True, required=False)
    team_leader = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), required=False)
    billing_type = serializers.CharField(required=False)
    fixed_fee_amount = serializers.DecimalField(max_digits=11,
                                                decimal_places=2,
                                                required=False)
    fixed_fee_pre_amount = serializers.DecimalField(max_digits=11,
                                                    decimal_places=2,
                                                    required=False)
    fixed_fee_expenses_included_in_fee = serializers.BooleanField(
        default=False, required=False)
    hourly_fee_ceiling = serializers.DecimalField(max_digits=11,
                                                  decimal_places=2,
                                                  required=False)
    hourly_has_fee_ceiling = serializers.BooleanField(default=False,
                                                      required=False)
    assignment = serializers.SerializerMethodField(required=False)
    is_paid = serializers.BooleanField(required=False)
    is_invoiced = serializers.BooleanField(required=False)

    def get_assignment(self, obj: ContractConfig):
        try:
            assignment = Assignment.objects.get_uninvoices_time(obj)
            serializers = SelectAssignmentSerializer(assignment)
            return serializers.data
        except:
            return None

    def validate_rates(self, values):
        errors = list()
        rate_list = list()
        for value in values:
            position = value.get('position', None)
            rate_list.append(position)
        if len(set(rate_list)) != len(rate_list):
            errors.append('position are duplicated')

        if errors:
            raise serializers.ValidationError(errors)
        return values

    def validate(self, attrs):
        errors = defaultdict(list)
        billing_type = attrs.get('billing_type', None)
        rates = attrs.get('rates', None)

        if billing_type == ContractConfig.HOURLY_BILLING and not rates:
            errors['non_field_errors'].append('rates are required')

        if billing_type == ContractConfig.HOURLY_BILLING and rates:
            count_position = Position.objects.all().count()
            rate_list = list()
            for value in rates:
                position = value.get('position', None)
                rate_list.append(position)
            if len(set(rate_list)) != count_position:
                errors['non_field_errors'].append('Take all positions')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def to_representation(self, instance):
        self.fields['team_leader'] = SelectUserSerializer()
        return super().to_representation(instance)
