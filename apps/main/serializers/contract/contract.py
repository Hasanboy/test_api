from collections import defaultdict
from django.db import transaction

from rest_framework import serializers

from main.models import Contract, ContractRate, Branch, BankAccount, Currency, \
    Assignment, ContractConfig
from main.serializers.contract.contract_configs import ContractConfigSerializer
from main.serializers.select_serializer import SelectBranchSerializer, \
    SelectClientSerializer, \
    SelectCurrencySerializer, SelectBankAccountSerializer


class ContractModelSerializer(serializers.ModelSerializer):
    configs = ContractConfigSerializer(many=True, required=False)
    branch = serializers.PrimaryKeyRelatedField(
        queryset=Branch.objects.filter(code__isnull=False))

    bank_account = serializers.PrimaryKeyRelatedField(
        queryset=BankAccount.objects.all())
    currency = serializers.PrimaryKeyRelatedField(
        queryset=Currency.objects.all())

    class Meta:
        model = Contract
        fields = '__all__'
        read_only_fields = ['contract_number']

    def validate(self, attrs):
        errors = defaultdict(list)
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    @transaction.atomic
    def create(self, validated_data):

        configs = validated_data.pop('configs', None)
        contract = super(ContractModelSerializer, self).create(validated_data)
        contract.save()

        if contract.id < 10:
            contract_number = 'C{}000{}'.format(contract.branch.code,
                                                contract.id)
        elif contract.id < 100:
            contract_number = 'C{}00{}'.format(contract.branch.code,
                                               contract.id)
        elif contract.id < 1000:
            contract_number = 'C{}0{}'.format(contract.branch.code,
                                              contract.id)
        else:
            contract_number = 'C{}{}'.format(contract.branch.code, contract.id)

        contract.contract_number = contract_number
        contract.save()

        for config in configs:
            rates = config.pop('rates', None)
            billing_type = config.pop('billing_type', None)
            external_assignment_name = config.pop('external_assignment_name',
                                                  None)
            internal_assignment_name = config.pop('internal_assignment_name',
                                                  None)
            team_leader = config.get('team_leader', None)

            rate_list = []
            if billing_type == ContractConfig.HOURLY_BILLING and rates:
                hourly_has_fee_ceiling = config.pop('hourly_has_fee_ceiling',
                                                    None)
                hourly_fee_ceiling = config.pop('hourly_fee_ceiling', None)

                contract_config = ContractConfig.objects.create(
                    contract=contract,
                    external_assignment_name=external_assignment_name,
                    internal_assignment_name=internal_assignment_name,
                    team_leader=team_leader,
                    billing_type=billing_type,
                    hourly_has_fee_ceiling=hourly_has_fee_ceiling,
                    hourly_fee_ceiling=hourly_fee_ceiling,
                )

                for rate in rates:
                    rate_list.append(ContractRate(
                        contract_config=contract_config,
                        position=rate['position'],
                        amount_per_hour=rate['amount_per_hour'],
                    ))
                ContractRate.objects.bulk_create(rate_list)
            else:
                fixed_fee_amount = config.pop('fixed_fee_amount', None)
                fixed_fee_pre_amount = config.pop('fixed_fee_pre_amount', None)
                fixed_fee_expenses_included_in_fee = config.pop(
                    'fixed_fee_expenses_included_in_fee', None)
                ContractConfig.objects.create(
                    contract=contract,
                    external_assignment_name=external_assignment_name,
                    internal_assignment_name=internal_assignment_name,
                    team_leader=team_leader,
                    billing_type=billing_type,
                    fixed_fee_amount=fixed_fee_amount,
                    fixed_fee_pre_amount=fixed_fee_pre_amount,
                    fixed_fee_expenses_included_in_fee=fixed_fee_expenses_included_in_fee,
                )

        return contract

    @transaction.atomic
    def update(self, instance, validated_data):
        configs = validated_data.pop('configs', [])
        contract: Contract = super(ContractModelSerializer, self).update(
            instance, validated_data)
        contract.save()
        if configs:
            contract_config_list = []
            for config in configs:
                rates = config.pop('rates', None)
                billing_type = config.pop('billing_type', None)
                external_assignment_name = config.pop(
                    'external_assignment_name', None)
                internal_assignment_name = config.pop(
                    'internal_assignment_name', None)
                team_leader = config.get('team_leader', None)

                if billing_type == ContractConfig.HOURLY_BILLING and rates:
                    id = config.pop('id', None)
                    assignments = None
                    contract_config = None
                    if id:
                        assignments = Assignment.objects.filter(
                            contract_config=id)
                        contract_config = ContractConfig.objects.filter(
                            id=id,
                            is_invoiced=True).first()
                    if assignments or contract_config:
                        contract_config_list.append(id)
                        pass
                    else:
                        hourly_has_fee_ceiling = config.pop(
                            'hourly_has_fee_ceiling', None)
                        hourly_fee_ceiling = config.pop('hourly_fee_ceiling',
                                                        None)

                        contract_config, _ = ContractConfig.objects.update_or_create(
                            id=id,
                            contract=contract,
                            defaults=dict(
                                external_assignment_name=external_assignment_name,
                                internal_assignment_name=internal_assignment_name,
                                team_leader=team_leader,
                                billing_type=billing_type,
                                hourly_has_fee_ceiling=hourly_has_fee_ceiling,
                                hourly_fee_ceiling=hourly_fee_ceiling)

                        )
                        contract_config_list.append(contract_config.id)
                        rate_list_ids = []
                        for rate in rates:
                            contract_rate, _ = ContractRate.objects.update_or_create(
                                position=rate['position'],
                                contract_config=contract_config,
                                defaults=dict(
                                    amount_per_hour=rate['amount_per_hour'])
                            )
                            rate_list_ids.append(contract_rate.id)

                        rate = ContractRate.objects.filter(
                            contract_config=contract_config
                        )
                        if rate:
                            rate.exclude(id__in=rate_list_ids).delete()

                else:
                    id = config.pop('id', None)
                    assignments = None
                    contract_config = None
                    if id:
                        assignments = Assignment.objects.filter(
                            contract_config=id)
                        contract_config = ContractConfig.objects.filter(
                            id=id,
                            is_invoiced=True).first()
                    if assignments or contract_config:
                        contract_config_list.append(id)
                        pass
                    else:
                        fixed_fee_amount = config.pop('fixed_fee_amount', None)
                        fixed_fee_pre_amount = config.pop(
                            'fixed_fee_pre_amount', None)
                        fixed_fee_expenses_included_in_fee = config.pop(
                            'fixed_fee_expenses_included_in_fee', None)
                        contract_config, _ = ContractConfig.objects.update_or_create(
                            id=id,
                            contract=contract,
                            defaults=dict(
                                external_assignment_name=external_assignment_name,
                                internal_assignment_name=internal_assignment_name,
                                team_leader=team_leader,
                                billing_type=billing_type,
                                fixed_fee_amount=fixed_fee_amount,
                                fixed_fee_pre_amount=fixed_fee_pre_amount,
                                fixed_fee_expenses_included_in_fee=fixed_fee_expenses_included_in_fee)

                        )
                        contract_config_list.append(contract_config.id)

                        rate = ContractRate.objects.filter(
                            contract_config=contract_config
                        )
                        if rate:
                            rate.delete()
            contract_config = ContractConfig.objects.filter(contract=contract)
            if contract_config:
                contract_config.exclude(id__in=contract_config_list).delete()
        return contract

    def to_representation(self, instance):
        self.fields['branch'] = SelectBranchSerializer()
        self.fields['bank_account'] = SelectBankAccountSerializer()
        self.fields['configs'] = ContractConfigSerializer(
            source='contract_configs', many=True)
        self.fields['client'] = SelectClientSerializer()
        self.fields['currency'] = SelectCurrencySerializer()
        return super().to_representation(instance)
