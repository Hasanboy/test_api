from rest_framework import serializers

from main.models import ContractConfig
from main.serializers.select_serializer import SelectContractSerializer, SelectRateSerializers


class ContractConfigModelSerializer(serializers.ModelSerializer):
    rates = SelectRateSerializers(many=True, required=False)

    class Meta:
        model = ContractConfig
        fields = '__all__'

    def to_representation(self, instance):
        self.fields['contract'] = SelectContractSerializer()
        return super().to_representation(instance)
