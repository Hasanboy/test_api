from rest_framework import serializers

from main.models import Tags
from main.serializers.content_type import ContentTypeSerializer


class TagsModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'

    def to_representation(self, instance):
        self.fields['model'] = ContentTypeSerializer(required=False)
        return super().to_representation(instance)


class TagsSelectSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
